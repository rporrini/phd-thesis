
The rapid growth of structured and semi-structured data available on the Web
unveiled new horizons and business opportunities, and lead to the development of
Data Intensive applications in a variety of domains, from eCommerce to the Open
Government context~\cite{ChenZ14a}. A Data Intensive application (DI
application) aims at fulfilling the information needs of end-users by leveraging
data acquired, integrated, and maintained over time. The established data
management approach for DI applications accounts to the \emph{pay-as-you-go}
integration of \emph{data sources} into a
\emph{dataspace}~\cite{Halevy-sigmodr05}. \emph{Semantic heterogeneity} of data
from different sources is one of the main challenges to be faced in the
maintenance of dataspaces~\cite{Halevy-queue05}. Knowledge Graphs (\kgs) emerged
as an effective tool to cope with this
challenge~\cite{JanowiczHHH15,KnoblockS15}.

A \kg is a semantically organized, machine readable, graph-like collection of
\emph{entities}, \emph{types}, and \emph{relations} holding between
them~\cite{SuchanekW13}, and can be represented using different data models,
such as RDF or the Relational Model. A \kg acts also as a repository of machine
readable knowledge used to integrate new data into the dataspace, via the
establishment of \emph{mappings} between the sources and the \kg. A \kg provides
a machine readable \emph{representation} of data that is the backbone upon which
the user-experience of a DI application can be enhanced by implementing advanced
data access features. Such features are usually supported by the enrichment of
the \kg with a set of \emph{facets}: specialized relations aimed at model the
\emph{salient} characteristics of entities from specific domains (e.g., news,
actors, or wine bottles), from an end-user perspective.

The effective maintenance of the dataspace, its \kg and mappings, is a key asset
for DI applications. The richness and quality of the representation provided by
the \kg influences not only the ability of a DI application to fulfill the
information needs of end-users, but also the ability to integrate new sources
into the dataspace~\cite{JanowiczHHH15}. Domain experts play a crucial role in
this picture because they are in charge of performing and supervising the
execution of crucial data management tasks, ensuring the equate quality of data
provided to end-users~\cite{SuchanekW14,SzekelyKSPSYKNM15}.
Such tasks includes the \emph{modeling} and the \emph{enrichment} of the schema
of the \kg (i.e., types, relations and facets), and the \emph{discovery} of
mappings between the sources and the \kg. In order to proper maintain the
dataspace, domain experts must be equipped with effective tools that not only
provide automatic support for specific data management tasks, but also provide
the necessary information for supervision and validation.

Several challenges arise in the maintenance of the \kg and mappings in a
dataspace. One of them accounts to the definition of meaningful and domain
specific facets. Enriching the schema of the \kg with domain specific facets so
as to support the advanced data access features demanded by DI applications is
crucial, but nevertheless hard. Such task requires an understanding of the
salient characteristics of instances for a large number of domains, and thus is
difficult and time consuming at a large scale. The majority of state-of-the-art
approaches in this area focus on the enrichment of the \kg schema with types and
relations extracted from the sources (see Section~\ref{sec:consolidation}),
while few focus on the extraction of
facets~\cite{PascaAlfonseca,PoundEAl,FacetedPedia,douCIKM2011,kongSIGIR2013} for
data presentation. Among such approaches, none specifically tackles the problem
of extracting domain specific facets. As a result, the current state-of-the-art
does not provide adequate support to domain experts in the enrichment of the
schema of the \kg with granular, domain specific representation.

Motivated by these challenges, this dissertation studies how to enrich the
schema of a \kg with domain specific facets extracted from a vast amount of
structured sources, providing interactive methods to domain experts in charge of
maintaining a dataspace to ensure the adequate quality of the data. To the best
of our knowledge, we are among the first in focusing on the enrichment of a \kg
with domain specific facets. We propose an approach to extract facets from
structured data integrated into a dataspace by leveraging mappings established
between sources and \kg types, as an effective way to enforce the domain
specificity of the extracted facets (see Chapter~\ref{cap:consolidation}). We
then discuss an approach to provide a domain specific interpretation of
extracted facets. We propose to re-use relations specified by the \kg of the
dataspace, or eventually by other external \kgs, so as to reconcile the end-user
oriented representation of instances provided by facets to the back-end oriented
representation provided by \kg relations, and thus refine the integration
between sources (see Chapter~\ref{cap:interpretation}). Finally, we introduce
an approach to provide an overview of the specificity of relations of a \kg,
which constitutes a vital information for domain experts in charge of
supervising the enrichment of the \kg schema with domain specific facets and
their domain specific interpretation (see Chapter~\ref{cap:summarization}).

In the remainder of this chapter we present a DI application from the eCommerce
domain that constitutes a motivating example of the research work discussed in
this dissertation (Section~\ref{sec:motivating-example}). We then sketch the
general data management methodology for dataspaces
(Section~\ref{sec:introduction-context}), discuss some of the limitations of
current approaches to the management of dataspaces
(Section~\ref{sec:introduction-limitations}), and provide an overview of the
contributions discussed in the dissertation
(Section~\ref{sec:introduction-contributions}).

\section{An eCommerce Data Intensive Application}\label{sec:motivating-example}

A Comparison Shopping Engine (CSE) is a particular type of DI application
specific for the eCommerce domain. A CSE integrates product offers from a large
set of eMarketplaces into a dataspace. End-users access a CSE in order to find
offers and compare them along several possible dimensions, including their price
or specific technical features (e.g., the screen size of smartphones).
TrovaPrezzi\footnote{\url{http://www.trovaprezzi.it}} is one of the most
accessed Italian CSEs. Active since year 2002, at the time of writing
TrovaPrezzi integrates about 12 millions offers from about 3000 eMarketplaces,
and is accessed by about 13 millions of unique end-users every month.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.8\textwidth]{images/trovaprezzi.png}
	\caption{The TrovaPrezzi Italian Comparison Shopping Engine.}
	\label{fig:trovaprezzi}
\end{figure}

\concept{The TrovaPrezzi Dataspace}

Figure~\ref{fig:trovaprezzi} depicts a screenshot of a web page from
TrovaPrezzi, which displays offers of a specific category (i.e., Televisions).
From an end-user perspective, TrovaPrezzi provides two basic data access
features: \emph{type-based} and \emph{facet-based} browsing. Types (or
categories) provide a coarse-grained representation of all the offers of the
TrovaPrezzi dataspace, and allow users to rapidly recall the ``family'' of
offers they are interested in. Conversely, facets provide a fine-grained, domain
specific representation of offers, which helps users to rapidly recall offers
with specific characteristics (e.g., all TVs with a 40 inches screen, from
Figure~\ref{fig:trovaprezzi}). The core of the TrovaPrezzi dataspace is a \kg
(exemplified in Figure~\ref{fig:trovaprezzi-dataspace}), which includes products
(e.g., $\named{Iphone6}$) modeled as \kg entities, product categories (e.g.,
$\named{MobilePhone}$) modeled as \kg types, generalist attributes common to
all the offers (e.g., $\named{price}$) modeled as generalist facets, and
technical features (i.e., $\named{displaySize}$) modeled as domain specific
facets.

Figure~\ref{fig:trovaprezzi-dataspace} provides a schematization of the data
management process adopted by the TrovaPrezzi CSE. Data sources (i.e.,
eMarketplaces) exchange their data by means of semi-structured CSV and XML
formats. The integration process is driven and supervised by domain experts with
the aid of tools, and is performed by establishing mappings between source and
\kg entities, types, and relations. When a new source requests the inclusion of
its data into the dataspace, it is first asked to provide a CSV of XML dump of
its offers, as for instance the one depicted in
Figure~\ref{fig:trovaprezzi-dataspace}. Then, domain experts incrementally
establish mappings from the data source to the \kg. Preliminary mappings are
established in order to interpret the generalist characteristics of offers, such
as the title or the price (i.e., the mapping between $\named{attr\_4}$ column
and $\named{brand}$ in Figure~\ref{fig:trovaprezzi-dataspace}). Offers are
integrated in the dataspace since this preliminary mapping: users can search and
find them.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.9\textwidth]{images/dataspace-cse.pdf}
	\caption{The dataspace of a Comparison Shopping Engine.}
	\label{fig:trovaprezzi-dataspace}
\end{figure}

The integration is initially weak, because offers have no fine-grained, domain
specific representation attached, being them characterized by very few
generalist facets such as $\named{price}$ or $\named{brand}$. As a result, at
this stage TrovaPrezzi is able to provide basic search and browse features with
somehow limited user-experience, but the absence of granular, domain specific
representation does not allow to go much far beyond that. Then, mappings are
established in order uniformly categorize offers using \kg types. At this stage
offers start to be interpreted, for instance, as mobile phones. Then, domain
experts start defining facets aimed at providing a domain specific
representation of offers, and the integration is possibly further refined by
establishing fine-grained mappings between instances. At this final stage,
offers are tightly integrated.

\concept{Data Management Challenges}

In the data management approach adopted by TrovaPrezzi the problem of semantic
heterogeneity between sources is tackled by establishing mappings between the
sources and the \kg. The \kg provides an end-user oriented representation of
offers based on types, relations and facets. Enhanced user-experience built on
top of this representation of data allows TrovaPrezzi to effectively fulfill the
information needs of its end-users. Intuitively, the quality and the richness of
such representation impact the user-experience of TrovaPrezzi, and consequently
the business revenue originated from it.

At the time of writing, a dedicated team composed by 12 domain experts drives,
supervises and validates the entire data management process described above
through several tools maintained by two teams of 2 and 3 software engineers,
including the author of this dissertation. The \emph{modeling} of the schema of
the \kg (i.e., the definition of types, relations and facets for data
presentation), and the \emph{discovery} of mappings from the sources to the \kg,
are the main duties of the domain experts team, and are difficult and time
demanding. The enrichment of the \kg with meaningful facets at large scale
requires a deep understanding of the salient characteristics of offers (e.g.,
wines are characterized by grape, type, provenance, and so on) for a large
number of diverse domains. Also, the discovery of mappings is challenging
because it requires the establishment of a huge amount of fine-grained mappings
(i.e., millions of mappings, in the case of TrovaPrezzi) between specialized
(i.e, domain specific) sources and the \kg.

As of April 2016, about the 85\% of offers integrated into the TrovaPrezzi
dataspace is represented and presented to end-users only by means of generalist
facets such as $\named{price}$ or $\named{description}$. Efforts in providing
rich and domain specific representation of offers focus on ``mainstream''
categories such as smartphones, tablets, or notebooks, and which account to
about the 35\% of product categories available in TrovaPrezzi, while more
``niche'' categories such as wines or ski equipment are deliberately left
behind, due to the high maintenance costs. However, while mainstream categories
alone contribute to a considerable portion of the revenue generated by
TrovaPrezzi, niche categories constitute the ``long tail'' currently not fully
exploited due to the lack of intelligent data management tools to support domain
experts in the maintenance of domain specific representation of offers.

As a final remark, the challenges highlighted in this section apply not only to
the specific case of TrovaPrezzi, but also to every more general scenario that
requires the management and the integration of data from specialized data
sources. For example, data released by public institutions in the Open Data and
eGovernment context~\cite{NCR20070} is extremely domain specific and include,
for instance, census, crime, but also pollution and public expenses related
data\footnote{see for instance \url{http://index.okfn.org/}}.
In this context, the development of improved services to citizens based on such
data requires a deep understanding of the underlying domain, that is difficult
to achieve~\cite{OpenData}. Remarkably, this difficulty constitutes one of the
barriers in the adoption and reuse of Open Data available from public
institutions~\cite{Sieber2015308}.

\section{Data Management Methodology for Dataspaces}
\label{sec:introduction-context}

Due to the large amount of heterogeneous data managed by DI applications like
the CSE described in the last section, the maintenance of a dataspace (i.e., the
\kg and the mappings) follows an incremental \emph{pay-as-you-go}
process~\cite{PAYGO}.

\concept{Model, Map and Materialize}

Three different but related data management tasks are continuously performed
during the whole lifespan of a dataspace: the \emph{modeling} of the schema of
the \kg, the definition of \emph{mappings} between the sources and the \kg, and
\emph{materialization} of integrated data into the dataspace. Modeling accounts
to the enrichment of the \kg with types, relations and facets for presenting
data to end-users. A facet is a specialized type of relation that models a
salient characteristic of entities of particular domains (i.e., types).
The goal of the first two activities (i.e., modeling and mapping) is to enable
the materialization phase, where mappings from the sources to \kg types and
relations defined in the modeling phase are automatically leveraged in order to
transform source data and import it into the dataspace.

\concept{Role of Domain Experts}

The contribution of domain experts is fundamental in the management of the
dataspace, as they are in charge of incrementally drive, supervise, and validate
all the data management tasks presented in this section. Through an effective
supervision, domain experts incrementally enrich the \kg with a richer and
domain specific representation of instances, and establish mappings between
elements of such representation and the sources. Providing automatic support for
domain experts in these tasks is crucial for a proper and effective management
of the dataspace.

\section{Limitations in the Support to Domain
Experts}\label{sec:introduction-limitations}

Enriching the schema of the \kg to provide a rich and domain specific
representation of instances requires a deep understanding of the salient
characteristics of instances for a large number of diverse domains, and thus is
difficult and time consuming at a large scale. Intelligent tools are needed also
to support domain experts in the establishment of mappings, to ultimately ensure
an high quality of the data in the dataspace. Moreover, domain experts must be
equipped not only with tools that actually enrich the schema of the \kg and
establish mappings, but also with the necessary information to proper supervise
such tools and validate their results, in order to ensure the adequate quality
of data.

Many different approaches can be potentially applied to support domain experts
in the management of the dataspace (see Chapter~\ref{cap:literature} for an in
depth discussion). However, such approaches have several limitations, among
which there are:
\begin{enumerate}
  \item \textbf{Extraction of domain specific facets.} Despite some work
  focused in the enrichment of the \kg schema with
  facets~\cite{PascaAlfonseca,PoundEAl,FacetedPedia,douCIKM2011,kongSIGIR2013},
  to the best of our knowledge no previous work focused on extracting domain
  specific facets. Enriching the \kg schema with such facets
  is challenging because requires a granular analysis of the semantics of a vast
  amount of heterogeneous data with approaches capable to capture domain
  specificity. As a result, the current state-of-the-art
  supports domain experts in the enrichment of the schema of the \kg with
  types, relations, and generalist facets, but not domain specific ones.
  \item \textbf{Profiling of the \kg.} There have been several efforts in the
  state-of-the-art to profile a \kg with the goal of
  providing domain experts with the necessary information needed to supervise
  the data management process of dataspaces~\cite{Naumann2014}. However, such 
  approaches either include only a portion of data estimated to be more relevant
  in order to understand and explore the structure of the \kg, or include a set
  of statistics without capturing patterns in the \kg, which can provide useful
  insights to domain experts in the execution of data management tasks for
  dataspace maintenance. Extracting a more complete profile of a \kg is
  challenging because it requires to find the right balance between richness and
  compactness. A profile should be rich enough to represent the whole \kg, and
  compact enough to avoid redundant information so as not to overwhelm domain
  experts.
\end{enumerate}

\section{Contributions and Outline}\label{sec:introduction-contributions}

Motivated by the challenges and the limitations discussed in the last sections,
this dissertation investigates how to enrich the schema of a \kg with domain
specific facets extracted from a vast amount of structured sources, providing
interactive methods to domain experts in charge of maintaining a dataspace to
ensure the adequate quality of the data. With the above research question in
mind, the contributions of this dissertation are schematized in
Figure~\ref{fig:contributions} and summarized in the remaining of this section.

\contribution{Domain Specific Facet Extraction}{cap:consolidation}

We study how to support domain experts in the \emph{extraction} of domain
specific facets so as to enrich the \kg schema with granular, domain specific
representation of data. Facets are specialized relations aimed at model the
salient characteristics of entities from specific domains (e.g., news, actors,
or wine bottles), and thus the technical problem tackled in this contribution
accounts to the extraction of salient domain specific relations. The proposed
approach leverages the information already present in the dataspace, in the form
of mappings established between source and \kg types, to suggest meaningful
facets specific for a given \kg type.

\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{images/contributions.pdf}
	\caption{A summary of the contributions of this dissertation.}
	\label{fig:contributions}
\end{figure}

\contribution{Specificity-based Interpretation of Facets}{cap:interpretation}

We then focus on providing a domain specific interpretation of extracted facets,
by \emph{annotating} them with \kg relations. Given a facet, the proposed
approach derives a set of candidate relations from \kg and ranks them
considering how much their semantics is similar to the semantics of the facet,
focusing on domain specificity. By re-using relations specified by the \kg of
the dataspace, we reconcile the end-user oriented representation of instances
provided by facets to the back-end oriented representation provided by \kg
relations, thus refining the integration between sources.

\contribution{Knowledge Graph Profiling}{cap:summarization} 

We propose a \kg \emph{summarization} model that provides an abstract view of
\kg relations, in the form of Abstract Knowledge Patterns (e.g.,
$\pattern{\named{Wines}}{\named{origin}}{\named{Region}}$) and their
corresponding occurrence statistics extracted from the \kg. With such knowledge
at hand, domain experts can properly supervise the extraction and annotation of
facets and validate their result, deciding for example to annotate a facet with
a relation that they consider more domain specific with respect to automatically
suggested ones. The summarization model has been included in ABSTAT, a framework
capable to profile a \kg by providing an abstract representation of it.

\contributionApp{Case Study: Product Autocomplete}{cap:autocomplete}

We describe a case study from the eCommerce domain where the rich domain
specific representation is leveraged in order to provide an advanced Product
Autocomplete feature for eMarketplaces. The Product Autocompletion system, which
is named COMMA, is an example of the impact of the core contributions of this
dissertation, deployed and evaluated on the real world scenario of an Italian
eMarketplace.

\concept{Outline of the Dissertation}

The remainder of the dissertation is organized as follows. In
Chapter~\ref{cap:dataspaces}, we provide a formal definition of a dataspace and
its components: the \kg, the data sources and the corresponding mappings between
them. We also describe the Data Management methodology adopted in the management
of a dataspace and stress the importance of supporting domain experts in 
the enrichment of the \kg with a domain specific representation of data. In
Chapter~\ref{cap:literature} we survey the state-of-the-art in management of
dataspaces.
Chapters~\ref{cap:consolidation},~\ref{cap:interpretation},~\ref{cap:summarization}
present the core contributions of this dissertation right before
Chapter~\ref{cap:conclusions}, where we conclude the dissertation with a summary
of the research goals achieved and contributions, and outline the potential
future work and impact of the proposed techniques.

\concept{Note}

The work described in this dissertation has been done in collaboration with
Matteo Palmonari, Brando Preda, Anisa Rula, Blerina Spahiu, Andrea Maurino,
Carlo Batini and Giuseppe Vizzari, and partially appeared
in~\cite{COMMA-WI,COMMA-WIAS,CAISE,Palmonari2015,Spahiu16}. The domain specific
facet extraction technique described in Chapter~\ref{cap:consolidation} is
currently running in production within the TrovaPrezzi CSE. The facet
interpretation technique described in Chapter~\ref{cap:interpretation} is
included in STAN\footnote{\url{http://stan.disco.unimib.it} -
\url{https://github.com/brando91/STAN}}, a tool for the interpretation of
generic tabular data (see
Section~\ref{sec:facet-interpretation-table-annotation}). Finally, the \kg
summarization framework described in Chapter~\ref{cap:summarization} can be
accessed online through the ABSTAT web
application\footnote{\url{http://abstat.disco.unimib.it} -
\url{https://github.com/rporrini/abstat}}.
