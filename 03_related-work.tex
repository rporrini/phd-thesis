
Chapter~\ref{cap:dataspaces} provided a detailed discussion of the technical and
methodological background in the management of a dataspace. We highlighted the
necessity to support domain experts in crucial data management tasks such as the
enrichment of the schema of the \kg (to support the modeling phase), the
establishment of mappings, and to provide domain experts with adequate
information for supervision and validation. We now discuss the state-of-the-art
in the management of dataspaces, focusing on how effectively supports the the
enrichment of the schema of the \kg (Section~\ref{sec:consolidation}) and the
establishment of mappings (Section~\ref{sec:interpretation}). Moreover, we will
discuss also approaches that aim at \emph{profiling} the current status of a
\kg, so as to provide the vital information to domain experts useful for
supervision and validation (Section~\ref{sec:summarization}), before concluding
the chapter in Section~\ref{sec:literature-summary}.

\section{Knowledge Graph Schema Enrichment}\label{sec:consolidation}

State-of-the-art approaches support the enrichment of the \kg schema by
extracting types (e.g.,
\cite{babelnet,Wu2008,wuSIGMOD2012,SnowJN06,NavigliV10,Navigli2011,Liu2012,Li2012,BansalBMK14,Castanet,DakkaIperiotis,Medelyan}),
relations and
facets~\cite{dbpedia,yago,FacetedPedia,douCIKM2011,kongSIGIR2013,PascaAlfonseca,fp275-li}
from source data. In our discussion we will focus our attention mainly on
relation and facet extraction approaches, because they share the same goal of
this dissertation: to enrich the \kg with a rich representation of instances.
As sources of a dataspace expose data in different formats (as discussed in
Section~\ref{sec:data-sources}), we classify and discuss state-of-the-art
approaches depending on how structured is data from which they extract types,
relations or facets.

\subsection{Extraction from Structured Sources}

A large body of work focus on the extraction types and relations from databases
(see,~\cite{spanosSWJ2012} for a recent survey). The general goal of such body
of work is to bootstrap the schema of \kg, eventually including a set of
terminological axioms. The most elementary approach for the extraction of types
and relations from a relational database consists of creating a \kg type from
each table (i.e., table-to-type) and a \kg relation for each table column (i.e.,
column-to-relation)~\cite{mapping,mappingTimbl}. An example of such approach is
depicted in Figure~\ref{fig:database}. Although elementary, this approach
inspired most of related work in this area, which aim at automatically
discovering the semantics hidden in the database structure for extracted types
and relations.

State-of-the-art approaches extract types and relations along with a set of
terminological axioms involving them
(e.g.,~\cite{ChampinHT07,Nyulas07,KorotkiyT04,VolzHSSS04,KupferENM06}).
Terminological axioms are usually in the form of domain and range restrictions
and emerge from constraints specified in the database (e.g., primary or foreign
keys). Consider, for instance, the relational database described in
Figure~\ref{fig:database}. Following the elementary approach described earlier,
the type $\named{Product}$ is extracted from the table $\named{Products}$, while
the type $\named{ProductCategory}$ is extracted from the table
$\named{Product\_Categories}$. An instance of $\named{Product}$ is characterized
by a set of relations including $\named{brand}$ and $\named{category}$. The
semantics of the latter relation is specified by terminological axioms that
emerges from the foreign key between tables $\named{Products}$ and
$\named{Product\_Categories}$ specified by the database.
Hence, domain and range restrictions can be extracted in the form of
terminological axioms like
\begin{formula*}
&\forall x~\big(\named{category}(x,y) \rightarrow \named{Product}(x)\big)\\
&\forall y~\big(\named{category}(x,y) \rightarrow 
\named{ProductCategory}(y)\big).
\end{formula*}

\begin{figure}[t] 
	\centering
	\includegraphics[width=1\textwidth]{images/database.pdf}
	\caption{An example of extraction of the \kg schema from a relational database.}
	\label{fig:database}
\end{figure}

Approaches described in this section are mainly applicable during the
boostrapping phase of the \kg lifecycle, but not in the maintenance phase,
because they have two limitations: (1) they extract generalist relations, and
(2) they consider a single data source as input. As a result, they are capable
to extract only a generalist, and not domain specific, representation of
instances. Moreover, this representation has limited power in representing data
exposed by other data sources. These two limitations make the described
approaches applicable during the early life of a \kg, and are particularly
useful when a \kg is bootstrapped from a set of ``core'' data sources (e.g., a
preliminary product catalog of the Comparison Shopping Engine described in
Section~\ref{sec:introduction-context}). However, they provide only a generalist
representation, which applies to data from single sources in isolation and
thus are of little help in the management phase.

\subsection{Extraction from Semi-Structured Sources}

Related work in the extraction of types and relations from databases inspired
the construction of the DBpedia \kg~\cite{dbpedia}. The general idea behind the
DBpedia project is that knowledge can be extracted from Wikipedia articles and
consolidated into a \kg which includes relations. In their very first
work~\cite{dbpediaFirst}, DBpedia creators extract relations from Wikipedia
infoboxes (e.g., the ones depicted in Figure~\ref{fig:infoboxes}). Infoboxes are
consistently-formatted boxes, which are present in most of Wikipedia articles,
and that synthetically characterize instances of a given set of types (e.g.,
musical artists). Infoboxes are generated through the application of domain
specific \emph{templates}, as for example the one depicted in
Figure~\ref{fig:infobox-template}. Practically, a template consists of a
structured set of predefined attributes (e.g., $\named{name}$) and relative
values. The DBpedia relation extraction framework turns these attributes into
relations and outputs the corresponding set of relational assertions involving
them. For example, given the template in Figure~\ref{fig:infobox-template}, the
DBpedia relation extraction framework extracts the relation $\named{birthPlace}$
along with the relational assertion $\named{birthPlace}(\named{BillGates},
\named{Seattle})$.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.6\textwidth]{images/infoboxes.pdf}
	\caption{Two infoboxes from Wikipedia (as of February 2016).}
	\label{fig:infoboxes}
\end{figure}

The approach to relation extraction just described has some evident, yet well
known limitations, which stem from the intrinsic heterogeneity that
characterizes Wikipedia templates and their attributes~\cite{dbpedia}. In fact,
as the Wikipedia infobox templating system has evolved over time, different
communities of Wikipedia editors use different templates to represent the same
types of entities (e.g., Japanese cities vs Swiss cities). Templates created by
different communities also model the same relations with syntactically different
attributes (e.g., $\named{birthplace}$ and $\named{placeofbirth}$). Moreover,
attribute values are heterogeneous, in the sense that they are expressed using a
wide range of different formats. As a result, the DBpedia relation extraction
approach produces a set of relations that are: (1) not normalized, and (2) whose
ranges potentially contains not normalized, possibly inconsistent values. In
summary: while the relations resulting from the extraction cover a wide range of
knowledge domains, they provide a low-quality representation of instances.

To address the limitations of the automatic extraction of relations, DBpedia
creators proposed a different crowd-sourcing based paradigm, based on the
explicit definition of mappings between template attributes and the well defined
relations provided by the DBpedia Ontology, which constitutes the schema of the
DBpedia \kg~\cite{dbpedia2009}. These mappings, along with the DBpedia Ontology,
are collaboratively curated and maintained by a community of
users\footnote{\url{http://mappings.dbpedia.org}}. This significantly increases
the quality and richness of representation provided by \kg. Observe that a
similar approach has been applied to the YAGO
\kg~\cite{yago}, which is the other noticeable \kg built from knowledge
extracted from Wikipedia articles. In fact, as for DBpedia relations, the
extraction approach implemented by YAGO relies on a predefined set of mappings
between template attributes and \kg relations.

\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{images/infobox-template.pdf}
	\caption{An example of template for persons.}
	\label{fig:infobox-template}
\end{figure}

Observations on the weaknesses of the DBpedia relation extraction approach are
particularly relevant to this dissertation as they stem from data heterogeneity.
In fact, while it is true that the DBpedia \kg is enriched with types and
relations extracted from a single data source (i.e., Wikipedia), it is also true
that Wikipedia editors are organized in different communities each one with
different and independent ways of classifying and representing source instances.
Observed from this perspective, Wikipedia poses the same data management
challenges faced by DI applications that deal with the integration of multiple
data sources. Approaches based on the explicit definition of mappings between
template attributes and \kg relations introduce a bottleneck in the management
of the dataspace, especially when extending the \kg towards new domains of
knowledge.

The problem of extracting domain specific facets, to represent instances
described by Wikipedia articles have been studied also by Li et
al.~\cite{FacetedPedia}. The approach proposed by the authors takes in input a
set of Wikipedia articles, and outputs a set of facets. Observe that, in this
case, all the extracted facets share a common domain which includes all the
entities described by input articles. Conversely, the ranges of facets include
all the instances described by articles that are hyperlinked from input
articles. Hyperlinks between two given articles are considered as a clue of the
fact that there exists a relationship between instances described by those
articles. Given a set of input articles, the proposed approach selects all the
instances described by hyperlinked articles and partitions them into facet
ranges, based on the classification provided by Wikipedia categories. As
Wikipedia categories provide a very rich classification scheme~\cite{yago} the
authors devise an automatic approach to find the partition that best balances
the generality and the specificity of facets.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\textwidth]{images/amazon-books.pdf}
	\caption{A Web page describing books.}
	\label{fig:offer}
\end{figure}

Facets have been extracted from HTML documents provided by semi-structured data
sources using unsupervised~\cite{douCIKM2011} or supervised~\cite{kongSIGIR2013}
machine learning techniques. Similarly to Li et al.~\cite{FacetedPedia}, those
approaches take in input a set of HTML documents describing domain specific
source instances (e.g., books from Figure~\ref{fig:offer}) and output a set of
facets, specified in terms of the same domain, which includes all the instances
described by input documents, and with facet ranges extracted from HTML elements
embedded in the documents (e.g., the ones highlighted in red in
Figure~\ref{fig:offer}). Again, the main challenges that such approaches must
face stem from the intrinsic heterogeneity of values extracted from HTML
elements. In order to address these challenges, QDMiner~\cite{douCIKM2011}
propose an unsupervised approach based on the adaptation of the Quality
Threshold clustering algorithm~\cite{heyer1999exploring}, which devises a set of facet
ranges by jointly maximizing specificity and coverage across instances described
by the input documents. The same challenge is tackled by Kong and
Allan~\cite{kongSIGIR2013}, who propose a Graphical Model based approach that is
based on the estimation of the probability of two values to be part of the same
facet range, based on training data.

\subsection{Extraction from Unstructured Data Sources}

Some state-of-the-art approaches extract facets from unstructured data
sources~\cite{PascaAlfonseca,PoundEAl}. Large unstructured text document
collections are analyzed in order to identify a set of relevant terms, that will
form the ranges of extracted facets. Such approaches typically rely on lexical
resources such as WordNet~\cite{wordnet} to handle the intrinsic heterogeneity
and ambiguity of source data, but also on extra information provided by full
text query logs, when available. Other state-of-the-art approaches extract types
taxonomies from unstructured data
sources~\cite{Medelyan,Castanet,DakkaIperiotis,SnowJN06,NavigliV10,Navigli2011,Liu2012,Li2012,BansalBMK14,wuSIGMOD2012}.
State-of-the-art approaches in this area analyze lexico-graphical patterns such
as \emph{x is a y} found in text (also known as Hearst
Patterns~\cite{hearstCOLING1992}) and devise a type taxonomy from them. They do
not extract relations, nor facets. Observe that approaches discussed in this
section cover unstructured data sources, which represent only one of the
possible formats used to exchange data in dataspaces. For this reason, they are
complementary to the contributions of this dissertation, which focus on more
structured data (see Section~\ref{sec:facet-extraction-related}).

\section{Mapping Discovery}\label{sec:interpretation}

State-of-the-art approaches related to mapping discovery automatically establish
mappings between source and \kg instances, types and relations, provided by
structured~\cite{FerraraNS13,shvaikoTKDE2013,CheathamH14a,Rimom,Suchanek2011,MelnikGR02,MaedcheS02,Pernelle2013},
semi-structured~\cite{limayeVLDB2010, venetisVLDB2011, shenKDD2012, wangER2012,
querciniEDBT2013, zwicklbauerISWC2013, mulwadISWC2013, zhangISWC2014,
zhangEKAW2014, zhangSWJ2015} and
unstructured~\cite{nadeau2007survey,shenTKDE2014} data sources. Related work
that is most relevant to this dissertation focuses on the establishment of
mappings between structured and semi-structured data sources and the \kg, and we
review them in the following sections. We will not cover the establishment of
mappings from unstructured data sources, as state-of-the-art approaches focus on
the establishment of i-to-i mappings between source instances described in free
text and \kg instances. Those approaches, proposed in the literature under the
general classification of Named Entity Recognition and Linking, are less
relevant to this dissertation, which focuses more on the schema of the \kg
(i.e., types and relations). However, we point the interested reader
to~\cite{nadeau2007survey,shenTKDE2014}, for two surveys of that area.

\subsection{Mapping Structured Sources}

Early approaches on the discovery of mappings in presence of structured sources
have been provided by the Data Management community, under the general
classification schema matching~\cite{spanosSWJ2012} approaches. They support the
management of particular dataspaces where sources provide direct access to
tables and views of a database. However, these approaches can be hardly applied
to most of the dataspaces on a Web scale, where sources are distributed over the
Web and are accessible via the HTTP protocol~\cite{Halevy-sigmodr05}. More
recently, the discovery of mappings between structured data sources have been
studied also in the ontology matching field (see~\cite{shvaikoTKDE2013} for a
survey), with focus on data sources that represent and exchange data by means of
the RDF data model.

Ontology matching approaches mainly focus on the establishment of t-to-t
mappings and r-to-r mappings, while the establishment of i-to-i mappings has
been studied under the general classification of Instance
Matching~\cite{FerraraNS13}. The widely adopted approach in establishing t-to-t
and r-to-r mappings is to analyze terminological axioms (i.e., subtype
relations, domain and range restrictions) as well as types' and relations'
lexicalizations. State-of-the-art approaches apply a composition of different
criteria in order to compute the similarity between two types or relations.
Syntactic similarity criteria are used to compare lexicalizations
(see,~\cite{StringSimilarity} for a survey of the main approaches). Structural
similarity criteria apply graph matching techniques to the graph extracted from
terminological axioms, which includes the subtype hierarchy but also domain and
range relationships between types and relations
(e.g.,~\cite{MelnikGR02,Rimom,Suchanek2011}).

State-of-the-art ontology matching approaches perform particularly well on
t-to-t mappings as testified by results of the Ontology Alignment Evaluation
Initiative~\cite{2015om}, a yearly competition held at the Ontology Matching
Workshop active since 2006\footnote{\url{http://oaei.ontologymatching.org/}}.
Unfortunately, current ontology matching approaches do not perform comparably
well in the establishment of r-to-r
mappings~\cite{MaedcheS02,Pernelle2013,CheathamH14a}.
In this context, approaches based on well chosen string similarity metrics tuned
to consider relations outperform more ``holistic'' ones that jointly establish
t-to-t, r-to-r and i-to-i mappings~\cite{CheathamH14a} such
as~\cite{Rimom,Suchanek2011}. The important remark is that information about the
domain and range of relations is crucial for effectively establishing r-to-r
mappings. In structured sources that adhere to the RDF data model, this
information is explicitly available (see Section~\ref{sec:data-sources}).

However, in the case of generalist relations, domain and range information may
not be discriminative enough, or even be absent (i.e., no terminological axioms
specified). In fact, generalist relations are often
underspecified~\cite{abedjanCIKM2012} as a result of precise modeling choices
that aim at favoring sharing and re-use, e.g., the relation
$\named{\escaped{dc:date}}$ from the general purpose Dublin Core Elements
Vocabulary\footnote{\url{http://dublincore.org/documents/dces/}}.
Still, underspecified relations may be used by a data source with a more domain
specific semantics. This kind of domain specificity (i.e., emerging from usage)
cannot be captured by state-of-the-art relation mapping approaches are based on
the analysis of terminological axioms.

\subsection{Mapping Semi-Structured Sources}
\label{sec:interpretation-semi-structured}

A large body of work discovers mappings between semi-structured data sources and
the \kg, and in particular of tabular (e.g., CSV) data, under the general
classification of table annotation approaches~\cite{limayeVLDB2010,
venetisVLDB2011, shenKDD2012, wangER2012, querciniEDBT2013, zwicklbauerISWC2013,
mulwadISWC2013, zhangISWC2014, zhangEKAW2014, zhangSWJ2015}). The task of
interpreting a semi-structured table gained attention since a seminal study from
2008 by Cafarella et al.~\cite{cafarellaWEBDB2008}, who shown that the Web pages
contain a huge amount of high-quality tables containing useful relational data.
Table annotation approaches are particularly relevant to this dissertation,
because semi-structured tables are one of the most commonly used exchange format
in dataspaces. Figure~\ref{fig:web-table} depicts the input (i.e., a table) and
the expected output (i.e., a set of mappings) of a generic table annotation
algorithm. Many table annotation approaches assume that a table include a
\emph{subject column} that is, a column containing the subject entities
described by the table~\cite{venetisVLDB2011, wangER2012, zhangSWJ2015}, while
the remaining columns contain entities or literal values that are in a
relationship with subject entities. The goal of a table annotation approach
is to interpret a table by establishing mappings between cells, columns and pair
of columns, and a \kg instances (i-to-i), types (t-to-t), and relations
(r-to-r), respectively.

\begin{figure}[t] 
	\centering
	\includegraphics[width=1\columnwidth]{images/table-interpretation.pdf}
	\caption{Schematization of the input and the output of a table annotation
	approach.}
	\label{fig:web-table}
\end{figure}

Venetis et al.~\cite{venetisVLDB2011} apply a maximum likelihood inference model
to estimate the probability of a relation that holds between values from two
columns. The same inference model is applied in order to annotate columns with
types. The estimation of the maximum likelihood model is based on computation of
the frequencies within the \kg of all pairs of values of the same row as
subjects and objects of relations, and does not consider any type information.
Wang et al.~\cite{wangER2012} interpret tabular data sources using the Probase
\kg~\cite{wuSIGMOD2012}. Probase is a probabilistic \kg and it provides scores
that model the \emph{plausibility} and the \emph{ambiguity} of entities being
instance of a certain type and a type being characterized by certain relations.
Those scores are computed during the bootstrapping of the \kg. They interpret a
table by identifying the subject column, establish a t-to-t mapping between it
and a \kg type and then establish r-to-r mappings between the remaining columns
and \kg relations. Their approach is \kg dependent, since they rely on
plausibility and ambiguity scores provided by Probase in order to compute the
overall score of a candidate mapping.

TableMiner~\cite{zhangSWJ2015} interprets tables with entities, types and
relations from the generalist \kg Freebase. One of the main contributions of
TableMiner is the usage of contextual information extracted from the Web page
that contains the table (e.g., table caption, surrounding text, RDFa/Microdata
annotations). Following the same intuition of Wang et al.~\cite{wangER2012},
TableMiner starts with identifying the subject column and provides a set of
preliminary i-to-i and t-to-t mappings. Those mappings are then jointly refined
using an iterative learning procedure. r-to-r mappings are then computed based
on the result of the refinement phase.

The above mentioned table annotation approaches tackle the establishment of
the various type of mappings independently and propose a principled
combination of the result. However, also more holistic approaches have been
proposed by Limaye et al.~\cite{limayeVLDB2010} and Mulwad et
al.~\cite{mulwadISWC2013}. They model the interdependence between table cells,
columns and rows using probabilistic graphical models~\cite{graphical-models}
and perform collective inference in order to jointly compute the optimal
mappings. However, more recent approaches that tackle the establishment of
different (i.e., i-to-i, t-to-t, r-to-r) mappings separately outperformed the
joint inference based ones~\cite{venetisVLDB2011,zhangSWJ2015}.

Karma~\cite{knoblockESWC2012,TaheriyanKSA12,TaheriyanKSA13,karmaeswc} is
designed for situations where data from different data sources to be interpreted
partially overlap and propose a semi-automatic and interactive mapping process
that learns how to establish new mappings based on mappings previously defined.
The domain expert is asked to initially specify t-to-t mappings between columns
and \kg types. Then, Karma automatically computes a set of r-to-r mappings
between pairs of table columns and \kg relations. At each step, the domain
expert can revise and refine the mappings automatically computed by the system.
As Karma assumes that data from the sources to be interpreted partially
overlap, it learns how to suggest t-to-t~\cite{knoblockESWC2012} and r-to-r
mappings~\cite{TaheriyanKSA13}, by using a Conditional Random
Fields~\cite{Lafferty2001} (CRF) based model trained with previously defined
t-to-t mappings that uses features extracted from values in the columns.
Given the defined t-to-t mappings, Karma is also able to infer r-to-r mappings
by leveraging axioms in the input ontology in terms of relation domains and
ranges.

\section{Knowledge Graph Profiling}\label{sec:summarization}

Another body of work that is relevant to this dissertation is related to data
\emph{summarization}~\cite{Zhang:2007,Troullinou15,Loupe2015,Campinas:2012,Jarrar:2012,Presutti:2011,KonrathGSS12,Colazzo:2014,LangeggerW09,auer2012},
which aims at profiling a \kg by providing an abstract representation of its
data (i.e., a \emph{summary}) in terms of types, relations and their reciprocal
usage. Effective summarization approaches are crucial in order to support both
algorithms and domain-experts in the supervision of data management
tasks in dataspaces, by providing the capability to answer to questions like:
(1) what instances or types are described in the \kg? (2) What relations are
used to characterize the instances? (3) What types of instances are linked by a
certain relations and how frequently? We now review state-of-the-art
summarization approaches explicitly proposed for large \kgs.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.9\textwidth]{images/summarization.pdf}
	\caption{Overview of a generic structured data summarization approach.}
	\label{fig:summarization}
\end{figure}

A schematization of the summarization process for a \kg is provided in
Figure~\ref{fig:summarization}. A first body of work devise summarization models
aimed at identifying portions of data that are estimated to be more relevant in
order to understand and explore the structure and the representation of \kg
instances. Early approaches rank the axioms of an ontology based on their
salience so as to present a view over the ontology to the
users~\cite{Zhang:2007}, while more recent approaches such as RDF
Digest~\cite{Troullinou15} identify the most salient subset of data using a more
rich set of different criteria, including the distribution of instances.

A second body of work focus on the summarization of a \kg by reporting
statistics about the usage of types and relations in the data.
Among these approaches Loupe~\cite{Loupe2015}, the most noticeable one, extracts
types and relations along with a rich set of statistics on their usage for the
representation of instances. Loupe provides a summary of a \kg by extracting a
rich set of abstract relation patterns found in the data and their corresponding
frequency. Those patterns are extracted in the form of
$\pattern{\namedtype}{\namedrelation}{D}$, where $\namedtype$ and $D$ are types,
and $\namedrelation$ is a relation. Patterns provide an abstract overview of how
relations are used to represent instances of particular types. In
\cite{Campinas:2012}, authors consider types and relation usage in the
summarization process of an RDF graph and use information similar to relation
patterns. A similar approach is also used in MashQL \cite{Jarrar:2012}, a system
proposed to query graph-based source data (e.g., RDF) without prior knowledge
about the structure and schema of a data source. Pattern extraction from RDF
data is also discussed in~\cite{Presutti:2011}, but in the context of domain
specific experiments and not with the purpose of defining a general structured
data summarization framework.

Other approaches that tackle the \kg summarization problem do not extract
relationships between source types but instead provide several other statistics.
SchemeEx extracts interesting theoretic measures for large \kgs, by considering
the co-occurrence of types and relations~\cite{KonrathGSS12}. A data analysis
approach on RDF data based on an warehouse-style analytic is proposed in
\cite{Colazzo:2014}. This approach focuses on the efficiency of processing
analytical queries which poses additional challenges due to their special
characteristics, such as complexity, evaluated on typically very large \kgs, and
long runtime. In the same line of work, Linked Open
Vocabularies\footnote{\url{http://lov.okfn.org/}}, RDFStats \cite{LangeggerW09}
and LODStats~\cite{auer2012} provide several statistics about the usage of types
and relations, but without representing connections between types.

\section{Summary}\label{sec:literature-summary}

In this chapter we reviewed the state-of-the-art in supporting domain experts in
the main data management tasks for the maintenance of dataspaces. We started
from by reviewing approaches that provide automatic support to domain experts in
the modeling of the \kg schema, and in particular approaches whose goal is to
enrich the schema of the \kg (Section~\ref{sec:consolidation}). We saw that a
large body of work have been proposed to support the enrichment of the schema by
extracting types, relations and facets from diverse types of sources, from
structured sources to unstructured sources. Then, we turned our attention on the
discovery of mappings between source data and elements of the \kg schema,
focusing in particular on structured and semi-structured data sources
(Section~\ref{sec:interpretation}). Finally, we reviewed approaches that aim at
profiling a \kg, so as to provide domain experts with the necessary information
needed to supervise and validate approaches that automatize the above data
management tasks (Section~\ref{sec:summarization}).

As we saw in this chapter, there are a variety of different approaches that can
be potentially applied to support domain experts in the management of the
dataspace. However, such approaches have several limitations, which we summarize
in the following.

\concept{Lack of Support for the Extraction of Facets} 

Most of the approaches described in Section~\ref{sec:consolidation}, focus on
the extraction of types and relations, while few focused on the extraction of
facets~\cite{PascaAlfonseca,PoundEAl,FacetedPedia,douCIKM2011,kongSIGIR2013}.
Although facets are by all means relations, they serve different purposes, being
relations back-end oriented and facets end-user oriented. Moreover, to the best
of our knowledge, no work focused on extracting domain specific facets. As a
result, the current state-of-the-art supports domain experts in the enrichment
of the schema of the \kg with types, relations, and generalist facets, but not
domain specific ones.

\concept{Limited Insights About the Status of the \kg} 

Related work in the profiling of \kgs is more limited, compared to \kg schema
enrichment and mapping discovery. From the review given in
Section~\ref{sec:summarization}, emerges that state-of-the-art approaches
currently provide a profile of the \kg that is either incomplete, because
includes only a portion of data estimated to be more relevant in order to
understand and explore the structure of the \kg, or a set of statistics without
being able to capture patterns in the data, which can in principle provide
useful insights to domain experts during the execution of data management tasks
for the maintenance of the dataspace.

In summary, current state-of-the-art provides limited support for the extraction
of facets, and in particular domain specific ones, which are currently left to
nearly completely manual work by domain experts. To overcome this limitation, in
this dissertation we propose an approach to the enrichment of the \kg with
domain specific facets composed by two different steps, discussed in
Chapters~\ref{cap:consolidation} and~\ref{cap:interpretation}. In particular, we
first extract domain specific facets from structured data sources based on
existing t-to-t mappings already established by domain experts, to enforce
domain specificity. Then, we provide a domain specific interpretation of
extracted facets, by reusing relations specified by the \kg of the dataspace or
eventually by other external \kgs. While the first approach specifically
enriches the \kg with domain specific facets, the second approach reconciles the
end-user oriented representation of instances provided by facets to the back-end
oriented representation provided by relations with the benefit of tightening the
integration between the sources up and thus empowering the capability to provide
advanced data access features to end-users of the dataspace. As supervision and
validation of domain experts is crucial in this picture, in
Chapter~\ref{cap:summarization} we propose a \kg profiling approach that
provides domain experts with insights about the current status of the \kg.
In particular, our approach is capable to provide an overview of the specificity
of relations of a \kg, which constitutes vital information for domain experts in
charge of supervising the enrichment of the \kg schema with domain specific
facets and their domain specific interpretation with \kg relations.
