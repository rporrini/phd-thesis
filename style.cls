%
% CUEDthesis v1.0
% By Harish Bhanderi <harish.bhanderi@cantab.net
% Version 1.0 released 15/07/2002
%-------------------------- identification ---------------------
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{CUEDthesisPSnPDF}[2003/01/11 v1.2 CUED thesis class]
%-------------------------- initial code -----------------------

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions\relax
\LoadClass[pdftex,a4paper,twosides]{book}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[usenames, dvipsnames]{color}
\usepackage{amsmath,amssymb,amsthm,environ}
\usepackage{tabularx,booktabs}
\usepackage{pgf}
\usepackage{algorithm,algorithmicx,algpseudocode}
\usepackage{xspace}
\usepackage{emptypage}
\usepackage{multirow}
\usepackage{framed}
\usepackage{xstring}

% Font
\usepackage{palatino}
\linespread{1.15} 

% Captions and Figures
\usepackage[small,it]{caption}
\usepackage{subcaption}

%-->
%--> Google.com search "hyperref options"
%--> 
%--> http://www.ai.mit.edu/lab/sysadmin/latex/documentation/latex/hyperref/manual.pdf
%--> http://www.uni-giessen.de/partosch/eurotex99/oberdiek/print/sli4a4col.pdf
%--> http://me.in-berlin.de/~miwie/tex-refs/html/latex-packages.html
%-->
\usepackage[ pdftex, plainpages = false, pdfpagelabels, 
                 pdfpagelayout = TwoPageRight,
                 bookmarks,
                 bookmarksopen = true,
                 bookmarksnumbered = true,
                 breaklinks = true,
                 linktocpage,
%                 pagebackref,
                 colorlinks = true,
                 linkcolor = black,
                 urlcolor  = blue,
                 citecolor = black,
                 anchorcolor = green,
                 hyperindex = true,
                 hyperfigures,
                 a4paper
                 ]{hyperref} 

\pdfcompresslevel=9
    
% A4 settings
\pdfpageheight=297mm
\pdfpagewidth=210mm

\setlength{\hoffset}{0.00cm}
\setlength{\voffset}{0.00cm}

% 0.72 odd, 0.72 even
\setlength{\evensidemargin}{0.72cm}
\setlength{\oddsidemargin}{0.72cm}

\setlength{\topmargin}{1mm}
\setlength{\headheight}{1.36cm}
\setlength{\headsep}{1.00cm}
\setlength{\textheight}{20.84cm}
\setlength{\textwidth}{14.5cm}
\setlength{\marginparsep}{1mm}
\setlength{\marginparwidth}{3cm}
\setlength{\footskip}{2.36cm}

% Page header
\usepackage{fancyhdr} 
\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\markboth{\MakeUppercase{\thechapter. #1 }}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\fancyhf{}
\fancyhead[RO]{\bfseries\rightmark}
\fancyhead[LE]{\bfseries\leftmark}
\fancyfoot[C]{\thepage}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0pt}
\addtolength{\headheight}{0.5pt}
\fancypagestyle{plain}{
  \fancyhead{}
  \renewcommand{\headrulewidth}{0pt}
}

%toc separators
\usepackage{etoolbox}
\usepackage[nottoc]{tocbibind}
\newcommand{\tocseparator}{%
  \addtocontents{toc}
    {\protect\addvspace{20pt}%
     %\hrule
     \protect\addvspace{10pt}%
    }%
}
\preto\mainmatter{\tocseparator}
\preto\backmatter{\tocseparator}


% Alter some LaTeX defaults for better treatment of figures:
% See p.105 of "TeX Unbound" for suggested values.
% See pp. 199-200 of Lamport's "LaTeX" book for details.
%   General parameters, for ALL pages:
\renewcommand{\topfraction}{0.9}	% max fraction of floats at top
\renewcommand{\bottomfraction}{0.8}	% max fraction of floats at bottom
%   Parameters for TEXT pages (not float pages):
\setcounter{topnumber}{2}
\setcounter{bottomnumber}{2}
\setcounter{totalnumber}{4}     % 2 may work better
\setcounter{dbltopnumber}{2}    % for 2-column pages
\renewcommand{\dbltopfraction}{0.9}	% fit big float above 2-col. text
\renewcommand{\textfraction}{0.07}	% allow minimal text w. figs
%   Parameters for FLOAT pages (not text pages):
\renewcommand{\floatpagefraction}{0.7}	% require fuller float pages
% N.B.: floatpagefraction MUST be less than topfraction !!
\renewcommand{\dblfloatpagefraction}{0.7}	% require fuller float pages
% remember to use [htp] or [htpb] for placement
\newcommand{\A}{A}
\setcounter{secnumdepth}{2}
\setcounter{tocdepth}{2}

%% now change the chapter style
%% load up the quotchap package
\RequirePackage[avantgarde]{quotchap}

%% make the quotation appear next to the chapter number
\renewcommand\chapterheadstartvskip{\vspace*{-5\baselineskip}}

%% now change the section heading to have a line beneath it
%% load up the fancy title-style package
\RequirePackage[calcwidth]{titlesec}

\titleformat{\section}[hang]{\sffamily\bfseries}
{\Large\thesection}{12pt}{\Large}[{\titlerule[0.5pt]}]

% Setup lettrine
\usepackage{lettrine}
\setcounter{DefaultLines}{2}

% Syntax package
\usepackage[rounded]{syntax}
\AtBeginDocument{\catcode`\_8}

% Bullet
%\definecolor{bulletColor}{rgb}{0,0,0}
%\renewcommand{\labelitemi}{\color{bulletColor}\tiny$\blacksquare$}

%ITEMIZE
\let\olditemize\itemize
\renewcommand{\itemize}{
  \olditemize
  \setlength{\itemsep}{3pt}
  \setlength{\parskip}{3pt}
  \setlength{\parsep}{3pt}
}
