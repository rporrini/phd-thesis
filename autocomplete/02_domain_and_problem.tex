\section{Problem Definition}\label{sec:domain-and-problem}

Consistently with the terminology introduced in Chapter~\ref{cap:dataspaces}, an
offer $o$ (i.e., a named instance of the \kg) is associated with one
lexicalization $\thelexicalization(o)$ that shortly describes it in natural
language (e.g., the string \quoted{Samsung Galaxy Tab 16 GB Android 2.2}). An
offer is instance of exactly one \kg type $\namedtype$ (e.g., $\named{Tablet}$)
and it is characterized by a set $\dsfacets{}$ of \textit{faceted assertions}
describing the technical features of the offer, which are represented as
\begin{formula*}
	\mfacet(o,~\mvalue) 
\end{formula*}
where $\mvalue$ is the \emph{facet value} (e.g., $\named{mp3}$, which can be
for example a value of the faceted assertion
$\named{supportedAudio}(o,\named{mp3})$).
An offer is thus represented as a triple
\begin{formula*}
	o~=~< \thelexicalization(o),~\namedtype,~\dsfacets{} > 
\end{formula*} 
where $\thelexicalization(o)$ is the offer's lexicalization, $\namedtype$ is a
the type whose $o$ is an instance of, and
$\dsfacets{}=\{\mfacet_1(o,\mvalue_1),...,\mfacet_n(o,\mvalue_n)\}$ is a
set of faceted assertions.

\concept{The Autocompletion Problem}
A result-driven \as presents a set of $k$ results that are most relevant to 
keyword-based query typed by the user, by processing the query at each
interaction of the user with the system. Observe that every \textit{query
fragment} (a query where the last keyword typed in by the user is a string
representing a word fragment, e.g., \quoted{smartphone nok}) is considered an
input query by the \as.

In order to be effective and perceived as instantaneous the autocompletion
operation must be completed in a relatively short time span (i.e., maximum 100
ms)~\cite{HCI}: this constraint represents a serious limit to the possibility of
exploiting complex techniques to improve the accuracy and coverage
of the autocompletion results. Since there is an element of distraction caused
by User Interface interrupts, which can overcome the user while typing the
query, the high quality of the data retrieved by the \as must justify the
distraction caused to the user by the \as ~\cite{UIDIST}. An \as has therefore
to fulfill both \textit{efficiency} and \textit{effectiveness} requirements, and
since an improvement in the latter usually has a negative impact on the former,
finding a good trade-off between efficiency and effectiveness is a major goal
for matching algorithms developed in this context.

\textit{Targeted} queries (e.g., \quoted{samsung galaxy tab}) are submitted by
users that look for a specific product; the keywords used in this case usually
point to specific terms used in offer lexicalization (e.g., brand and model).
These queries can be handled using well known exact (e.g., prefix matching) and
approximate matching techniques~\cite{NavarroStringMatching2002}, which can
support provide results also when query terms are
misspelled~\cite{EXTENDING-AUTOCOMPLETE,EFFICIENT-FUZZY-SEARCH}.
However, even approximate matching techniques would be unsuccessful when an \exq
query is submitted. Consider a query such as "Tablet PC with Office Mobile"
which refers to a class of products rather than to a specific product.
In this query, neither the keywords nor any string approximately matching these
keywords occur in the offers' lexicalization.

Instead, many keywords used in \exq queries describe product types (e.g.,
$\named{TabletPC}$) and/or technical features. These pieces of information are
often available from types and facet values. Furthermore, the distinction
between \taq and \exq queries is not sharp; in fact queries such as
\quoted{samsung tab office mobile} can contain terms referring to specific
products as well as to generic facet values; we will refer to these queries as
\hyq queries. Using matching algorithms that specifically leverage
well-structured representation of offers provided by the \kg (i.e., semantic
matching) can therefore play a crucial role in effectively answering \exq (and
\hyq) queries. One of the greatest challenges for a result-driven \as seamlessly
processing all the above types of queries consists in combining syntactic and
semantic matching so that the capability of handling more types of queries
(improving the coverage of AS) does not lower the quality of the results
returned when \taq queries are submitted.
