\section{Comparison with Prior Art}\label{sec:relworks}

The problem of developing an effective and efficient \as has been analyzed in
the literature from many different perspectives; nonetheless, \as applications
in the \ecomm area are rarely described in the scientific literature and they
are often protected by patents (see e.g.~\cite{AMAZON-AUTOCOMPLETE}). After an
analysis of the features of the available commercial \as, we found that the only
system that explicitly uses types and facets to support \emph{query-driven}
autocompletion is Bing Shopping: when a query matches one or more type the
system helps the user to refine the query by adding (i) types (e.g.,
$\named{Mp3Players}$), and (ii) constraints over facet values. Also our
\emph{result-driven} approach matches keywords with relevant types and
facet values. However, our approach is fully automatic and uses these matches to
suggest meaningful results instead of suggesting query refinements.

In general, autocompletion techniques have been developed both for
\textit{finding information} and for \textit{helping the user to express search
queries}. In the field of Data Management the autocompletion problem has been
interpreted as the problem of matching a query fragment with searchable data
represented by strings, and different techniques of both exact and approximate
string matching~\cite{NavarroStringMatching2002}, such as \textit{prefix
matching}~\cite{OUTPUT-SENSITIVE}
\textit{q-grams}~\cite{EXTENDING-AUTOCOMPLETE} and \textit{trie} based
matching~\cite{EFFICIENT-FUZZY-SEARCH} have been proposed. Our approach can
potentially leverage any of these techniques. However, the experimental results
presented in Section~\ref{sec:evaluation} show that an approach to
autocompletion that employs only syntactic matching is not able to deal with
\exq and \hyq queries in a satisfactory way.

Autocompletion techniques have also been studied in the field of Semantic Search
(see~\cite{SEMANTIC-SEARCH-SURVEY}). Hyv{\"o}nen et al. formalize the
autocompletion problem as the problem of matching a query to \kg
types~\cite{SEMANTIC-AUTOCOMPLETION}. From this perspective, the \as proposed in
this dissertation can be classified as a form of \textit{Semantic Autocompletion
Search}. Autocompletion in this area have been introduced to suggest types from
is-a (or part-of) hierarchies~\cite{SEMANTIC-CONCEPT-SELECTION}, and to retrieve
data characterized using different types~\cite{VENTURI}. Semantic autocompletion
techniques are significantly related to the approach proposed in this
dissertation, since COMMA exploits two different orthogonal representations of
data to handle several query types. However, the above mentioned approaches to
semantic autocompletion do not deal with the problem of ranking the proposed
results (a full list of matches is returned).

The use of semantics in search-related tasks tasks has been studied by different
research communities~\cite{SEMANTIC-SEARCH-MIKA}, as testified by recent
workshops hosted by different international conferences~\cite{ESAIR, JIWES,
SEM-SEARCH}. In this field large body of work has been devoted to the so called
\emph{Entity Search} (ES) task, that is the search for instances of a \kg. ES is
also known in traditional Information Retrieval community as \emph{Ad-Hoc Object
Retrieval}~\cite{OBJECT-RETRIEVAL}. The goal of ES techniques is to retrieve and
rank instances from large \kgs such that they are more relevant to a
keyword-based query submitted by users. The best performing
approaches~\cite{MIKA-BM25F, BM25F} use an adaptation of the BM25F ranking
model~\cite{BM25F-FOUNDATION} that considers different sections of documents
representing an entity, each one characterized by specific relational assertions
and assigns different weights depending on the assertion where the terms
matching the query appear, (e.g., assertion of the $\named{\escaped{foaf:name}}$
or $\named{\escaped{rdfs:label}}$ relations). A more recent approach models the
instances using an improved generative language modeling
framework~\cite{LANGUAGE-MODEL} in order to preserve the semantics associated
with instances without sacrificing retrieval effectiveness~\cite{BALOG}. Despite
being close to our approach, ES techniques do not consider time efficiency
issues, such as those that must be considered when implementing an \as and do
not leverage the co-occurrence of different instance representations (which
can be mapped to facets and types in our domain) to rank results. Moreover,
BM25F considers different sections of documents representing an instance, each
one defined by values of specific relations. However, we consider all the
faceted assertions (relational assertions in a ES context) and we do not assign
different weights to specific relations.

The task of helping user in formulating queries to search engines has been also
studied in the \textit{Interactive Query Expansion} (IQE) research area. In this
context autocompletion is interpreted as the problem of retrieving the terms
that, when added to the already expressed query, improve the precision and
recall of the results. The suggested terms can be retrieved considering their
conditional probability of belonging to the same topic that the previously
specified terms belong to~\cite{TOPIC-BASED}. Other ways to determine a set of
terms to suggest is to consider reciprocal semantic relations such as
\textit{synonymy}, \textit{hyponymy} or
\textit{hypernymy}~\cite{EXPANSION-WORDNET} or to consider user behaviour in
submitting multiple queries~\cite{CONTEXT-AWARE}. A more recent approach,
acknowledging the limits of purely syntactic approaches, uses query logs to
evaluate the similarity between query contexts~\cite{QUERY-AUTOCOMPLETION}. A
query context consists of all the query terms previously stated by the
user plus the initial letters of the term that the user is currently typing. All
of these approaches are intrinsically different from the COMMA approach, since
they all suggest queries (not results). Moreover COMMA deals with
structured data instead of textual documents.
