\section{Overview}\label{sec:introduction}

As pointed out in Chapter~\ref{cap:dataspaces}, a rich and domain specific
representation allows a DI application to provide advanced search and browse
features over the \kg instances. This chapter describes a case study from the
eCommerce domain where \kg types and facets are leveraged in order to provide an
advanced Product Autocomplete feature for eMarketplaces. The Product
Autocompletion system, which is named COMMA, described in this chapter is an
example of the impact of the contributions of this dissertation, evaluated on
the real world scenario of an Italian eMarketplace.

This chapter is organized as follows: Section~\ref{sec:autocompletes} frames the
contribution described in the chapter in the context of the eCommerce domain.
The autocompletion problem and the is discussed in
Section~\ref{sec:domain-and-problem}; the matching algorithm that constitutes
the core of COMMA and that leverages domain specific representation of
instances is described in Section~\ref{sec:algorithm}; details about the
implementation and the deployment in a real world scenario of the proposed
Product Autocomplete system are given in Section~\ref{sec:deployment};
experiments carried out to evaluate the system are discussed in
Section~\ref{sec:evaluation}, and a summary of the contribution described in
this chapter is given in Section~\ref{sec:autocomplete-conclusion}.

\section{Autocomplete Interfaces in eCommerce}~\label{sec:autocompletes}

An ordinary eMarketplace typically performs a matchmaking activity between
search queries submitted by the users and a set of products. Maximizing the
\emph{accuracy} and \emph{coverage} of the matchmaking algorithm over search
queries is a crucial aim that a successful eMarketplace must achieve. Providing
an autocompletion interface is a recent but already established approach to
improve product search features. For particular DI applications such as
CSE (Comparison Shopping Engines), providing an autocompletion feature
\textit{as-a-service} to small/medium client eMarketplaces (i.e., data sources
of the dataspace of a CSE, consistently with terminology from
Chapter~\ref{cap:dataspaces}), often characterized by a poor retrieval features,
represents an interesting business opportunity.

An autocompletion interface can perform two types of autocompletion operations:
it can complete the \emph{query} that the user is expressing by proposing a set of
queries that are most relevant and reasonable to the query fragment already
typed by the user; this type of autocompletion can be called
\emph{query-oriented}~\cite{CONTEXT-AWARE, QUERY-AUTOCOMPLETION,
EXPANSION-WORDNET, TOPIC-BASED}. The autocompletion interface can instead preview
the \emph{results} of the query fragment that is being typed by the user; this
type of autocompletion can be called
\emph{result-oriented}~\cite{OUTPUT-SENSITIVE, EXTENDING-AUTOCOMPLETE,
EFFICIENT-FUZZY-SEARCH, SEMANTIC-CONCEPT-SELECTION, VENTURI}.

Most of the autocompletion interfaces proposed so far in the \ecomm domain
(e.g., Amazon, and Bing, Google and Yahoo Shopping) are based on query-oriented
approaches. In fact, all the above CSEs integrate product offers coming from
several eMarketplaces, achieving an almost complete coverage of the products
available on the market. Remarkably, the advanced search features provided by
such CSE are based on the structured information provided by their \kgs.
However, when a query is submitted to a single small/medium-sized eMarketplace
(i.e., a data source of the CSE), correctly completing a query that would not
lead to any or poorly ranked results, would not be effective. In this cases,
result-oriented autocompletion can be highly effective, by previewing only
product offers available in the eMarketplace, and by providing an insight on the
internal working of the search functionality.
We focus on the latter scenario, investigating a novel matching and ranking
technique to support result-oriented autocompletion for small/medium
eMarketplaces.

One of the main problems to address for an Autocompletion System (\as) is to
seamlessly handle the different types of query that can be submitted by a user.
Autocompletion techniques that adopt string-based matching algorithms and
consider offers' titles and descriptions, can provide accurate results when
users submit queries targeted to specific products (e.g., \quoted{Samsung
i7500}), and they can be made error-tolerant by adopting approximate matching
methods~\cite{NavarroStringMatching2002,EXTENDING-AUTOCOMPLETE,EFFICIENT-FUZZY-SEARCH}
over misspelled terms (e.g., \quoted{Samsong 7500}). However, user queries are
often not really aimed at describing a specific product that is being searched
but they are rather aimed at \emph{exploring} the set of available products or
offers, looking for products of a given category (e.g., $\named{MobilePhone}$)
or characterized by specific features (e.g., $\named{Android}$ as value of the
facet $\named{operatingSystem}$). Sometimes, more targeted and more generic
keywords are even mixed in a \hyq type of query (e.g., \quoted{Samsung
Android}).

Product types from the \kg of the CSE and the most significant product features
(i.e., facets) represent an invaluable source of information for handling \exq
and \hyq queries. Indexing the terms contained in this representation
enables an autocompletion interface to provide results also for \exq and \hyq
queries but obtaining a relatively poor ranking, as shown by experiments
discussed later on. For sake of clarity, we will call semantic techniques any
matching technique aimed at handling this representation (types and facets)
in a different way from pure textual descriptions, in the vein of research
carried out in semantic
search~\cite{SEMANTIC-SEARCH-SURVEY,SEMANTIC-SEARCH-MIKA}.
