\section{COMMA}\label{sec:algorithm}

The autocompletion approach proposed in this chapter and implemented by COMMA
consists of three main phases as sketched in Figure~\ref{fig:algorithm}:
\begin{itemize}
\item \textbf{indexing (offline)}: the structured information from the \kg that
characterizes the offers is indexed along three different descriptive
dimensions:
lexicalizations, types and faceted assertions;
\item \textbf{filtering (online)}: given a query fragment $q$ submitted by the
user, the matching algorithm applies three filters, one for each descriptive
dimension; one filter selects offers whose \emph{lexicalization} match with the
query (syntactic matching); one filter selects offers whose \emph{types}
match with the query (semantic matching); one filter selects offers whose
\emph{facet values} match with the query (semantic matching); the results of
the three filters are combined by means of a set union returning a final set of
matching offers;
\item \textbf{ranking (online)}: several scoring methods are applied to rank
the offers selected by the filters; local scoring methods evaluate the relevance
of each offer to the query by considering different criteria such as their
popularity, the strength of the syntactic match and the semantic relevance; the
local scores are combined in a final relevance score, which is used to rank the
offers and define the list of top-k relevant offers.
\end{itemize}

\begin{figure}[t]
	\centering
	\includegraphics[width=0.8\columnwidth]{autocomplete-images/process.pdf}
	\caption{Overview of the matching algorithm.} 
	\label{fig:algorithm}
\end{figure}

Ranking is applied after filtering because most of the scores applied in ranking
reuse the results of matches discovered during filtering; moreover, this
approach allows to limit the number of items to which scoring functions are
applied, achieving a better efficiency.

\subsection{Indexing}

In order to index the lexicalizations, types and faceted assertions associated
with each offer, three inverted indexes are built: $I^{\thelexicalization}$,
$I^{T}$ and $I^{\dsfacets{}}$ respectively denote the lexicalization, type and
facet indexes. In the facet index $I^{\dsfacets{}}$ only facet values (i.e., the
objects of the the domain specific relation conceptualized by the facet) are
indexed under the assumption that the names of the facets are not very
significant for searches (e.g., a user is more likely to search for
\quoted{samsung phone}, instead of \quoted{brand samsung phone}).

All the indexes are built by extracting every term occurring in the indexed
string (respectively the lexicalization, the type identifier and the facets'
values); we tokenize the strings representing codes (e.g., from \quoted{Nokia
c5-03} we extract the three terms \quoted{nokia}, \quoted{c5} and \quoted{03});
terms are stemmed and stop-words are removed. We call \emph{lexicalization
terms}, \emph{type terms} and \emph{facet value terms} the elements belonging
respectively to the sets $I^{\thelexicalization}$,  $I^{T}$ and $I^{\dsfacets{}}$.

\subsection{Syntactic and Semantic Filtering}
Let $O^q_{SYN}$, $O^q_{TYPE}$, and $O^q_{FAC}$ be the set of offers whose
textual descriptions (lexicalizations, in our case), types and facet values respectively
match a query $q$. The set $O^q$ of offers matching a query $q$ can be defined
as follows:
\begin{formula*}
	O^q = O^q_{SYN} \cup O^q_{TYPE} \cup O^q_{FAC}.\label{def:total}
\end{formula*}

In other words, all the offers that match the input query along at least one
dimension are selected in the filtering phase. The rest of this section explains
how each matching set is obtained.

\concept{Syntactic Filters}
The set $O^q_{SYN}$ of the offers that syntactically match against a query
fragment is computed by performing a set union of the results of two filters: a
prefix-based (\emph{pref}) string matcher, and an approximate string matcher
(\emph{asm}) based on normalized edit distance
(\emph{nedit}~\cite{NavarroStringMatching2002}) and a filtering threshold
$th^{asm}$. We combine the results of these two filters to handle keyword
fragments together with complete keywords with possible misspellings. In fact,
using only an asm matcher with a reasonably high threshold would lead to poor
results when matching short prefixes of long terms.

A lexicalization term $l\in I^{\thelexicalization}$ \textit{pref-matches} an
input keyword fragment $k$, iff $k$ is a prefix of $l$; a lexicalization term
$l\in I^{\thelexicalization}$ \textit{asm-matches} an input keyword fragment
$k$, iff $nedit(k,l)\leq th^{asm}$. An offer $o$ pref-matches (asm-matches,
respectively) an input keyword fragment $k$, iff there is at least a
lexicalization term $l\in o$ such that $l$ pref-matches (asm-matches) $k$.
An offer $o$ pref-matches (asm-matches, respectively) a query
$q=\{k_{1},...,k_{n}\}$ if and only if $o$ pref-matches (asm-matches)
\textit{every} keyword $k_{i}\in q$, with $1 \leq i\leq n$. The set $O^q_{SYN}$
of offers syntactically matching an input query $q$ is defined as the union of
of all the offers pref-matching $q$ and all the offers asm-matching $q$.

\begin{example}
Suppose to have a query $q$=\quoted{nakia c}, and two offers
named \quoted{Nokia c5-03} and \quoted{Nokia 5250 Blue}; \quoted{Nokia c5-03}
syntactically matches $q$ because \quoted{nakia} asm-matches the keyword
\quoted{Nokia} and \quoted{c} asm-matches the keyword fragment \quoted{c}
(instead, observe that this is not a pref-match because
\quoted{nakia} does not pref-match \quoted{Nokia}); instead \quoted{Nokia 5250 Blue}
does not syntactically match  $q$ because there is no term in the offer
lexicalization pref-matching or asm-matching the keyword fragment \quoted{c}.
\end{example}

\concept{Semantic Filters} \label{subsec:semfilter}
The goal of the semantic filter is to identify a set of offers that are
potentially relevant to a query where some type and/or facet value occur as
keywords. We define two semantic filters, one based on type matching
(\emph{type-matching}), and one based on facet value matching
(\emph{fac-matching}). Before applying semantic filters, stemming and stop-words
removal are applied to a query $q$ submitted by the user, obtaining a new query,
denoted by $\overline{q}$ to which filters are applied.

Intuitively, the \textit{type-matching} filter returns all the offers that are
instance of some type matching a query $\overline{q}$. This set
$O^{\overline{q}}_{TYPE}$ is defined as follows. A type $\namedtype$ matches a
keyword $k$ iff there exists a type term $i\in I^{C}$ associated with
$\namedtype$ such that $i=k$ (remember that type names can contain more terms,
and that an indexed type term can be associated with more types); an offer $o$
cat-matches a query $\overline{q}=\{k_{1},...,k_{n}\}$  iff its an instance of a
type $t$ that matches at least one keyword $k \in \overline{q}$.

Intuitively, the \textit{fac-match} filter returns all the offers characterized
by some facet value matching a query $\overline{q}$; this set
$O^{\overline{q}}_{FAC}$ is defined as follows. A facet value $\mfacet(o,v)$
matches a keyword $k$ iff there exists a facet value term $i\in I^{\dsfacets{}}$
associated with $\mvalue$ such that $i=k$ (remember that only facet values are
considered in the matching process); an offer $o$ fac-matches a query
$\overline{q}=\{k_{1},...,k_{n}\}$ iff it is annotated with \textit{at least}
one facet $f$ that matches at least one keyword $k \in \overline{q}$ (remember
that an offer can be characterized with several facet values).
Given a query $q$, we also identify the sets $T^q$ and $\dsfacets{q}$,
respectively representing the set of all the types and facet values that match
with at least a keyword of $k$.

\begin{example}
Suppose to have a query $q$=\quoted{players with mp3}, which is
transformed in the query $\overline{q}$=\quoted{player mp3} after stemming and
stop-words removal; since the type $\named{Mp3Players}$ matches the
query, all the offers that are instances of this type are included in the
$O^{\overline{q}}_{TYPE}$ set; since the value of several faceted assertions
such as $\named{compression}(o, \named{mp3})$, $\named{audioFormat}(o,
\named{mp3})$, $\named{recordingFormat}(o, \named{mp3})$, match $\overline{q}$
as well, every offer associated with any of these facets is included in the
$O^{\overline{q}}_{FAC}$ set.
\end{example}

Observe that while an offer is required to syntactically match all the terms in
the query, an offer can semantically match only one keyword in order to be
considered in the semantic matches for that query. The reason for such a
difference is that, while for precise queries targeted to retrieve offers based
on their lexicalizations the user has more control on the precision of the
results (under the assumption that the user knows what he/she is looking for),
for \exq queries containing generic type and facet terms we want to consider
all the offers potentially relevant to these vaguer terms. Although the
semantic match significantly expands the set of matching offers, the ranking
process will be able to discriminate the offers that are more relevant to the
given query.

\subsection{Ranking and Top-k Retrieval}

Several criteria are used to rank the set of matching offers. Each criterion
produces a local relevance score; all the local scores are combined by a
\textit{linear weighted function} which returns a global relevance score for
every matching offer; the top-k offers according to the global score are finally
selected and presented to the user.
 
\concept{Basic Local Scores}
Three basic local scores are introduced to assess the relevance of an offer with
respect to a query submitted by a user.


\textbf{Popularity}: $pop(o)$ of an offer $o$ is a normalized value in the
range $[0,1]$ that represents the popularity of an offer $o$ based on the
number of views the offer received. An absolute offer popularity score is
assessed offline analyzing log files and it is frequently updated; the absolute
popularity score is normalized at runtime by distributing the absolute
popularity of the offers matching the input query into the range $[0,1]$.

\textbf{Syntactic relevance}: offers retrieved by exact (prefix) matching should
be rewarded \wrt the offers approximately matched (e.g., because the order of
digits makes a difference in technical terms like product codes); we therefore
define a syntactic relevance score that discriminates between the offers matched
by the two methods. The syntactic relevance $syn_q(o)$ of an offer $o$ \wrt a
query $q$ assumes a value $m$ if $o$ is a pref-match for $q$, $n$ if $o$ is a
asm-match for $q$, and $0$ elsewhere, where $m > n$.

\textbf{Semantic relevance}: semantic relevance is based on the principle that
the more matching facet values occur in an offer, the more relevant this offer
is \wrt the query. Semantic relevance does not consider types because the number
of offers that are instances of specific types is generally high. The semantic
relevance $sem_q(o)$ of an offer $o$ to an input query $q$ is computed as the
number of faceted assertions $\dsfacets{o}$ associated with $o$ and matching
with $q$, normalized over the total number of the faceted assertions whose value
matches the query $\dsfacets{q}$, according to the following formula:
\begin{formula}
sem_q(o) = \cfrac{|\dsfacets{q} \cap \dsfacets{o}|}{|\dsfacets{q}|}.
\end{formula}

\concept{Intersection Boost and Facet Salience}
When a user submits a \taq query (see Section~\ref{sec:domain-and-problem}), the
basic local scores introduced above perform quite well. When the matching is
driven mostly by syntactic criteria, the accuracy of the syntactic match and the
popularity become key ranking factors. However, \exq queries are usually more
ambiguous (each single keyword can match more types and more facet values at the
same time, and matches for each keyword are combined by means of set union); in
these cases, the semantic relevance score based on the matching facet values can
be too weak, with matching types not even considered in the score.

\begin{example}
All the offers that instances of the type \quoted{vacuum cleaner}
or annotated with the faceted assertion $\named{typology}(o, \named{robot})$
match the query \quoted{robot vacuum cleaners}; however,
some of these offers are about vacuum cleaners having $\named{typology}(o,
\named{robot})$, while others are associated with kitchen tools having
$\named{typology}(o, \named{robot})$. The ranking scores defined so far do
not discriminate among the two kinds of offers because they do not consider
their types; intuitively, offers describing vacuum cleaners of type robot
should be better rewarded because they match on both the type and facet
value dimensions.
\end{example}

Considering the general case when offers match the query according to more than
one dimension, we define a new local score called \textit{Intersection Boost}.
This local score aims at rewarding offers that occur in many specific matching
sets selected by different filters. Before formally defining the Intersection
Boost score we introduce a new filter in addition to previously defined ones.
In order to better assess the relevance of the offers, and in particular of the
offers that have been matched by semantic filters, we introduce a filter which
is based on of \textit{Facet Domain Specificity}. Intuitively we define a
special set of offers that consists of the ones characterized by assertions
about facets particularly \textit{specific to} some types (i.e., domains)
matching the query submitted by the user. A facet is specific to a type if it is
highly likely to characterize offers that are instances of that type.

Formally, the specificity of a facet $\mfacet$ to a type $\namedtype$ is defined
by a function that represents the conditional probability of the occurrence of a
facet $\mfacet$ in the set of offers $O^\namedtype$ belonging to the type $t$;
given also the set $O^f$ of the offers characterized with the facet
$\mfacet$, $spec$ is defined by the following formula:
\begin{formula}
spec(\mfacet, \namedtype) = \cfrac{| O^{\mfacet} \cap
O^{\namedtype}|}{|O^\namedtype|}.
\end{formula}
The set $\hat{\dsfacets{}}^q$ of facets specific to a query $q$ is defined as
the set of facets matching with $q$ whose specificity to at least one type
matching $q$ is higher than a given threshold $l$:
\begin{formula}
	\hat{\dsfacets{q}} = \{ \mfacet \in \dsfacets{q} \ | \ \exists \namedtype \in
	\namedtype^q \ : \ spec(\mfacet,\namedtype)\geq l\}.
\end{formula}
Given a query $q$, the set $O^q_{SPEC}$ of offers specific to $q$ is defined as
the set of offers that fac-match some facet $\mfacet \in
\hat{\dsfacets{q}}$.

Now we can introduce the {Boosting with Facet Domain Specificity} (BFS) score.
Let $M$ be the set of all the matching filters defined above (i.e., syntactic,
type and facet match and Facet Domain Specificity) and $M^o_q \subseteq M$ the
subset of all matched filters for the offer $o$ given the query $q$; the
$boost_q(o)$ function can be formally defined as follows:
\begin{formula}
	boost_q(o) = \cfrac{|M^o_q|}{|M|}.
\end{formula}

\begin{example}
Consider the query $q$=\quoted{robot vacuum cleaner}. Suppose that the offer
$o_1$ with lexicalization $\thelexicalization(o_1)$ = \quoted{i-robot roomba},
of type $\named{VacuumCleaners}$ and characterized by a faceted assertion
$\named{typology}(o, \named{robot})$ is matched by syntactic, type, facet and
Domain Specificity. Therefore, since $o_1$ is matched by 4 out of 4 filters,
$boost_q(o_1) = 4/4 = 1$. On the other hand, suppose that the offer $o_2$ with
$\thelexicalization(o)$ = \quoted{kenwood cooking chef kitchen machine},
instance of the type $\named{KitchenTools}$ characterized by the faceted
assertion $\named{typology}(o, \named{robot})$, is only matched by facet
filter. Thus, $boost_q(o_2) = 1/4 = 0.25$.
\end{example}

\concept{Global Matching Score and Ranking}
The global scoring function can therefore be defined considering the
Intersection Boost with Domain Specificity. Formally, the \textit{global score} can
be defined by a function $score_q$:
\begin{formula}\label{global-score}
score_q(o) & = w_{pop} \cdot pop(o) + w_{syn} \cdot syn_q(o) + \\
& + w_{sem} \cdot sem_q(o) + w_{boost} \cdot boost_q(o) \nonumber
\end{formula}
where all $w_{i}$ with $i\in{pop,syn,sem,boost}$ are in the range $[0,1]$ and sum up to 1.

\concept{Top-k Selection and Presentation}\label{sec:presentation}
The output of the matching algorithm is a ranked set $O^q$ of offers matching a
query $q$. To effectively present $O^q$ to a user as result of an autocompletion
operation we need to present a subset of the results, namely $O^{q,k}$, selected as the top-$k$
ranked offers according the \emph{global score} function defined in
Equation~\ref{global-score}. It must be noted that due to UI constrains, the
list of results of the autocompletion operation is necessarily small and should
only contain high quality completions~\cite{EARLY-INTERFACE, UIDIST}.

A straightforward way to present the set $O^{q,k}$ of the top-k results is to
display a flat list of offers ordered by rank (\textit{presentation by rank}).
However, it has been shown that grouped presentation of autocompletion results
helps users to perform search tasks more
quickly~\cite{HIERARCHY-QUERY-EXPANSION, GROUPING-STRATEGIES}.
Thus, we defined a simple and effective \textit{presentation by type}
strategy that presents offers in $O^{q,k}$ grouped by type. Offers within
each group are ordered by rank and groups are ordered by the \emph{score} of
their top ranked offer. An example of the application of this grouping strategy
is depicted in Figure~\ref{fig:category-ranking}.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\columnwidth]{autocomplete-images/category-ranking.pdf}
	\caption{The type grouping strategy.} \label{fig:category-ranking}
\end{figure}