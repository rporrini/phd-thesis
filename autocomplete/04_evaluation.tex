\section{Implementation and Deployment}\label{sec:deployment}
COMMA has been implemented using the Java programming language, exploiting
several functions of the Lucene search engine
library\footnote{\url{http://lucene.apache.org/}}. As shown in
Figure~\ref{fig:architecture}, the system accepts the
AJAX\footnote{\url{http://www.w3schools.com/ajax/default.asp}} calls from the
client Web page and supplies results of the matching operation on offers in
JSON\footnote{\url{http://www.json.org/}} format. This deployment allowed for
the provisioning of the autocompletion functionality as a remote service instead
of requiring a deployment on the eMarketplace server. In this deployment
configuration the size of the list of offers presented to the user as results of
the autocompletion operation was set to $7$, a good trade-off between
compactness and coverage of the suggested results~\cite{Saaty03}.

\begin{figure}[t]
\includegraphics[width=1\columnwidth]{autocomplete-images/architecture.pdf}
	\caption{COMMA evaluation deployment scenario.} \label{fig:architecture}
\end{figure}

\section{Evaluation}\label{sec:evaluation}

We conducted several experiments to evaluate COMMA. In order to investigate the
\textit{effectiveness} of COMMA we evaluate the accuracy of the matching and
ranking algorithms by 1) comparing COMMA with a baseline, defined by a syntactic
matching approach based on the well-known TF-IDF weighting
scheme~\cite{SaltonTfIdf88} (TF-IDF from now on), 2) analyzing the behavior of
COMMA under different parameter settings, and 3) studying the impact of
different results presentation strategies. In order to evaluate the
\textit{efficiency} of COMMA, we investigate whether the \as satisfies the
strict time constraints required by the domain, and to which extent it can scale
up to handle a significant amount of data. Finally, we conducted experiments in
a real world scenario, deploying COMMA to an Italian eMarketplace and measuring
the impact on the \textit{conversion rate}, i.e., the ratio of users that
complete a purchase over the users that visit the eMarketplace.

The experiments were run on an Intel Core2 2.54GHz processor laptop, 4GB RAM,
under the Ubuntu Linux Desktop 12.04 32bit operating system. For the real-world
experiments only, COMMA has been deployed to an Ubuntu Linux Server 10.04 LTS
64bit, 2GB RAM, 2GB storage, virtualized machine.

\subsection{Effectiveness}
In order to evaluate the effectiveness of our approach we evaluate the accuracy
of the results ranked by COMMA with respect to an ideal rank, adopting the
Normalized Discounted Cumulative Gain (nDCG) measure~\cite{DCG}. Discounted
Cumulative Gain (DCG) is a well-known measure of effectiveness for ranking
algorithms adopted in Information Retrieval. The rationale of the measurement is
that highly relevant documents (offers, in this case) should appear in the top
positions of the list of results and, more generally, that the list should be
ordered according to the ideal relevance of the documents to a query. The nDCG
measure is obtained by normalizing the DCG measure according to the ideal DCG
measure of the result list. Intuitively, a higher nDCG value corresponds to a
better agreement between the results proposed by the system and an ideal
ordering based on human judgments.

\concept{Experimental Setup}
To the best of our knowledge there is no available benchmark and dataset defined
to evaluate a result-oriented \as, and in particular considering the types of
queries addressed by COMMA. We used a dataset from a portion of the real
\kg of the Italian CSE TrovaPrezzi (see Section~\ref{sec:motivating-example}),
which contains 5594 offers belonging to 92 different types and characterized with 463
different facets. The ideal ranks for 30 keyword-based queries (13 \taq, 13 \exq
and 4 \hyq queries) have been defined by collecting graded relevance judgments
(adopting a 3-point Liker Scale from 0=Bad, to 2=Excellent) from 10 average
users and aggregating the individual judgments into mean graded relevance
scores. 

All the queries have been manually constructed, considering frequent types of
queries made by Italian eCommerce users. For example, the query \quoted{nakon
coolpix} (notice the misspelling) is classified as \taq, the query \quoted{nokia
wifi} is classified as \exq, and the query \quoted{lcd samsung} is classified as
\hyq because all the keywords can refer both to the name and the facets of an
offer. For each query the user was asked to fill in a questionnaire assigning a
graded relevance judgment (adopting the previously introduced scale) to a set of
distinct 20 offers from the dataset. This set contained 15 offers selected by a
pool of experts and was enriched with the top 5 ranked offers according to
TF-IDF\footnote{An archive containing the dataset used for our experiments,
including the questionnaire and the user judgments can be downloaded from
\url{http://www.lintar.disco.unimib.it/COMMA2012/query_set_extended.zip}}, which
did not appear in the list selected by the experts already.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.5\textwidth]{autocomplete-plots/ndcg-overall.pdf}
	\caption{nDCG for the 3 different configurations of COMMA compared to
	the baseline at different rank thresholds (all query types).}
	\label{fig:ndcg-overall}
\end{figure}

\concept{Comparison with a Baseline}
In the first experiment we consider as baseline the naive solution that can be
proposed to the problem of responding to both \exq and \taq queries:
lexicalizations, types and facet values are indexed and the offers that have at
least one term in common with the query (considering also misspellings) are
selected and ranked according to a well-known weighting function. Here we use
the result obtained using TF-IDF. The baseline is compared to three different
configurations of COMMA: simple scoring, that is without ranking improvements
(i.e., the \textit{Intersection Boost} and \textit{Facet Domain Specificity}),
with \textit{Intersection Boost} and without \textit{Facet Domain Specificity}
(COMMA-B), and with both \textit{Intersection Boost} and
\textit{Facet Domain Specificity} (COMMA-BFS). Figure~\ref{fig:ndcg-overall}
shows that all the configurations of COMMA that include at least one method for
ranking improvement perform better than the baseline, at all the considered rank
thresholds, while TF-IDF approach outperforms the basic COMMA algorithm. In the
following we discuss more in depth the behavior of COMMA when different types of
queries are considered.

Using both \textit{Intersection Boost} and \textit{Facet Salience} in the global
ranking function leads to significant improvements, especially if we consider
the results for \exq queries, plotted in
Figure~\ref{fig:explorative-queries-all}. The accuracy in ranking results in the
top positions, which is noticeable for any type of query, is even more
remarkable for \exq queries; this behavior is particularly beneficial to an \as,
which presents a limited number of results to the user. Rewarding the offers
selected by more than one filter (\textit{Intersection Boost}), and particularly
relevant because of the specificity of their faceted assertions to the query
enables COMMA to be sufficiently selective achieve a high performance for higher
rank thresholds.

\begin{figure}[t]
	\centering
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{autocomplete-plots/explorative-queries-all.pdf}
		\caption{Only \exq queries.}
		\label{fig:explorative-queries-all}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{autocomplete-plots/notmixed-queries-all.pdf}
		\caption{All query types except \hyq queries.}
		\label{fig:notmixed-queries-all}
	\end{subfigure}
	\begin{subfigure}[c]{\textwidth}
		\centering
		\vspace{1.3cm}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{autocomplete-plots/targeted-queries-all.pdf}
		\caption{Only \taq queries.}
		\label{fig:targeted-queries-all}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\includegraphics[width=\textwidth]{autocomplete-plots/mixed-queries-all.pdf}
		\caption{Only \hyq queries.}
		\label{fig:mixed-queries-all}
	\end{subfigure}
	\caption{nDCG for the 3 different configurations of COMMA compared to
	the baseline at different rank thresholds.}
\end{figure}

In general, the COMMA configuration with simple scoring performs as good as
the baseline when considering both \taq and \exq queries, as plotted in Figure
\ref{fig:notmixed-queries-all}, while both \textit{Intersection Boost} and
\textit{Facet Domain Specificity} improve the effectiveness of COMMA with
negligible impact on performance in responding to \taq queries, as plotted in
Figure~\ref{fig:targeted-queries-all}. This can be explained by considering that
\taq queries do not contain keywords referred to types and facet values.

The baseline performs better than COMMA only on \hyq queries, as shown in
Figure~\ref{fig:mixed-queries-all}. The limited performance of COMMA on these
queries can be explained by considering that \hyq queries contain keywords that
refer to offer lexicalizations \textit{and} keywords that refer to types or
facet values. The syntactic filter used in COMMA is quite restrictive because it
selects only offers that match every keyword in the query. The rationale behind
this choice is to favor recall on \exq queries, without sacrificing precision on
\taq queries. Therefore given a \hyq query such as \quoted{galaxy mobile phone},
the COMMA syntactic filter does not provide any results; results for this query,
i.e., all mobile phones, are returned by COMMA semantic filters (\quoted{mobile
phone} does not occur in offers' lexicalizations but it does in the
lexicalization of types). Therefore, intersection boost does not reward enough
offers such as $\named{SamsungGalaxyS3}$ with respect to other mobile phones.

\concept{Parameters Tuning}

In the second experiment, the effect of the weights $w_{boost}$, $w_{pop}$,
$w_{syn}$ and $w_{sem}$ on the overall effectiveness of COMMA is evaluated.
Figure~\ref{fig:ndcg-boost-varying} presents the nDCG values obtained at fixed
ranks (1, 10 and 20 respectively) for different configurations of COMMA, each
one with a different assignment to $w_{boost}$ (ranging from $0.1$ to $1$), and
with an uniform distribution of the other weights  according to the formula
$w_{pop} = w_{syn} = w_{sem} = \frac{1 - w_{boost}}{3}$. This experiment shows
that the best results are obtained when $w_{boost}$ is assigned values between
$0.6$ and $0.9$. In other words, the higher the weight of the
\textit{Intersection Boost} factor is, the better COMMA ranks according to human
judgment, with the exception of $w_{boost}=1$. Given the optimal
\textit{Intersection Boost} weight, the best performing configuration of COMMA
is the one that emphasizes the syntactic score, as depicted in the
Figure~\ref{fig:ndcg-localscores-varying}. Although several criteria have to be
introduced to refine the overall ranking scores (especially for offers selected
by semantic filters, which extend the functionality of the \as) the syntactic
score represents the second most important ranking factor after
\textit{Intersection Boost}.

\begin{figure}[t]
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=\textwidth]{autocomplete-plots/ndcg-boost-varying.pdf}
		\caption{$w_{boost}$ varying.}
		\label{fig:ndcg-boost-varying}
	\end{subfigure}
	\begin{subfigure}[b]{0.5\textwidth}
		\includegraphics[width=\textwidth]{autocomplete-plots/ndcg-localscores-varying.pdf}
		\caption{Varying weights of local scores.}
		\label{fig:ndcg-localscores-varying}
	\end{subfigure}
	\caption{Normalized DCG for different configurations of
	COMMA with different weights at fixed rank values.}
\end{figure}

\concept{Effectiveness of Result Presentation}
In the last experiment we investigate the effectiveness of the two results
presentation approaches introduced in Section~\ref{sec:algorithm}:
\emph{presentation by rank} and \emph{presentation by type}. The experiment
shows that a grouped presentation of the outcomes of the autocompletion process
does not decrease the effectiveness of COMMA, as depicted in
Figure~\ref{fig:ndcg-pure-vs-category}. Usability tests would be necessary to
understand if the presentation by type is actually preferred by users in real
world settings.

\begin{figure}[t]
	\centering
	\includegraphics[width=.5\columnwidth]{autocomplete-plots/ndcg-pure-vs-category.pdf}
	\caption{nDCG at different rank thresholds for COMMA using two different ranking functions.}
	\label{fig:ndcg-pure-vs-category}
\end{figure}

\subsection{Efficiency}
We remind that the autocompletion operation must take place in a relatively
short time span (100 ms) in order to be perceived as instant by the user. We
assess the efficiency of COMMA \wrt time elapsed for the computation of an
autocompletion operation. For these experiments we use a dataset of 30725
offers, belonging to 206 different types, characterized with 936 different
facets. In order to analyze the scalability of the COMMA approach, the execution
times for completing a query are evaluated with sets of offers of different
size. Test queries for this experiment are taken from a set of 200 queries of
variable length chosen among the most common queries actually submitted by users
of the Italian CSE TrovaPrezzi. All the results of the single query computation
are merged to a mean elapsed time measure. Results of this test are shown in
Figure~\ref{fig:efficiency}: COMMA is compared to the approach based on TF-IDF,
and to other configurations of COMMA that does not include any approximate
string matching algorithm (referred to as ``not robust'' in
Figure~\ref{fig:efficiency}).

\begin{figure}[t]
	\centering
	\includegraphics[width=.5\columnwidth]{autocomplete-plots/efficiency.pdf}
	\caption{Average execution times with set of offers of different size.} 
	\label{fig:efficiency}
\end{figure}

The results of this experiment show that COMMA is able to respect the strict
requirements of an \as, completing the autocompletion operation in less than 25
ms and leaving a reasonable time for network overhead. A second kind of analysis
is related to the impact of the semantic filters and ranking scores adopted by
COMMA on the overall execution times, which can be estimated by considering the
difference between COMMA and TF-IDF. Within the experimented field, this
difference remains quite limited and it does not grow significantly with the
growth of the size of the dataset. Finally, the computational costs of the
approximate string matching algorithms dominate the overall costs of
computation. This problem is due to a sub-optimal implementation of the
approximate string matching algorithm in the Lucene framework used in the
experiments; however, an improved implementation has announced to be included in
Lucene, the overall performance of COMMA is expected to significantly improve.

\subsection{Real World Experimentation}
The experiments in real world scenario were run to evaluate COMMA from a
business perspective. Two different experiments were run considering two
different configurations of COMMA deployed to an Italian eMarketplace dealing
with consumer electronics and photography related goods. The results reported in
this section are not as extensive as the ones previously described due to
confidentiality agreements with the eMarketplace company.

The goal of the first experiment was to evaluate the impact of the addition of
the autocompletion function to the eMarketplace Web site, with particular
reference to the effects on usability of the Web site search feature. The
autocompletion function is a form of site adaptation and we were concerned that
it could reduce the overall site usability. We used Google
Analytics\footnote{\url{http://www.google.com/analytics/}} to gather data about
the usage of the autocompletion function, adopting a purely syntactic
configuration of COMMA (we ran the test in the early phases of the work):
results showed that during the test period (50 days) users increasingly employed
the autocompletion function (considering the frequency of activation of the
autocompletion function on the overall number of page views). Users also
increasingly selected offers returned by the \as instead of submitting queries to
the eMarketplace, as depicted in Figure~\ref{fig:autocomplete-usage}. During
this experiment we found also that the 41.21\% of the top 200 unanswered
autocompletion queries were \exq queries.

\begin{figure}[t]
	\centering
    \includegraphics[width=.5\columnwidth]{autocomplete-plots/autocomplete-usage.pdf}
	\caption{Ratio between users \as result selection and eMarketplace search engine usage.} 
	\label{fig:autocomplete-usage}
\end{figure}

The percentage of \exq unanswered queries from the last experiment confirms the
importance of handling these queries for \as in the \ecomm domain. This
intuition is further confirmed by the second experiment. In a later phase, we
evaluated the impact of the full autocompletion function on the (previously
introduced) conversion rate by means of an A/B
test~\cite{AB-TESTING}\footnote{The A/B test was carried out using Visual
Website Optimizer A/B testing tool. More information can be found at
\url{http://visualwebsiteoptimizer.com/}} during a shorter time period (5 days,
for a total of 7,339 visitors): the introduction of this function led to an
increase of $6.67\%$ of the conversion rate.
