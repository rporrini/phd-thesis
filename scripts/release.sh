#!/bin/bash

set -e

read  -p "********** Press enter to run the spellcheck on the dissertation **********"
scripts/spellcheck.sh en tex "*.tex"

read  -p "********** Press enter to run the check if linked urls are resolvable **********" 
scripts/resolvable-urls.sh

read  -p "********** Please Regenerate the dissertation **********"

read  -p "********** Press enter to run the spellcheck on the italian abstract **********"
scripts/spellcheck.sh it none abstract-it.txt
echo "Character count: $(scripts/count-characters.sh abstract-it.txt)"

read  -p "********** Press enter to run the spellcheck on the english abstract **********"
scripts/spellcheck.sh en none abstract-en.txt
echo "Character count: $(scripts/count-characters.sh abstract-en.txt)"

read  -p "********** Commit and push changes, than press enter **********"

version=$(scripts/generate-version.sh $@)
rm -rf releases
mkdir -p releases
cp abstract-it.txt releases
cp abstract-en.txt releases
cp riccardo-porrini-thesis.pdf releases
mv releases/riccardo-porrini-thesis.pdf releases/riccardo-porrini-dissertation-$version.pdf

git add -A releases
git commit -m "release $version"

commit=$(git log -n 1 --oneline | cut -d' ' -f1)
directory="https://bitbucket.org/rporrini/phd-thesis/raw/$commit/releases"
link="$directory/riccardo-porrini-dissertation-$version.pdf"
sed -i "2s|^|\* [$version]($link) - [Browse]($directory) \n|" README.md

git add README.md
git commit -m "updated README with link to release $version"

git tag $version
git push --tags
git push

echo "share link is"
echo $link
