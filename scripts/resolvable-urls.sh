#!/bin/bash

set -e

find . -name "*.tex" -exec egrep 'url{' "{}" \; | grep '\url' | perl -pe 's|.*\\url{(.*?)}.*|\1 |p' | sed 's/\}/ /' | while read url; do status=$(curl -L -m 10 -o /dev/null --silent --head --write-out '%{http_code}\n' $url); echo $status $url; done | grep -v 200
