#!/bin/bash
# eclipse post-build hook
set -e
echo "post-build hook"
echo $PWD
./scripts/clean.sh
./scripts/spot-unused-commands.sh
echo
