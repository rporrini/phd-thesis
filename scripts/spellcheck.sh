#!/bin/bash

set -e

language=$1
format=$2
pattern=$3

find . -name "$pattern" -exec aspell --home-dir=.aspell --lang=$language --mode=$format check "{}" \;
find . -name "*.bak" -exec rm "{}" \;
