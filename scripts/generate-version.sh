#!/bin/bash

set -e

note=$1
version=$(date +%Y-%m-%d-%H-%M)

if [[ $note != '' ]]
then
	version=$version-$note
fi
if [[ $note == '-M' ]]
then
	version=$2
fi
echo $version
