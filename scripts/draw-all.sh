#!/bin/bash

set -e

function draw_image {
	image=$1
	echo $image
	directory=$(dirname "$image")
	file=$(basename "$image")
	file="$directory/${file%.*}"
	
	libreoffice --headless --convert-to pdf $image --outdir $directory
	pdfcrop "${file}.pdf" "${file}.pdf"
}

echo "drawing all ..."
echo "   images"
for file in $(git status -s images/*.odp | awk '{ print $2 }')
do 
	draw_image $file
done
echo "   property extraction images"
for file in $(git status -s property-extraction-images/*.odp | awk '{ print $2 }')
do 
	draw_image $file
done
echo "   property interpretation images"
for file in $(git status -s property-interpretation-images/*.odp | awk '{ print $2 }')
do 
	draw_image $file
done
echo "   property interpretation plots"
if git status -s property-interpretation-plots/* | grep -v '.pdf' 
then
	cd property-interpretation-plots 
	./draw-all-plots.sh
	cd ..
fi
echo "   abstat images"
for file in $(git status -s abstat-images/*.odp | awk '{ print $2 }')
do 
	draw_image $file
done
echo "   autocomplete images"
for file in $(git status -s autocomplete-images/*.odp | awk '{ print $2 }')
do 
	draw_image $file
done
echo "   autocomplete plots"
for file in $(git status -s autocomplete-plots/*.p | awk '{ print $2 }')
do
	cd autocomplete-plots
	./draw $(basename $file)
	cd ..
done
echo "finished drawing"
