#!/bin/bash

set -e

echo "cleaning .gitignore from garbage ..."

sed -i 's^/tmp/^^g; /^$/d' .gitignore
cat .gitignore

echo "finished cleaning"