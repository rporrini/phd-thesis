#!/bin/bash

echo "------------------"
echo "ununsed commands"
grep "was never used" tmp/main.log | grep -v "@"
echo "------------------"
echo "dead references"
grep "undefined" tmp/main.log
echo "------------------"
echo "suspicious naming"
find . -name "*.tex" -exec egrep 'section{|concept{|chapter{' "{}" \; | grep -v newcommand | grep -v "%" | sed 's/\\label{.*}//g' | sed 's/}//g' | sed 's/\\concept{//g' | sed 's/\\section{//g' | sed 's/\\subsection{//g' | sed 's/\\chapter{//g' | grep -P '^[a-z]| [a-z](?!r|f|ith|he|n|s|rom|C|o|hrough| )'
echo "------------------"
