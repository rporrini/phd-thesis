# Releases
* [boa-uploaded](https://bitbucket.org/rporrini/phd-thesis/raw/76bc703/releases/riccardo-porrini-dissertation-boa-uploaded.pdf) - [Browse](https://bitbucket.org/rporrini/phd-thesis/raw/76bc703/releases) 
* [2016-05-24-18-59-abstracts](https://bitbucket.org/rporrini/phd-thesis/raw/5fbf816/releases/riccardo-porrini-dissertation-2016-05-24-18-59-abstracts.pdf) - [Browse](https://bitbucket.org/rporrini/phd-thesis/raw/5fbf816/releases) 
* [2016-05-23-19-25-empty-abstracts](https://bitbucket.org/rporrini/phd-thesis/raw/c14aab9/releases/riccardo-porrini-dissertation-2016-05-23-19-25-empty-abstracts.pdf) - [Browse](https://bitbucket.org/rporrini/phd-thesis/raw/c14aab9/releases) 
* [submitted](https://bitbucket.org/rporrini/phd-thesis/raw/1936a90/releases/riccardo-porrini-dissertation-submitted.pdf)
* [2016-05-01-10-27](https://bitbucket.org/rporrini/phd-thesis/raw/7dcea2d/releases/riccardo-porrini-dissertation-2016-05-01-10-27.pdf)
* [2016-04-30-17-51-pre-submission](https://bitbucket.org/rporrini/phd-thesis/raw/66f85b6/releases/riccardo-porrini-thesis-2016-04-30-17-51-pre-submission.pdf)
* [2016-04-29-19-21-before-polishing](https://bitbucket.org/rporrini/phd-thesis/raw/fc33b7e/releases/riccardo-porrini-thesis-2016-04-29-19-21-before-polishing.pdf)
* [2016-04-28-17-14](https://bitbucket.org/rporrini/phd-thesis/raw/18c1f1a/releases/riccardo-porrini-thesis-2016-04-28-17-14.pdf)
* [2016-04-27-20-04](https://bitbucket.org/rporrini/phd-thesis/raw/0d63189/releases/riccardo-porrini-thesis-2016-04-27-20-04.pdf)
* [2016-04-25-19-50-facet-intermediate](https://bitbucket.org/rporrini/phd-thesis/raw/8bca36b/releases/riccardo-porrini-thesis-2016-04-25-19-50-facet-intermediate.pdf)
* [2016-04-23-19-38-complete-draft](https://bitbucket.org/rporrini/phd-thesis/raw/56eca55/releases/riccardo-porrini-thesis-2016-04-23-19-38-complete-draft.pdf)
* [2016-04-21-13-22](https://bitbucket.org/rporrini/phd-thesis/raw/511b8f0/releases/riccardo-porrini-thesis-2016-04-21-13-22.pdf)
* [2016-04-13-22-12-after-introduction](https://bitbucket.org/rporrini/phd-thesis/raw/b0327bc/releases/riccardo-porrini-thesis-2016-04-13-22-12-after-introduction.pdf)
* [2016-03-30-22-48-corpus](https://bitbucket.org/rporrini/phd-thesis/raw/2f54bab/releases/riccardo-porrini-thesis-2016-03-30-22-48-corpus.pdf)
* [2016-03-24-20-46](https://bitbucket.org/rporrini/phd-thesis/raw/d15cffc/releases/riccardo-porrini-thesis-2016-03-24-20-46.pdf)
* [2016-03-20-20-46-formalism-and-contributions](https://bitbucket.org/rporrini/phd-thesis/raw/17dee66/releases/riccardo-porrini-thesis-2016-03-20-20-46-formalism-and-contributions.pdf)
* [2016-03-13-19-31-after-autocomplete](https://bitbucket.org/rporrini/phd-thesis/raw/7789197/releases/riccardo-porrini-thesis-2016-03-13-19-31-after-autocomplete.pdf)
* [2016-03-11-20-13-after-abstat](https://bitbucket.org/rporrini/phd-thesis/raw/dfa6947/releases/riccardo-porrini-thesis-2016-03-11-20-13-after-abstat.pdf)
* [2016-02-28-19-19-literature](https://bitbucket.org/rporrini/phd-thesis/raw/00e0859/releases/riccardo-porrini-thesis-2016-02-28-19-19-literature.pdf)
* [2016-02-07-17-50-preliminaries](https://bitbucket.org/rporrini/phd-thesis/raw/12292db/releases/riccardo-porrini-thesis-2016-02-07-17-50-preliminaries.pdf)
* [2016-01-31-13-17-story](https://bitbucket.org/rporrini/phd-thesis/raw/83962dd/releases/riccardo-porrini-thesis-2016-01-31-13-17-story.pdf)
* [2016-01-24-18-14-first-feedback](https://bitbucket.org/rporrini/phd-thesis/raw/a1b75f4/releases/riccardo-porrini-thesis-2016-01-24-18-14-first-feedback.pdf)
* [2016-01-23-19-10-first](https://bitbucket.org/rporrini/phd-thesis/raw/506feae/releases/riccardo-porrini-thesis-2016-01-23-19-10-first.pdf)

### How to roll a new Release or Milestone
* to publish a new relese: ```scripts/release.sh [$NICKNAME]```
* to publish a new milestone: ```scripts/release.sh -M $NAME```

