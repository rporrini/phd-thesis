\section{Summary of the Dissertation}

This dissertation studied how to enrich the schema of Knowledge Graphs (\kgs)
with domain specific facets extracted from a vast amount of structured sources,
providing interactive methods to domain experts in charge of maintaining a
dataspace to ensure the adequate quality of the data. We focused on data
management settings that imply the incremental integration of independent
\emph{data sources} into a \emph{dataspace}~\cite{Halevy-sigmodr05}, where a \kg
provides a structured machine readable representation of data on top of which
advanced data access features are built, and where the integration process is
managed and supervised by domain experts. This dissertation focuses on two main
aspects:
\begin{itemize}
  \item \textbf{Domain specificity.} This dissertation proposed a
  facet extraction and interpretation approach that incorporate
  the notion of domain specificity. We leverage already established
  mappings between source and \kg types to extract domain specific facets, and
  propose an approach to provide a domain specific interpretation of them, by
  re-using relations already defined in the \kg.
  \item \textbf{Enhanced Domain Expert Supervision.} By mean of the \kg
  summarization approach presented in this dissertation, domain experts can
  profile and inspect the \kg to understand the usage of relations in the data.
  This acquired understanding is crucial in order to properly supervise
  the approaches presented in this dissertation.
\end{itemize}

\concept{Domain Specific Facet Extraction}

Chapter~\ref{cap:consolidation} introduced an automatic, domain specific facet
extraction approach, which supports domain experts in the definition of
significant domain specific facets. Facets are specialized relations aimed at
model the salient characteristics of entities from specific domains (e.g., news,
actors, or wine bottles), and thus the technical problem tackled in this
contribution accounts to the extraction of salient domain specific relations.
The basic intuition behind the proposed approach is to reuse the representation
of source instances provided by domain specific data sources. In particular, we
leverage mappings established between source and \kg types, to suggest
meaningful, domain specific facets for a given \kg type, based on \emph{Taxonomy
Layer Distance} (TLD), a novel metric used as clustering distance metric for
grouping together facet values. Through our approach we are not only able to
extract meaningful facets, but also to populate the \kg with the relative
faceted assertions about instances, thus supporting the provision on advanced
data access features with an enhanced user-experience.

\concept{Specificity-based Interpretation of Facets}

Chapter~\ref{cap:interpretation} discussed how to interpreting extracted facets
by \emph{annotating} them with relations holding between \kg instances of given
domain and facet values. Given a facet, the proposed approach derives a set of
candidate relations from \kg and ranks them considering how much their semantics
is similar to the semantics of the facet, focusing on domain specificity. The
proposed approach handles the intrinsic ambiguity of facets by annotating them
with relations that are more specific with respect to their facet domain.
Our approach effectively quantifies the specificity of relation by considering
its extensional semantics, that is the usage of the relation in the \kg with
respect to the different domains of knowledge conceptualized by \kg types.

\concept{Knowledge Graph Profiling}

Chapter~\ref{cap:summarization} introduced ABSTAT: a \kg \emph{summarization}
framework that provides an abstract view of \kg relations. ABSTAT is capable to
provide an overview of the specificity of relations of a \kg. Such information
is crucial to enable a proper supervision of the extraction and annotation
phases by domain experts. Due to the proposed minimalization based approach,
ABSTAT is able to provide both compact and informative summaries for a given
\kg. We showed that using the ABSTAT framework, summaries are more compact than
the ones generated from other related models and they also help humans (or
algorithms) to gain insights about the semantics of relations in the \kg.

\section{Future Work}

Several future lines of work originate from the contributions presented in this
dissertation. The rest of the section summarizes the main ones.

\concept{``Local'' Improvements}
The three proposed approaches can be independently extended and improved along
different directions. Firstly, advanced NLP techniques can be used to improve
the facet extraction approach, by normalizing source types considering different
lexicalizations (as discussed in Section~\ref{sec:facet-extraction-evaluation}).
Secondly, the definition of the specificity of a relation with respect to a
facet domain (discussed in Chapter~\ref{sec:facet-interpretation-problem}) can
be adapted to consider \kgs with deep and specialized subtype graphs (as pointed
out in Section~\ref{sec:facet_interpretation-evaluation}). Thirdly, we plan to
conduct the user study described in Section~\ref{sec:abstat-evaluation} in large
scale, thus including more users with different background characteristics in
order to analyze in details which is the target group of users for which ABSTAT
is more useful. Also, we plan to complement our coverage-oriented approach with
relevance-oriented summarization approaches based on connectivity analysis as
discussed in Section~\ref{sec:abstat-related-work}.

\concept{Table Annotation}

As discussed in Section~\ref{sec:facet_interpretation-conclusion}, the facet
annotation approach can in principle be applied to the problem of relation
annotation in tabular data. To this end, a preliminary work has been done by
including an adapted version of the facet annotation approach into a table
annotation tool, namely STAN\footnote{\url{http://stan.disco.unimib.it}}.
Further investigation and additional experiments are needed, but we believe that
this represents one of the most interesting directions of future work
originating from the contributions of this dissertation.

\concept{Bring Explicit Feedback into the Picture}

The approaches discussed in this dissertation are data-driven, and are conceived
to be included in a workflow where the validation of domain experts at all the
stages is crucial. Explicit feedback gathered from domain experts validation
represents an invaluable information that can be used to refine the
interpretation phase. In particular, we envision a system that learns how to
better capture the domain specificity of \kg relations in the annotation of
facets, based on explicit feedback from domain experts on its past
effectiveness. Towards this end, it would be interesting to investigate to which
extent the facet annotation approach can benefit from Schema and Ontology
matching literature, where the development of interactive matching systems based
on explicit feedback loops recently gained attention with the advent of Crowd
Sourcing platforms and paradigms
(e.g.,~\cite{CruzLPST14,Duan2010,Shi2009,BelhajjamePFHE11}).

\concept{Consider Data Access Patterns from End-users}

The representation of data provided by specialized sources is not the only
source of valuable information that can be leveraged in order to extract domain
specific facets~\cite{PoundEAl}. As the ultimate purpose of DI application is to
make integrated data searchable and accessible to end-users, the understanding
of \emph{how} end-users search and browse the data may provide useful insights
on how to represent dataspace instances. In other words, a domain specific
representation may emerge not only from data, but also from \emph{usage}. For
example, the analysis of full text queries submitted by users with exploratory
information needs (e.g., the full text query ``android mobile phones''), may
reveal interesting patterns useful to identify what are the salient
characteristics of instances in particular domains (e.g., the operating system
for mobile phones). We believe that our facet extraction approach can gain a
huge benefit from the study of frequent patterns in the access of integrated
data. Towards this end, however, it is crucial to develop effective and
efficient methods to analyze big, potentially noisy data access logs.

\concept{Reuse of Summaries}

We envision a large scale application of our summarization approach. As ABSTAT
focuses on RDF \kgs, we would like to run our summarization framework on \kgs of
the Linked Open Data cloud. Such large scale analysis of well known \kgs would
potentially unveil commonly used data representation patterns from independent
sources. This would particularly benefit domain experts, especially in providing
a domain specific interpretation of facets. With ``global'' information about
common patterns of use of relations from shared vocabularies, domain experts
will potentially get better insights about their most commonly adopted
semantics.

\concept{Support for Multilingual Dataspaces}

In this dissertation we didn't consider the case where data provided by the
sources is lexicalized in different languages. The facet extraction approach
presented in this dissertation does not leverage any language-specific feature
in the extraction of facets, and can be easily applied to different languages
other than English (see~\ref{sec:facet-extraction-evaluation}). From the other
side, the facet interpretation approach presented in
Chapter~\ref{cap:interpretation} implicitly assumes consistency between the
language in which facet values and \kg instances are lexicalized. In order to
extend such approach to the multilingual case, one would have to investigate to
which extent our approach can benefit from the literature in Cross-Lingual
Schema and Ontology Matching (e.g.,~\cite{HelouPJ16,pado2009cross}). We believe
that this would particularly benefit the application of the approaches discussed
in this dissertation to scenarios that require the integration of multilingual
data, as for example integrating Open Data released by institutions from
different countries.
