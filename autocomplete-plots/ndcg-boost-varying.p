set xrange [0.04:1.06]
set xlabel "Intersection Boost Value"
set xtics 0.1

set key right top
set style data boxes

plot "ndcg-boost-varying.data" using ($1-0.03):2:(0.02) ls 1 fs solid 0.1 title "COMMA (rank 1)", \
"ndcg-boost-varying.data" using 1:3:(0.02) ls 2 fs solid 0.3 title "COMMA (rank 10)", \
"ndcg-boost-varying.data" using ($1+0.03):4:(0.02) ls 3 fs solid 0.5 title "COMMA (rank 20)"
