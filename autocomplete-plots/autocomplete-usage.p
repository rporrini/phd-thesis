set ylabel "Selections/Searches"
set yrange [0:1]
set xlabel "Day Number"
set xrange [1:50]
set xtics 5
f(x)= a*x + b
fit f(x) "autocomplete-usage.data" using 1:2 via a,b 
plot "autocomplete-usage.data" using 1:2 notitle, \
f(x) notitle
