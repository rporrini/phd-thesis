\section{Application to Table Annotation
Problems}\label{sec:facet-interpretation-table-annotation}

The approach presented in this chapter tackles the problem of interpreting a
facet by annotating it with relations from a \kg. As we highlighted in
Section~\ref{sec:facet-interpretation-problem}, solving the facet annotation
problem basically accounts to solving a particular relation annotation (i.e.,
interpretation) problem. Although our approach is tailored to facets, we believe
that it can be applied to solve other types of problems which account to the
establishment of a r-to-r mapping (i.e., relation-to-relation, see
Section~\ref{sec:mappings}). 

As highlighted in Section~\ref{sec:facet-interpretation-related}, table
annotation approaches establish r-to-r mappings when annotating a table column
with the relation that hold between values in such column and values in a
previously identified subject column. Table annotation approaches analyze the
reciprocal distributions of values in the subject column and the column to be
annotated, when part of the same row, under the assumption that values in the
subject columns can be linked to entities in the \kg, for example by matching
their lexicalizations. However, there are tables for which this assumption does
not hold, as for example the table depicted in Figure~\ref{fig:difficult-table}.
This table includes data about crimes reported in the city of Chicago, and is
publicly accessible through the Chicago Open Data
portal\footnote{\url{https://data.cityofchicago.org/Public-Safety/Crimes-2001-to-present/ijzp-q8t2}
- Accessed on April 2016}. The subject column of this table is the \textbf{ID}
column that contain a progressive numerical identifier and, remarkably, says
nothing about the entities described by the table. 

\begin{figure}[t] 
	\centering
	\includegraphics[width=1\textwidth]{property-interpretation-images/difficult-table.png}
	\caption{A table with an uninformative subject column.}
	\label{fig:difficult-table}
\end{figure}

This setting is common, especially when dealing with highly domain specific
tabular data released by public institution in the Open Government context. In
this situation, state-of-the-art table annotation approaches are likely to fail
in annotating the non-subject columns with relations from a \kg, because they
are unlikely to be able to match values in this column with the lexicalization
of \kg entities. We believe that our facet annotation approach can, at least in
principle, help to solve this problem. In fact, our approach does not assumes
the presence of two co-ordinate sets of values, but only the lexicalization of the
facet domain and values of the facet range. In the case of the table depicted in
Figure~\ref{fig:difficult-table}, a similar representation can be provided for
non-subject columns. For example, the column \textbf{Primary Type} can be
represented as a facet:
\begin{formula*}
<\named{Crime}, \{\named{Narcotics}, \named{Criminal\;Trespass},
\named{Theft}, \named{Robbery} \ldots\}>
\end{formula*}
where the facet domain can be specified by hand by domain experts, or
even automatically extracted by leveraging data profiling techniques that
identify the general topic of a table (e.g.,~\cite{Bohm2012}).

\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{property-interpretation-images/ui-suggestions.png}
	\caption{A screenshot of the column annotation feature of STAN.}
	\label{fig:stan}
\end{figure}

As a proof of concept, we adapted and included our facet annotation approach in
STAN\footnote{\url{http://stan.disco.unimib.it} -
\url{https://github.com/brando91/STAN}}: a semantic table annotation tool that
supports the annotation of tabular data with types and relations from a defined
\kg. Figure~\ref{fig:stan} depicts a screenshot of the column annotation feature
provided by STAN. At the time of writing, STAN supports the annotation of a
table with the DBpedia \kg. Our facet annotation approach is leveraged to
suggest relations to annotate table columns. Although the application of our
approach to table annotation problems is out of the scope of this dissertation,
we believe that the state-of-the-art can benefit from the insights we got by
tackling the facet annotation problem. This represents an interesting research
direction that we plan to investigate in the near future.

\section{Summary}\label{sec:facet_interpretation-conclusion}

In this chapter we presented an automatic facet interpretation approach that
allows to annotate a facet with \kg relations. We proposed an approach that is
capable to handle the intrinsic ambiguity of facets, by annotating them with
relations that are more specific with respect to their facet domain. Experiments
conducted by annotating facets with two large \kgs (DBpedia and YAGO) confirm
the effectiveness of the proposed approach.

