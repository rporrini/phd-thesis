\section{Overview}

In the last chapter we discussed an automatic facet extraction approach that
supports domain experts in the extraction of facets. As for most of
state-of-the-art facet extraction approaches (e.g., the ones discussed in
Section~\ref{sec:consolidation}) the approach presented in the last chapter does
not take into account the interpretation of a extracted facets, in the sense
that it is not able to understand \emph{which} relation is modeled by the facet
and provide a machine-readable semantics associated with it. In order to fully
exploit the powerful domain specific representation potentially provided by
facets, a more complete interpretation of their semantics is needed.

This chapter presents an automatic approach to interpret a facet by
\emph{annotating} it with relations holding between \kg instances of given
\emph{domain} and facet values. The key insight behind the approach discussed in
this chapter is to re-use relations defined by in a \kg to interpret the
meaning of a facet. Given a facet comprised of a set of facet values and a facet
domain, the proposed approach derives a set of candidate relations from \kg and
ranks them considering how much their semantics is similar to the semantics of
the facet. The core of the approach is the notion of semantic similarity.
Our hypothesis is that we can assess the similarity between a facet and a
relation $\namedrelation$ by considering the extensional semantics of
$\namedrelation$, that is the usage of $\namedrelation$ in \kg with respect to
the different domains of knowledge conceptualized by \kg types.

Solving the facet annotation problem is challenging for several reasons. Facet
values are ambiguous and highly domain dependant, and thus there may be more
than one relation suitable for the annotation of a given facet. For example,
facet values such as $\named{English}$, $\named{German}$, $\named{French}$,
$\named{Spanish}$ may refer to the language of a specific book or to the
nationality of a basketball player. So as to leverage the extensional semantics
of the relations provided by \kg, both facet values and domain must be matched
with relational assertions and types that are used to classify entities from
\kg. We address this challenge by providing a convenient representation of \kg
that supports the matching of facet values with relational assertions from \kg,
and the matching of the facet domain with types. Moreover, we design a ranking
function that is able to effectively quantify semantic similarity.

The rest of this chapter is organized as follows.
Section~\ref{sec:facet-interpretation-problem} introduces the facet
annotation problem, while Section~\ref{sec:facet-interpretation-similarity}
defines the semantic similarity between a facet and a \kg relation. In
Section~\ref{sec:facet_interpretation-approach}, we describe a filter and rank
facet annotation approach that, given a facet, selects relations from a \kg and
ranks them according to their similarity.
Section~\ref{sec:facet_interpretation-evaluation} describes an extensive
evaluation using {\kg}s with different characteristics, along with the
comparison of the proposed facet interpretation approach with state-of-the-art
ones, which are described in Section~\ref{sec:facet-interpretation-related}
before concluding the chapter in
Section~\ref{sec:facet_interpretation-conclusion}\footnote{The approach
described in this chapter, which is the result of the most recent work carried
out as part of PhD activities of the author, is planned to be submitted within
May or June 2016 at most. A version of the working paper is available at
\url{http://bit.ly/facet-annotation-paper-draft}}.
