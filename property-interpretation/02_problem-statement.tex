\section{Problem Definition}\label{sec:facet-interpretation-problem}

Our approach considers facets and a \kg. In the following paragraph we briefly
recall the definition of these two kind of artifacts from
Chapter~\ref{cap:dataspaces}, and the facet annotation problem. We then
highlight the basic intuition behind our proposed approach.

\concept{Knowledge Graph} 

A Knowledge Graph (\kg) consists of a set of named \emph{relations} holding
between named \emph{instances} classified using \emph{types}. A relation
$\namedrelation$ is specified in terms of \emph{domain} and \emph{range}
restrictions expressed by means of First Order Logic (FOL) terminological
axioms. Such domain and range axioms typically impose a restriction on the type
of subject or object instances of a relation, respectively. We refer to this
specification as the \emph{intensional} semantics of a relation (see
Section~\ref{sec:knowledge-graphs}). The \kg includes also a set of relational
assertions $\assertions{\namedrelation}$ in the form of $\namedrelation(\msub,
\mobj)$. A relational assertion explicitly states that the $\namedrelation$
holds between a subject instance $\msub$ and an object instance $\mobj$.
Relational assertions provide an alternative specification of the semantics of
$\namedrelation$ emerging from its usage within \kg. We refer to this
specification as \emph{extensional} semantics, as opposed to the intensional
semantics specified by the \kg schema.

Types are organized in subtype graph $\typegraph = (\namedtypes, \subtype)$,
where $\namedtypes$ is the set of all \kg types, and $\subtype$ is a subtype
relation that holds between them. Observe that in principle a \kg may specify
more than one subtype graphs (e.g., the DBpedia \kg). An instance $\ment$ is an
instance of one or more types. We define a function $\mtypesof{\ment}$ that
returns the set of types of \ent, including all the supertypes through the
subtype graph (i.e., the transitive closure of the types' set). Both types, and
entities are associated to \emph{lexicalizations}. A lexicalization is a
sequence of natural language tokens that are associated to a type, entity, or
a literal value. We denote with $\mlabelof{\namedtype}$, $\mlabelof{\ment}$ the
set of lexicalizations of a type \typ, an entity \ent, respectively. The
lexicalizations of a literal value is the set that contains the value itself.
For instance, considering Figure~\ref{fig:facet-role-linking}:
$\mlabelof{\named{J\_R\_R\_Tolkien}}$ = $\{$\quoted{John Ronald Reuel Tolkien},
\quoted{J. R. R. Tolkien}$\}$.

\concept{Facet} 

Recall from Section~\ref{sec:facets} that a facet is a relation that models a
salient characteristic of a specific type of entities and a set of values.
A facet $\mfacet = < \namedtype, \facetrange >$ holds between a \emph{domain} of
instances of the type $\namedtype$ and a \emph{range} of facet values
$\facetrange = \facetvalues$. For instance, the facet
\ifacet{\named{Books}}{\namedtext{Agatha Christie, Arthur C. Clarke, \ldots}} is
depicted in Figure~\ref{fig:facet-role-linking}. Observe that in this setting,
the facet domain $\namedtype$, is expressed in terms of a source type of
entities. We assume this type (e.g., $\named{Books}$) to be available at least
in the form of its lexicalization. We notice also that the specification of
facet is not ``balanced'', in the sense that the facet domain is defined
differently from the facet range, being the first a lexicalized type and the
second a set of values, respectively.

\concept{Facet Annotation}

Annotating a facet $\mfacet$ means finding a relation $\namedrelation$ or more
generally a set $\mpreds= \{ \namedrelation_1, \ldots, \namedrelation_n \}$ of
relations from \kg, such that the semantics of $\namedrelation$ is
\emph{similar} to the semantics of $\mfacet$.
Figure~\ref{fig:facet-role-linking} shows a graphical depiction of the facet
annotation problem. In essence, solving the facet annotation problem consists of
assessing the degree of similarity between the semantics of a relation
$\namedrelation$ from \kg and a facet $\mfacet$.

\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{property-interpretation-images/problem-statement.pdf}
	\caption{The facet annotation problem.}
	\label{fig:facet-role-linking}
\end{figure}

Solving the facet annotation problem basically accounts to solving a relation
annotation (i.e., interpretation) problem~\cite{limayeVLDB2010, venetisVLDB2011,
shenKDD2012, wangER2012, querciniEDBT2013, zwicklbauerISWC2013, mulwadISWC2013,
zhangISWC2014, zhangEKAW2014, zhangSWJ2015,CheathamH14a}.
The goal of relation annotation approaches (see
Section~\ref{sec:interpretation}), is to annotate an input source relation with
target relations from a defined \kg. However, there is one crucial difference
between a relation annotation problem and a facet annotation problem.
In fact, the specification of a facet is \emph{unbalanced}, in the sense that
the facet domain is specified differently from the facet range. The facet domain
is specified by means of a lexicalization of an entity type, while the facet
range is specified by means of a set of values. This setting is different from
typical relation annotation settings, which deal with relations whose
specification is \emph{balanced}. Relation domains and ranges are either both
specified in terms of sets of values (i.e., for relation annotation in web table
interpretation tasks~\cite{limayeVLDB2010}) or in terms of entity types (i.e.,
for relation annotation in ontology matching tasks~\cite{CheathamH14a}).

\section{Specificity Driven Semantic
Similarity}\label{sec:facet-interpretation-similarity}

For seek of simplicity, we write that a relation $\namedrelation$ \emph{is
similar} to a facet $\mfacet$, meaning that their semantics are similar. In a
nutshell, we consider a relation $\namedrelation$ similar to a facet $\mfacet$
if $\namedrelation$ is: (1) \emph{specific} with respect to the facet domain,
(2) it \emph{covers} all the facet values, and (3) it \emph{frequently} connects
entities from the facet domain to facet values within \kg. Given a facet
$\mfacet$, our intuition is to quantify \emph{specificity}, \emph{coverage}, and
\emph{frequency} of relations, by looking at their extensional semantics, that
is the usage in \kg.

The core concept on which we base our definition of semantic similarity is
\emph{specificity}. We argue that the more a relation $\namedrelation$ is
specific with respect to the domain and the range of a facet $\mfacet$, the more
$\namedrelation$ is similar to $\mfacet$. We rely on an example to better
explain why we consider it crucial in determining the similarity between a facet
and a relation\footnote{The presented example is drawn from the DBpedia \kg
(version 3.9), with some simplifications.}. Consider the facet from
Figure~\ref{fig:facet}. The facet range includes, for instance,
$\named{Arthur\_C.\_Clarke}$, and $\named{J.\_R.\_R.\_Tolkien}$, while the
facet domain is $\named{Books}$. Intuitively, the facet holds between books and their
authors. Now, consider the two relations $\named{author}$
(Figure~\ref{fig:author}) and $\named{creator}$ (Figure~\ref{fig:creator}). Both
relations hold between $\named{The\_Hobbit}$ and $\named{J.\_R.\_R.\_Tolkien}$,
and between $\named{2001\_A\_Space Odyssey}$ and $\named{Arthur\_C.\_Clarke}$.
From this view point they both look similar to $\mfacet$ as they hold between
the same entities.

\begin{figure*}[t]
	\centering
	\begin{subfigure}[b]{.55\textwidth}
		\includegraphics[width=\textwidth]{property-interpretation-images/book-author-facet.pdf}
		\vspace{0.25cm}
	\end{subfigure}
	\begin{subfigure}[b]{.49\textwidth}
		\includegraphics[width=\textwidth]{property-interpretation-images/example-author.pdf}
		\caption{A \emph{specific} relation w.r.t. books.}
		\label{fig:author}
	\end{subfigure}
	\begin{subfigure}[b]{.49\textwidth}
		\includegraphics[width=\textwidth]{property-interpretation-images/example-creator.pdf}
		\caption{A \emph{generic} relation w.r.t. books.}
		\label{fig:creator}
	\end{subfigure}
	\caption{An example that illustrates the difference between specific and
	generic relations.}
	\label{fig:facet}
\end{figure*}

However, if we take a look at the intensional semantics of $\named{author}$ and
$\named{creator}$, we realize that the domain of the latter is completely
unspecified: any entity can potentially be part of such a domain. Moreover,
looking at the extensional semantics of $\named{creator}$ provided by \kg, we
realize that $\named{creator}$ holds not only between books and writers, but
also between fictional characters and places and their respective creators.
Observed from this viewpoint, $\named{author}$ looks much more similar than
$\named{creator}$ to $\mfacet$. In fact, the domain of $\named{author}$ is much
more specific with respect to the facet domain (i.e., $\named{Books}$), compared
to the domain of $\named{creator}$.

Our definition of semantic similarity includes also \emph{coverage}.
Ideally, given $\mfacet$, a relation $\namedrelation$ that holds between
entities from the domain $\mfdomain$ and \emph{all} facet values should be
considered highly similar to $\mfacet$. For instance, given the facet from
Figure~\ref{fig:facet}, suppose that we find that the relation $\named{author}$
holds between instances of type $\named{Book}$ and all the books denoted by
facet values. In this situation, we consider $\named{author}$ similar to the
facet, because all the facet values appear within instances of $\named{author}$
(i.e., the ``coverage'' of $\named{author}$ over the facet values is maximum).

\emph{Frequency} is orthogonal to coverage. In principle, given $\mfacet$, the more
frequently facet values are connected by a relation $\namedrelation$ to instances from the
domain $\mfdomain$, the higher the similarity between $\namedrelation$ and
$\mfacet$. The rationale behind frequency is to favour relation that are more frequently
used within \kg with respect to the facet domain, for instance favoring
relations like $\named{language}$ over $\named{origLanguage}$\footnote{Both of
them are used in DBpedia to denote the original language of publication of books.}.
