\section{Evaluation}\label{sec:facet_interpretation-evaluation}

In our experiments we annotate facets with relations from two different {\kg}s:
DBpedia and YAGO. We compare our ranking approach with the \emph{majority
voting} model and the \emph{maximum likelihood} model proposed by Venetis et
al.~\cite{venetisVLDB2011} adapted to the facet annotation problem. We rely on
two different gold standards as ground truth for the annotation: a manually
created one for DBpedia and a state-of-the-art one for YAGO, which has been
introduced for evaluation of table annotation approaches~\cite{limayeVLDB2010}.
Experimental results indicate that our approach outperforms the state-of-the-art
ones in annotating facets. 

We implemented our approach along with the baseline algorithms using the Java
programming language and the Lucene
library\footnote{\url{http://lucene.apache.org/}}. The code repository, along
with all the gold standards we compared our approach against and experimental
results provided in this section are publicly
accessible\footnote{\url{https://bitbucket.org/rporrini/facet-annotation/}}.
Experiments were conducted on a Dual Core 2.54GHz processor machine, 4GB RAM,
128GB SSD HD, under the Ubuntu Linux Desktop 64bit operating system.


\subsection{Knowledge Graphs}

\begin{smalltable}
	\caption{Knowledge Graphs' statistics.}
	\label{tab:kbs}
	\begin{tabularx}{\columnwidth}{@{}l *4{>{\centering\arraybackslash}X}@{}}
	\toprule
	\midrule
	& \textbf{Types} & \textbf{Relations} & \textbf{Relational Assertions}\\
	\midrule 
	\textbf{DBpedia} & 753958 & 53195 & $\sim$ 96.8M \\
	\textbf{YAGO} & 184512 & 89 & $\sim$ 5.5M \\
	\midrule
	\bottomrule
	\end{tabularx}
\end{smalltable}

We consider two different {\kg}s with different characteristics, highlighted in
Table~\ref{tab:kbs}. DBpedia (version 3.9) contains 753958 types, 53195
relations and more than 96M relational assertions involving them. In our
experiments, we include two subtype graphs: the first one is extracted from the
DBpedia ontology, and the second one is extracted from DBpedia categories.
DBpedia categories are extracted from Wikipedia categories and are known to be
noisy and low quality~\cite{WIKIPEDIA-CATS}. The second \kg we consider is YAGO
(version: 2008-w40-2)\footnote{The reason for using an old version of YAGO is
that the state-of-the-art annotated gold standard that we use to evaluate our
approach is built considering this particular version of
YAGO~\cite{limayeVLDB2010}.}, which includes 184512 types, 89 relations and more
than 5M relational assertions involving them.

\subsection{Gold Standards}

The data sets used for evaluation consist of two disjoint sets of facets
manually annotated with relations from DBpedia and YAGO, respectively.
In the case of DBpedia we found that there exist more than one relation that is
similar to the facet, while in case of YAGO there is only one similar
relation for each facet. Statistics about these two gold standards are
summarized by Table~\ref{tab:gss} , while Table~\ref{tab:gold-standard-facets}
presents few examples of facets from the gold standards.

\begin{smalltable}
	\caption{Gold Standards' statistics.}
	\label{tab:gss}
	\begin{tabularx}{\columnwidth}{@{}r *4{>{\centering\arraybackslash}X}@{}}
	\toprule
	\midrule
	& \multicolumn{2}{c}{\textbf{Facets}} & \multicolumn{2}{c}{\textbf{Relations}}\\ 
	& \emph{\#} & \emph{Domains} & \emph{Vital} & \emph{Correct}\\
	\midrule
	\textbf{dbpedia-numbers} & 8 & 7 & $\sim$4 & $\sim$19\\
	\textbf{dbpedia-entities} & 31 & 13 & $\sim$7 & $\sim$7\\
	\textbf{dbpedia} & 39 & 13 & $\sim$3 & $\sim$9\\ 
	\midrule
	\textbf{yago-explicit} & 83 & 17 & 1 & -\\
	\textbf{yago-ambiguous} & 83 & 10 & 1 & -\\
	\midrule
	\bottomrule
	\end{tabularx}
\end{smalltable}

\begin{smalltable}
	\caption{Example of facets from the gold standards.}
	\label{tab:gold-standard-facets}
	\begin{tabularx}{\textwidth}{@{}r *3{>{\centering\arraybackslash}X}@{}}
	\toprule
	\midrule
	& \textbf{Domain} & \textbf{Values} & \textbf{Relations}\\
	\midrule
	\textbf{dbpedia-numbers} & music albums & 1967, 1969, 1971, \ldots, 2000 &
	$\named{relYear}$, $\named{year}$, $\named{releaseDate}$ \ldots \\
	\textbf{dbpedia-entities} & wines & USA, Italy, \ldots, Slovenia &
	$\named{wineRegion}$, $\named{origin}$, \ldots\\
	\midrule
	\textbf{yago-explicit} & wordnet-actor-109765278 & 1776, California, \ldots,
	Wilson & $\named{actedIn}$\\
	\textbf{yago-ambiguous} & countries & Abu Dhabi, Accra, \ldots, Zagreb &
	$\named{hasCapital}$\\
	\midrule
	\bottomrule
	\end{tabularx}
\end{smalltable}

\concept{DBpedia Gold Standard} 
Following the methodology proposed for the creation of gold standards for table
annotation tasks~\cite{venetisVLDB2011,mulwadISWC2013,zhangSWJ2015}, we manually
collected 39 facets from the Web, from different domains. 32 facets were
collected from Wikipedia (non infobox) table columns, while 7 were collected
from IMDB, eCommerce and sport statistics websites. For each facet, we selected
from DBpedia \kg all the relations whose object matches \emph{at least one}
facet values, without considering facet domain matching. We presented the gold
standard to a total of 10 human annotators, who rated the similarity of each
relation using a Likert scale from 0 up to 2 (0 = \emph{incorrect}, 1 =
\emph{correct}, 2 = \emph{vital}). We presented only a portion of the gold
standard to each annotator in order to reduce the cognitive workload, ensuring
that each relation was been rated by at least 3 annotators. We aggregated all
the judgments, manually resolving inconsistencies. We report an average
correlation of 0.42 (Spearman Correlation Coefficient) between annotators. We
then partitioned the gold standard into two disjoint sets of facets:
\textbf{dbpedia-numbers} (i.e., facets whose values are digits) and
\textbf{dbpedia-entities}.

\concept{YAGO Gold Standard} 
The gold standard for the YAGO dataset has been used to evaluate several table
annotation algorithms~\cite{limayeVLDB2010} and consists of tables where cells
have been annotated with entities, columns with types and column pairs with
relations (on a total number of 90). We derived 90 facets from all table columns
involved in at least one relation annotation, considering the lexicalization of
annotated type for the corresponding subject columns as facet domain, when
present. Among these 90 facets, 7 had no corresponding type annotation for the
corresponding relation's subject column and thus were discarded. We ended up
with 83 annotated facets. Observe that, there are no numeric facets. We then
created two versions of the YAGO gold standard. Within the
\textbf{yago-explicit} gold standard we used the local name of types (extracted
from the type URI) as facet domains (e.g.,
$\named{wikicategory{\thehyphen}American{\thehyphen}science{\thehyphen}fiction{\thehyphen}novels}$). By
considering the local name of types, we minimize ambiguity in facets domains. Conversely, in the
\textbf{yago-ambiguous} gold standard,  we used more general (and thus,
ambiguous), lexicalizations for facet domains, such as $\named{novels}$.

\subsection{Algorithms}

Most of the state-of-the-art table annotation approaches have been compared to
the \emph{majority voting} based model~\cite{querciniEDBT2013, venetisVLDB2011,
limayeVLDB2010} (MAJ). Given \facet, the majority based approach selects all the
relations whose object lexicalizations match at least one facet values and
ranks them by frequency. The \emph{maximum likelihood} based
model~\cite{venetisVLDB2011} (MAX) is based on the application of the maximum
likelihood hypothesis. Given a facet \facet, the most similar relation is the
one that maximizes the conditional probability of occurrence of the relation
given the facet values. More formally:
\begin{formula*}
	ml(\namedrelation, \mfacet) = K_{p} \times \prod_{\mvalue \in
	\facetrange}{\cfrac{\text{Pr}[~\namedrelation~|~\mvalue~]}{\text{Pr}[~\namedrelation~]}}
	\text{,~~s.t.}
	\sum_{p}{ml(\namedrelation, \mfacet) = 1}
\end{formula*}
where $\text{Pr}[~\namedrelation~|~\mvalue~]$ and $\text{Pr}[~\namedrelation~]$ are computed over
the considered \kg.

Observe that these two approaches leverage the quasi-relational structure of a
table. When annotating a pair of columns, they match cells of the first column
to relation subjects and the cells of the other column to relation objects. In a
nutshell, they apply different matching criteria in selecting an appropriate
relation extension $\Delta$ so as to compute the semantic similarity. These
matching criteria are tailored to \emph{balanced} input, that is relations whose
domain and ranges are specified by means of two sets of values. However, we
recall from Section~\ref{sec:facet-interpretation-problem} that the input of the
facet annotation problem is unbalanced, being the facet domain the
lexicalization of a type. In order to provide a fair comparison, we
compare our relation ranking function (i.e., DRC) with the baselines applied to
the extensions $\Delta_{\namedrelation}^{\mfacet}$ induced by facets, computed by
matching both the facet domain and values to relational assertions.

\subsection{Evaluation Metrics}

In our experiments we compare the effectiveness of DRC, MAJ, and MAX using three
different metrics: Normalized Discounted Cumulative Gain (nDCG), Mean Average
Precision (MAP), and Mean Reciprocal Rank (MRR). nDCG metric compares the
ranking of relations established by one algorithm with the ideal ranking
derived by ratings specified by human annotators. Intuitively, a higher nDCG
value corresponds to a better agreement between the results proposed by the
facet annotation approach and an ideal ordering obtained from human annotators.
The nDCG provides a fine-grained measure of the effectiveness of DRC and the
baselines, at different ranks. We use it for comparison on both the DBpedia and
YAGO datasets.

The MAP metric is computed by averaging the precision calculated at the rank of
each \emph{correct} or \emph{vital} relation in $P$ (as judged by human
annotators) for each facet and finally averaged over all the facets. MAP takes
into account the fact that there exists more than one similar relation for
each facet, and provides a single valued, coarse-grained measure of
effectiveness. We use it to for comparison against the DBpedia datasets.
The MRR metric is an adaptation of the MAP measure to consider facets for which
there exists only one relation. MRR is defined as the multiplicative inverse of
the rank of the \emph{vital} relation (if present, 0 otherwise), averaged over
all the facets. Intuitively, MRR captures to which extent an high rank is given
to the similar relation by the algorithm, being maximum when the similar
relation is always ranked first. We use it for comparison against the YAGO gold
standard.

\subsection{Experimental Results}

\concept{DBpedia}

We compare DRC with the baselines with respect to MAP computed over the top 20
similar relations. We assumed all relations judged as either \emph{correct}
or \emph{vital} by human annotators to be equally similar, because we want to
ensure that DRC is able to discriminate between similar and not similar
relations in a boolean fashion. Experimental results, depicted in
Figure~\ref{plot:dbpedia-map}. DRC consistently achieves better effectiveness in
terms of MAP over all the DBpedia datasets. From a more granular point of
view, Figure~\ref{plot:dbpedia-ndcg} gives an overview of the nDCG obtained at
different ranks. DRC is more effective than MAJ and MAX to capture the
similarity at all the ranks from 1 to 20, achieving a maximum nDCG of 0.81
(at rank 1), compared to 0.77 of MAJ and 0.76 of MAX. The interesting
observation is that the more ``naive'' MAJ baseline performs better than the MAX
baseline, at all ranks. These results are compliant the ones provided by the
authors of MAX~\cite{venetisVLDB2011}. In they work, they ultimately composed
their approach with the majority based one (MAJ) to achieve better results.
However, both MAJ and MAX are less effective that DRC in capturing the semantic
similarity between facets and DBpedia relations.

\begin{figure*}[t!]
	\begin{subfigure}[b]{\textwidth}
		\centering
		\hspace{0.82cm}
		\includegraphics[width=0.4\textwidth]{property-interpretation-plots/key.pdf}
		\hspace{0.91cm}
		\includegraphics[width=0.4\textwidth]{property-interpretation-plots/key-ndcg.pdf}
		\hspace{1cm}
	\end{subfigure}
	\begin{subfigure}[b]{.49\textwidth}
		\centering
		\includegraphics[width=\textwidth]{property-interpretation-plots/dbpedia-map.pdf}
		\caption{Whole DBpedia dataset.}
		\label{plot:dbpedia-map}
	\end{subfigure}
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{property-interpretation-plots/dbpedia-whole-ndcg-with-context.pdf}
		\caption{\textbf{dbpedia}.}
		\label{plot:dbpedia-ndcg}
	\end{subfigure}
	\begin{subfigure}[b]{.5\textwidth}
		\includegraphics[width=\textwidth]{property-interpretation-plots/dbpedia-numbers-ndcg-with-context.pdf}
		\caption{\textbf{dbpedia-numbers}.}
		\label{plot:dbpedia-numbers-ndcg}
	\end{subfigure}
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{property-interpretation-plots/dbpedia-no-numbers-ndcg-with-context.pdf}
		\caption{\textbf{dbpedia-entities}.}
	\end{subfigure}
	\caption{Evaluation of DRC over the DBpedia dataset.}
	\label{fig:dbpedia-evaluation}
\end{figure*}


Our interpretation for this behavior is that the baselines are less robust to
ambiguity introduced by the unbalanced specification of facets, which
potentially results into an high number of false positive matches between the
facet and relational assertions. The baselines, which are not explicitly
designed to cope with this distinctive characteristic of facets, are biased towards
relations whose extension induced by the input facet has an higher cardinality,
even if it contains false positive matches. In contrast, DRC is less sensitive to false
positive matches as frequency is weighted by the similarity between the facet
domain and the subject type. For example, the top ranked relation computed by
DRC for the facet \ifacet{\named{Airports}}{\named{Milan}, \named{Rome},\ldots,
\named{Turin}} is $\named{cityServed}$, which is ranked only at the fourth place
by both MAJ and MAX.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.5\columnwidth]{property-interpretation-plots/dbpedia-numbers-ndcg-with-context-comparison.pdf}
	\caption{The effect of the \textit{specificity} score on the
	\textbf{dbpedia-numbers} dataset.}
	\label{fig:dbpedia-numbers-comparison}
\end{figure}

The observation about the robustness of DRC with respect ambiguity of facets is
confirmed by the nDCG achieved by our approach on the \textbf{dbpedia-numbers}
dataset, depicted in Figure~\ref{plot:dbpedia-numbers-ndcg}. The semantics of
digits is ambiguous and is context dependant, as exemplified by the facet
\ifacet{\named{Basketball\;Players}}{\named{1948}, \named{1949}, \ldots,
\named{2012}}. This facet has been extracted from a Web page that lists NBA
basketball players by their draft year. The semantics of this facet is ambiguous
because, besides the draft year (i.e., the year where a player starts playing
for an NBA team as a professional), it may represent for example the birth or
retirement year. However, the draft year is one of the most noticeable relations
of basketball players, while the birth or retirement year characterize people or
athletes in general. Thus, the draft year is \emph{specific} with respect to the
domain of basketball players. In this setting, the specificity scores
implemented by DRC and in particular the domain specificity is able to boost
relations that model the draft year (e.g., the relation $\named{draftYear}$)
over more generic ones (e.g., $\named{birthDate}$). Instead, MAX and MAJ, which
do not capture the specificity of relations, assign an higher rank to relations
such as $\named{birthDate}$ or $\named{deathDate}$.

%\footnote{\url{http://www.nba-allstar.com/players/lists/players-by-draft-year.htm}}

The effect of the \emph{specificity} score on DRC on the
\textbf{dbpedia-numbers} dataset is shown in
Figure~\ref{fig:dbpedia-numbers-comparison}. We compare three different versions
of DRC in terms of nDCG: the first one uses only the frequency score (i.e.,
freq), the second one uses both the frequency and the coverage score (i.e.,
freq+cov), while the third one is the complete DRC ranking function.
Experimental results clearly highlight the importance of capturing the
specificity of relations for facet annotation. The specificity score is crucial
to provide high quality annotations for numeric facets. We also study the effect
of the specificity score over the \textbf{dbpedia-entities} dataset and found
that it provides an observable, though slight, improvement of effectiveness.
This was therefore expected, as entities are less ambiguous compared to numbers.
We report an higher nDCG at all ranks for DRC compared to freq and freq+cov
scores, with an average improvement of around 4.5\% and 5\%, respectively.

\concept{YAGO}

We compare DRC with the baselines with respect to MRR computed over the top 5
similar relations for the YAGO dataset. Insights from the previous experiments
are further confirmed: the most similar relation for facets is generally ranked
higher by DRC compared to baselines, as shown in Figure~\ref{plot:mrr-yago}.

\begin{figure*}[t]
	\begin{subfigure}[b]{\textwidth}
		\centering
		\hspace{0.82cm}
		\includegraphics[width=0.4\textwidth]{property-interpretation-plots/key.pdf}
		\hspace{0.91cm}
		\includegraphics[width=0.4\textwidth]{property-interpretation-plots/key-ndcg.pdf}
		\hspace{1cm}
	\end{subfigure}
	\begin{subfigure}[b]{\textwidth}
		\centering
		\includegraphics[width=.5\textwidth]{property-interpretation-plots/yago-mrr.pdf}
		\caption{Whole YAGO dataset.}
		\label{plot:mrr-yago}
	\end{subfigure}
	\begin{subfigure}[c]{\textwidth}
		\centering
		\vspace{1cm}
	\end{subfigure}
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{property-interpretation-plots/yago-full-ndcg.pdf}
		\caption{\textbf{yago-explicit}.}
		\label{plot:ndcg-yago-explicit}
	\end{subfigure}
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[width=\textwidth]{property-interpretation-plots/yago-abstract-ndcg.pdf}
		\caption{\textbf{yago-ambiguous}.}
		\label{plot:ndcg-yago-ambiguous}
	\end{subfigure}
	\caption{Evaluation of DRC over the YAGO dataset.}
	\label{fig:yago-evaluation}
\end{figure*}


From a more granular point of view, we compare DRC with baselines in terms nDGC
(Figures~\ref{plot:ndcg-yago-explicit} and~\ref{plot:ndcg-yago-ambiguous}). Our
approach is able to provide a better characterization of the similarity between
a facet and relations also in presence of a single, well defined type hierarchy
and a much smaller number of possibly similar relations (89 vs 53195 of the
DBpedia dataset), making it suitable also for the annotation of facets with
precision oriented \kgs. DRC achieves better results at each rank. Observe that
the most noticeable improvement over the baselines is obtained at rank 1. This
is crucial in order to support a fully automatic (and not validated by humans)
facet annotation process. An example of such capability is provided by the facet
\ifacet{\named{Actors}}{\named{Blondie\;Hits\;the\;Jackpot},
\named{Charlie\;Chaplin\;Cavalcade}, \ldots}. For this facet, the top ranked
relation computed by DRC for is $\named{actedIn}$, which is ranked at the second
position (after $\named{created}$) by MAJ and ranked at the third position by
MAX (after $\named{livesIn}$ and $\named{created}$) by MAX.

\begin{figure}[t]
	\begin{subfigure}[b]{.49\columnwidth}
		\centering
		\includegraphics[width=\textwidth]{property-interpretation-plots/yago-full-ndcg-comparison.pdf}
		\caption{\textbf{yago-explicit}.}
	\end{subfigure}
	\begin{subfigure}[b]{.49\columnwidth}
		\centering
		\includegraphics[width=\textwidth]{property-interpretation-plots/yago-abstract-ndcg-comparison.pdf}
		\caption{\textbf{yago-ambiguous}.}
	\end{subfigure}
	\caption{The effect of the \textit{specificity} score over the
	YAGO dataset.}
	\label{fig:yago-evaluation-comparison}
\end{figure}

As for the DBpedia datasets, in our last experiment we study the effect of the
specificity score on the quality of the annotation for facets of the YAGO
datasets. Again, we compare the complete version of DRC with freq and freq+cov
scores. Experimental results depicted in
Figure~\ref{fig:yago-evaluation-comparison} show that the specificity score have
a little impact on the effectiveness of DRC on the YAGO dataset. However, we
recall that the YAGO dataset includes \emph{no} numeric facets, and thus we
expected the specificity score to have little impact on quality in this
particular dataset. Moreover, the subtype graph provided by the YAGO \kg is
deeper and more specialized compared to DBpedia subtype graphs. As a result, the
subject and object type sets extracted from the extension of relations are too sparse
compared to ones extracted from extensions induced by facets, thus making the
specificity score less discriminative. A more deep investigation on how to
better capture the specificity of relations for {\kg}s with similar
characteristics to YAGO represents an interesting extension that we leave for
future work.

