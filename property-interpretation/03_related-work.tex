\section{Comparison with Prior Art}\label{sec:facet-interpretation-related}

The facet annotation problem described in this chapter is similar to the problem
of Web table annotation~\cite{limayeVLDB2010, venetisVLDB2011, shenKDD2012,
wangER2012, querciniEDBT2013, zwicklbauerISWC2013, mulwadISWC2013,
zhangISWC2014, zhangEKAW2014, zhangSWJ2015}.
The general goal of table annotation approaches is to interpret a table by
annotating cells with \kg entities, columns with \kg types, and pairs of columns
with \kg relations.
The approaches that more relevant to ours tackle the problem of relation
annotation~\cite{limayeVLDB2010, venetisVLDB2011, wangER2012, mulwadISWC2013,
zhangSWJ2015}. Other state-of-the-art table annotation approaches that are less
relevant to the contribution described in this chapter are described in detail
in Section~\ref{sec:interpretation}.

Table annotation approaches annotate pairs of columns with relations from a
given \kg~\cite{limayeVLDB2010, venetisVLDB2011, wangER2012, mulwadISWC2013,
zhangSWJ2015}. As stated in Section~\ref{sec:facet-interpretation-problem}, the
main difference between the specification of a facet and a relation between to
table columns is that the first is unbalanced (i.e., the facet domain is
specified differently from the range) while the latter is balanced. That is,
there is a relation holding between values in different columns whenever they
are part of the same row. In contrast, in the facet annotation problem the facet
range is a single set of values and there is no other collection of
corresponding values on which we can rely. Approaches to relation annotation in
web tables heavily rely on the analysis of the reciprocal distributions of
values in different columns, but as part of the same
row~\cite{venetisVLDB2011,zhangSWJ2015}. Due to the unbalanced specification of
facets, this kind of information is not available, when dealing with the facet
annotation problem. However, approaches to relation annotation in web tables
leverage the extensional semantics of relations specified by a \kg.

Venetis et al.~\cite{venetisVLDB2011} build \kg mined from Web pages using the
TextRunner relation extraction framework~\cite{bankoACL2008}. They apply a
maximum likelihood inference model to estimate the probability of a \kg relation
that holds between values from two columns. The same inference model is
leveraged in order to annotate columns with \kg types. The estimation of the
maximum likelihood model for relation annotation is based on computation of the
frequencies of relational assertions within the \kg of all pairs of values of
the same row, and does not consider any type-based representation of \kg
instances. As a result, the approach from Venetis et al. does not allow to
quantify the specificity of a relation with respect to instances of a \kg type.
We claim that this is a crucial feature for a facet annotation approach that
deals with the unbalanced specification of a facet. This claim is substantiated
by results of our experiments from Section~\ref{sec:evaluation}, were we
compared our proposed approach with the maximum likelihood based approach
adapted to consider the unbalanced specification of a facet, and shown that is
less effective in annotating a facet.

Wang et al.~\cite{wangER2012} annotate web tables using the Probase
\kg~\cite{wuSIGMOD2012}. Probase is a probabilistic \kg and it provides scores
that model the \emph{plausibility} and the \emph{ambiguity} of entities being
instance of a certain type and characterized by certain relations.
Those scores are computed during the creation of the \kg. The approach to table
annotation by Wang et al.~\cite{wangER2012} is based on the hypothesis that
tables often describe real world entities and thus they consist of column that
denotes the main entity (i.e., the \emph{subject column}) and multiple relation
columns. They interpret a table by identifying the subject column, annotate it
with the most suitable Probase type and then annotate the remaining columns with
the most suitable relations. Their approach is \kg dependent, since they heavily
rely on the plausibility and ambiguity scores provided by Probase in order to
compute the overall score of a candidate annotation. In contrast, our approach
is \kg agnostic, since we rely solely on types, which are present in any \kg.

TableMiner~\cite{zhangSWJ2015} annotates tables with entities, types and
relations from the Freebase \kg. One of the main contributions of TableMiner is
the usage of contextual information extracted from the Web page that contains the
table (e.g., table caption, surrounding text, RDFa/Microdata annotations).
Following the same intuition of Wang et al.~\cite{wangER2012}, TableMiner starts
with identifying the subject column and provides a set of preliminary entity and
type annotations. Those annotations are then jointly refined using an iterative
learning procedure. Relation annotations are then computed based on the result
of the refinement phase. This approach can hardly be adapted to our problem,
mainly because in our setting we lack contextual information for facets being
them typically the result of automatic facet extraction approaches (as the one
described in Chapter~\ref{cap:consolidation}) and thus they do not have any
caption nor text directly surrounding them and lack any kind of RDFa/Microformat
markup.
