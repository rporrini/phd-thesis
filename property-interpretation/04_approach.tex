\section{Facet Annotation}\label{sec:facet_interpretation-approach}

The intuition underlying our facet annotation approach is to consider the
extensional semantics of relations as a source of \emph{evidence} on the
similarity between $\mfacet$ and a relation $\namedrelation$. As a principle, we
may consider a relational assertion $\namedrelation(\msub, \mobj)$ as positive evidence on
the similarity between $\mfacet$ and $\namedrelation$ if (1) $\msub$ is an instance
of the same type described by the facet domain $\mfdomain$, and (2) $\mobj$ is
equal to some facet value $\mvalue \in \facetrange$. However, the concrete
application of this principles is challenging because (1) both the facet domain and facet
values must be matched with types and relation objects from \kg, and (2) we
cannot assume \kg to completely cover the domain of knowledge conceptualized by
$\mfacet$. In fact in this situation, it may be that \kg only partially represents
the domain of knowledge conceptualized by a facet (i.e., some book authors may
not correspond to any entity in \kg). Thus, although \kg may be a source for
positive evidence, we cannot consider it as a source of negative evidence
whenever the facet domain cannot be matched to any \kg type, or some facet
values cannot be matched to any object. For this reason, our approach relies on
positive evidence only in order to quantify the similarity between $\mfacet$ and
$\namedrelation$. We assume the world conceptualized by \kg to be \emph{open} in
the sense that we admit the annotation of $\mfacet$ with $\namedrelation$, even
when the facet domain partially matches \kg types (i.e., players vs.
basketball players) facet values only overlap with relation objects.

Our facet annotation approach is depicted in Figure~\ref{fig:approach} and its
composed by on two distinct phases: relation \emph{filtering} and relation
\emph{ranking}. Given a facet $\mfacet$, we first match the facet domain and the
facet values to relational assertions $\namedrelation(\msub, \mobj)$ from \kg, so as to
gather evidence from \kg on the similarity between $\mfacet$ and \kg relations.
In this phase, the facet domain and values are matched against a convenient
representation of relational assertions, that we name \emph{relational assertion
signature}. Signatures support the matching between the facet domain and the
types of $\msub$, and between facet values and objects $\mobj$. The result of
the filtering phase is a set of matching signatures for each \kg relation
$\namedrelation$, to which we refer as the \emph{extensions of} $\namedrelation$
\emph{induced by} $\mfacet$. Then, in the ranking phase, we analyze the non
empty extensions of relations induced by $\mfacet$ in order to quantify the degree of
similarity between each relation and $\mfacet$, adhering to principles described in
Section~\ref{sec:facet-interpretation-similarity}: \emph{specificity},
\emph{coverage}, and \emph{frequency}.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{property-interpretation-images/approach-books-refined.pdf}
	\caption{The Facet Annotation process.}	
	\label{fig:approach}
\end{figure} 

\subsection{Relation Filtering}

So as to support the filtering phase, we represent each relational assertion in
\kg by means of a \emph{relational assertion signature} (\emph{signature} for
brevity). A signature $\delta_{(\msub,\mobj)}$ is an \emph{abstract} and
\emph{lexicalized} representation of a relational assertion $\namedrelation(\msub, \mobj)$.
It is \emph{abstract} as includes all the types of the subject $\msub$ as well
as the types of the object $\mobj$. It is \emph{lexicalized} as it includes the
lexicalization of $\mobj$ as well as the lexicalization of \emph{all} the types of
$\msub$. More formally, a signature $\delta_{(\msub,\mobj)}$ is defined as:
\begin{formula}
	\delta_{(\msub,\mobj)}~=~
	< 
	\mtypesof{\msub},~\mtypesof{\mobj},~\mtypelabelsof{\msub},~\mlabelof{\mobj}
	>
\end{formula}
where $\mtypesof{\msub}$, $\mtypesof{\mobj}$ are the set of types of the subject
and the object, respectively and $\mlabelof{\mobj}$ is the lexicalization of the
object. Special attention must be posed to the set of subject types
lexicalizations $\mtypelabelsof{\msub}$, which is defined as the union of the
lexicalizations of \emph{all} the types of the subject. Formally:
\begin{formula}
\mtypelabelsof{\msub} = \bigcup_{t \in \mtypesof{\msub}}
\mlabelof{t}.
\end{formula} 

Through abstraction we explicitly record that \emph{there exists} a
relationship that holds between instances of certain types. In other words, we
adopt an existential semantics for signatures. Such semantics will be
leveraged to quantify the \emph{specificity} of a relation with respect to the
facet domain. Through lexicalization we describe subject types and objects of
relational assertions with a set of natural language terms, that are matched to
facet domain $\mfdomain$ and facet values from $\facetrange$. This allow us to
capture the \emph{coverage} and the \emph{frequency} principles. Signatures are indexed
and stored in a convenient inverted index, so as to support the filtering phase,
where relation extensions are selected by matching the facet with signatures.

We represent the extensional semantics of a relation $\namedrelation$ in terms
of the previously defined signatures. In particular, we represent the
\emph{extension of a relation $\namedrelation$} as the set $\Delta_\namedrelation$ of
signatures $\delta_{(\msub, \mobj)}$ of all relational assertions from the
assertion set $\assertions{}$ of \kg involving $\namedrelation$. Formally:
\begin{formula}
\Delta_\namedrelation = \{ \delta_{(\msub, \mobj)}~|~\namedrelation(\msub,
\mobj) \in \assertions{}\}.
\end{formula}
Given $\mfacet$ and a matching function $match$ between a facet and a signature,
in the \emph{relation filtering} phase, we form the \emph{extension of a relation
$\namedrelation$ induced by $\mfacet$} by selecting the set of signatures of
$\namedrelation$ that match $\mfacet$. Formally:
\begin{formula}
\Delta_\namedrelation^\mfacet = \{\delta_{(\msub, \mobj)} \in
\Delta_\namedrelation~|~match(\mfacet,~\delta_{(\msub,\mobj)})\}.
\end{formula}

$\Delta_\namedrelation^\mfacet$ stores the amount of \emph{positive} evidence that can
possibly be found in \kg on the similarity between $\mfacet$ and $\namedrelation$. Evidence
is collected by matching the facet to signatures. The criteria according to
which signatures are considered to be positive evidence are encoded in the
$match$ boolean matching function. Our approach is based on the combination of
two different matching functions, focusing on matching the facet domain and the
facet values, respectively. The $d\text{-}match$ function selects the signatures
whose subject types match the facet domain. In order to accomplish this task we
consider the lexicalizations of the subject types $\mtypelabelsof{\msub}$ and
compare it with the lexicalization of the facet domain $\mlabelof{\mfdomain}$:
\begin{formula}
d\text{-}match(\mfacet, \delta_{(\msub, \mobj)})~\text{is \emph{true}
\emph{iff}}~\mlabelof{\mfdomain}~\cap~\mtypelabelsof{\msub}~\neq~\emptyset.
\end{formula}
Orthogonally, $r\text{-}match$ function focuses on selecting those signatures
whose object matches \emph{at least one} facet value:
\begin{formula}
r\text{-}match(\mfacet, \delta_{(\msub, \mobj)})~\text{is \emph{true}
\emph{iff}}~\exists \mvalue \in \facetrange~s.t.~\mlabelof{v} \subseteq
\mlabelof{\mobj}.
\end{formula}
We then combine $d\text{-}match$ and $r\text{-}match$ together so as to select
the signatures whose subject types match the facet domain \emph{and}
the object matches at least one facet value. More formally:
\begin{formula}
\Delta_\namedrelation^\mfacet = \{\delta_{(\msub, \mobj)} \in
\Delta_\namedrelation~|~d\text{-}match(\mfacet, \delta_{(\msub, \mobj)})
\wedge~r\text{-}match(\mfacet, \delta_{(\msub, \mobj)})\}.
\end{formula}

In general, the higher the cardinality of $\Delta_\namedrelation^\mfacet$, the more
positive evidence can be found in \kg on the similarity between $\mfacet$ and
$\namedrelation$. Conversely, when $\Delta_\namedrelation^\mfacet = \emptyset$, we can conclude that
$\mfacet$ and $\namedrelation$ are not similar, because there is no evidence in \kg on their
similarity. This relation allows us to prune a relation $\namedrelation'$ when
$\Delta_{\namedrelation'}^\mfacet = \emptyset$. Given $\mfacet$, we query the indexed
signatures and form the set $\mpreds = \{\namedrelation_1,\ldots,\namedrelation_n\}$ of relations
with a non-empty relation extension $\Delta_{\namedrelation_{1}}^\mfacet$, \ldots,
$\Delta_{\namedrelation_{n}}^\mfacet$.

\subsection{Relation Ranking}

In the \emph{relation ranking} phase, extensions selected in the filtering phase
are analyzed so as to assess the similarity between $\mfacet$ and
relations. We encode all the principles described in
Section~\ref{sec:facet-interpretation-similarity} (i.e., \emph{specificity},
\emph{coverage}, \emph{frequency}) into a set of scores that will be aggregated into an overall
semantic similarity score. Finally, we rank the relations in $\mpreds$
accordingly.

\concept{Specificity}
We capture the \emph{specificity} of a relation $\namedrelation$ with respect to
$\mfacet$ by looking at the extension $\Delta_\namedrelation$ and compare them with the
extension $\Delta_\namedrelation^\mfacet$ induced by $\mfacet$. If $\Delta_\namedrelation^\mfacet$
is very ``close'' to $\Delta_\namedrelation$, we may consider the semantics of
$\namedrelation$ to be somehow preserved when annotating $\mfacet$ with
$\namedrelation$. Of course, it is crucial to provide a definition of what we
mean by ``close''. In principle one may apply a majority voting inspired
approach, that is the more $\Delta_\namedrelation^\mfacet$ and $\Delta_\namedrelation$ overlap
(i.e., more ``votes'' for $\namedrelation$), the more we can consider
$\namedrelation$ similar to $\mfacet$. However, this majority-based approach is
likely to fail in capturing the specificity of $\namedrelation$ with respect to
$\mfacet$.

The cardinality of set of facet values $\facetrange$ is usually several
orders of magnitude smaller compared to the cardinality of the set of possible
objects of a given relation $\namedrelation$. As a result, $|\Delta_\namedrelation^\mfacet|
\ll |\Delta_\namedrelation|$, thus making the direct comparison of the two extensions not
discriminative enough. Moreover, $\Delta_\namedrelation^\mfacet$ is the result of a
matching algorithm and it may include signatures resulting from false positive
matches. To account for these issues we consider types instead of signatures. We
compare the sets of subject and object types extracted from the signatures in
$\Delta_\namedrelation$ with the ones extracted from signatures in
$\Delta_\namedrelation^\mfacet$. We follow the intuition that the more these sets are
similar, the more $\Delta_\namedrelation^\mfacet$ is ``close'' to $\Delta_\namedrelation$ and thus
the semantics of $\namedrelation$ is preserved. We consider this a strong
positive clue of the similarity between $\namedrelation$ and $\mfacet$.

Given a generic extension $\Delta$, we form the subject type set $Dom[\Delta]$
by selecting all the types of all the subjects from signatures in $\Delta$. More
formally, the subject type set $Dom[\Delta]$ of an extension $\Delta$ is defined
as
\begin{formula}
\label{eq:subject-types}
Dom[\Delta] = \{ \namedtype~|~\exists\delta_{(\msub, \mobj)} \in \Delta,~\namedtype
\in \mtypesof{\msub} \wedge \not\exists\namedtype' \in \mtypesof{\msub}, \namedtype' \subtype
\namedtype\}.
\end{formula}
Observe that $Dom[\Delta]$ does not contain \emph{all} the subject types.
Instead, it contains all the types that are \emph{minimal}. In general, a type \typ is
minimal for an instance \ent if there are no other types in $\mtypesof{\ment}$
that are subtypes of \typ.

By considering the minimal types only, we enforce $Dom[\Delta]$ to provide a
view of the specificity of the domains of $\Delta$, as $Dom[\Delta]$ contains
the most \emph{specific} types that are used to categorize the subjects of
signatures from $\Delta$. We than capture the \emph{domain}-specificity of a
relation $\namedrelation$ with respect to $\mfacet$ by comparing the subject
type sets extracted from the two extensions $\Delta_{\namedrelation}$ and
$\Delta_{\namedrelation}^{\mfacet}$. As a principle, we consider $\namedrelation$
specific with respect to $\mfacet$ if their extensions contain the same set of
specific subject types. More formally, we compare $Dom[\Delta_{\namedrelation}]$ and
$Dom[\Delta_{\namedrelation}^{\mfacet}]$ using the well known weighted Jaccard similarity
for sets:
\begin{formula}
\label{eq:d-spec}
d\text{-}spec(\mfacet, \namedrelation) = \cfrac{w\text{-}card(Dom[\Delta_\namedrelation] \cap
Dom[\Delta_\namedrelation^\mfacet])}{w\text{-}card(Dom[\Delta_\namedrelation] \cup
Dom[\Delta_\namedrelation^\mfacet])}
\end{formula}
where the $w\text{-}card$ function provides a weighted cardinality of the
subject type set $Dom[\Delta]$. Each type $\namedtype \in Dom[\Delta]$ is
weighted considering the cardinality of the set of its supertypes (i.e., depth)
according to the subtype graph. We furthermore scale the weight of each type \typ by the
maximum depth of all the subtypes of \typ to ensure weights within the
$[0,1]$ interval. More formally:
\begin{formula}
w\text{-}card(Dom[\Delta]) = \sum\limits_{\namedtype \in Dom[\Delta]}
\cfrac{|supertypes(\namedtype)|}{\max\limits_{\namedtype' \in subtypes(\namedtype)}
|supertypes(\namedtype')|}.
\end{formula}
The rationale of using this scaled depth is to further bias the function
$d\text{-}spec$ towards the most specific subject types.

We compute also the \emph{range}-specificity of $\namedrelation$ with respect to $\mfacet$, by
adapting the $r\text{-}spec$ function to consider the object types instead of
the subject types. Similarly to Equation~\ref{eq:subject-types}, we formally
define the set $Ran[\Delta]$ of object types extracted from a generic extension
$\Delta$ as:
\begin{formula}
Ran[\Delta] = \{ \namedtype~|~\exists\delta_{(\msub, \mobj)} \in \Delta,~\namedtype \in
\mtypesof{\mobj} \wedge \not\exists\namedtype' \in \mtypesof{\mobj}, \namedtype' \subtype
\namedtype\}.
\end{formula}
We thus adapt Equation~\ref{eq:d-spec} accordingly: 
\begin{formula}
r\text{-}spec(\mfacet, \namedrelation) = \cfrac{w\text{-}card(Ran[\Delta_\namedrelation] \cap
Ran[\Delta_\namedrelation^\mfacet])}{w\text{-}card(Ran[\Delta_\namedrelation] \cup
Ran[\Delta_\namedrelation^\mfacet])}.
\end{formula}

\concept{Coverage}

We capture the \emph{coverage} of a relation $\namedrelation$ over a facet $\mfacet$ by
computing the rate of facet values for which there exists at least one matched
signature $\delta_{(\msub, \mobj)} \in \Delta_{\namedrelation}^{\mfacet}$. Intuitively,
we aim at capturing percentage of facet values for which there is evidence of them
being object of the relation. More formally:
\begin{formula}
cov(\mfacet, \namedrelation) = \cfrac{|\{v \in \facetrange~|~\exists \delta_{(\msub, \mobj)}
\in \Delta_\namedrelation^\mfacet~s.t.~r\text{-}match(\{v\}, \delta_{(\msub,
\mobj)})\}|}{|\facetrange|}.
\end{formula}
The rationale behind the coverage is to boost those relations whose
objects are equally distributed (i.e., they cover) over the facet value set
$\facetrange$.

\concept{Weighted Frequency}

Observe that both specificity and coverage do not take into account the
cardinality of the extension $\Delta_{\namedrelation}^{\mfacet}$, which can in principle
give a clue on the similarity between $\namedrelation$ and $\mfacet$: the bigger
$\Delta_{\namedrelation}^{\mfacet}$, the more evidence we got from \kg that the
semantics of $\namedrelation$ is preserved when annotating $\mfacet$. However, recall that
$\Delta_{\namedrelation}^{\mfacet}$ may include false positives resulting from the our
matching phase. As a result, not all the evidence gathered from
$\Delta_{\namedrelation}^{\mfacet}$ may equally contribute in assessing the similarity
between $\namedrelation$ and $\mfacet$. In principle, the highest quality evidence that we may
get from \kg are signatures whose subject type is \emph{exactly} the facet
domain $\mfdomain$.

So as to capture this principle, we introduce the \emph{weighted frequency}.
Intuitively, we count signatures in $\Delta_{\namedrelation}^{\mfacet}$, weighting them
considering the similarity between the lexicalization of facet domain
$\mlabelof{\mfdomain}$ and the lexicalization of all subject types
$\mtypelabelsof{\msub}$. Our intuition is that the more similar
$\mlabelof{\mfdomain}$ and $\mtypelabelsof{\msub}$, the higher quality of the
evidence provided by the signature. More formally:
\begin{formula}
freq(\mfacet, \namedrelation) = \cfrac{1}{|\facetrange|} \sum\limits_{\delta_{(\msub, \mobj)} \in \Delta_\namedrelation^\mfacet} \cfrac{|\mtypelabelsof{\msub} \cap \mlabelof{\mfdomain}|}{|\mtypelabelsof{\msub} \cup \mlabelof{\mfdomain}|}
\end{formula}
where the quality of each signature $\Delta_{\namedrelation}^{\mfacet}$ is computed as
the Jaccard similarity between the two lexicalizations. The factor
$1/|\facetrange|$ is introduced to scale down the resulting weighted frequency by
the number of the facet values (i.e., the cardinality of $\facetrange$).

\concept{Computing the Final Rank}

We aggregate all the scores that capture the specificity, coverage and frequency
principles into a single, global similarity score for a relation
$\namedrelation$ and a $\mfacet$. Observe that all the scores provide values within the $[0,1]$
interval, with the noticeable exception of the $freq$ score (i.e., weighted
frequency). The $freq$ score may in principle have a huge impact on the overall
score because provides positive evidence captured in an unbounded way. As a
result, it can possibly skew the overall score.

Thus, to make all the scores more comparable we smooth them by a logarithmic
factor. Moreover, we enforce 1 as the lower bound of each score and then
multiply all the smoothed scores together. This aggregation results in a
similarity score which is a non-decreasing monotone function with
no upper bound and lower bound equal to 1. More formally:
\begin{formula}
	drc(\mfacet, \namedrelation) =
	&\Big( 1 + \ln(d\text{-}spec(\mfacet, \namedrelation) + 1)\Big)~
	\cdot~\\ \nonumber
	&\Big( 1 + \ln(r\text{-}spec(\mfacet, \namedrelation) + 1)\Big)~
	\cdot~\\ \nonumber
	&\Big( 1 + \ln(cov(\mfacet,\namedrelation)+ 1)\Big)~
	\cdot~\\ \nonumber
	&\Big( 1 +\ln(freq(\mfacet,\namedrelation)+ 1)\Big)~
\end{formula}
where the $+ 1$ inside the logarithm ensures each score $\geq 0$ while the $+ 1$
outside the logarithm enforces 1 as lower bound. Intuitively, we thus let each
score to contribute by a positive factor, as we consider positive evidence only.
