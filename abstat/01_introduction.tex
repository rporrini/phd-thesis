\section{Overview}

In this chapter we study how to extract, represent and convey information needed
by domain experts in order to supervise and validate the extraction and
interpretation of facets, discussed in the last chapters. In particular, we
argue that such information can be effectively provided by means of a \emph{summary}
of the \kg, which allows domain experts to answer to questions like: what types
of instances are described in the \kg? What relations are used to characterize
the instances? What types of instances are linked by a certain relation and how
frequently? How many instances have a certain type and how frequent is the use
of a given relation? With such knowledge at hand, domain experts can properly
supervise the extraction and annotation phases and validate their result,
deciding for example to annotate a facet with a relation that they consider more
domain specific with respect to automatically suggested ones.

Here we introduce ABSTAT: a framework that is capable to provide a complete and
compact abstract summary of the content of a \kg, focusing on \kg that are
modeled using the RDF data model. With completeness we refer to the fact that
every relation between types that is not in the summary can be inferred. One
distinguishing feature of ABSTAT is to adopt a minimalization mechanism based on
\textit{minimal type patterns}. A minimal type pattern is a triple
$\pattern{\namedtype}{\namedrelation}{D}$ that represents the occurrences of
assertions $\namedrelation(a,b)$ in RDF data, such that $\namedtype$ is a
minimal type of the subject $a$ and $D$ is a minimal type of the object $b$. By
considering patterns that are based on minimal types we are able to exclude
several redundant patterns from the summary. The
ABSTAT\footnote{\url{http://abstat.disco.unimib.it}} framework supports users to
query (via SPARQL), to search and to navigate the summaries through web
interfaces. The remainder of this chapter is organized as follows. The
summarization model is presented in section \ref{sec:abstat-dataset-profile}.
The implementation of the model in ABSTAT is given in Section
\ref{sec:abstat-computation}. Experiments conducted in order to validate our
proposed approach to \kg summarization are presented in Section
\ref{sec:abstat-evaluation} while we discuss the related  work in
Section~\ref{sec:abstat-related-work}. A final summary of the chapter is
presented in Section~\ref{sec:abstat-conclusion}.
