\section{Summary Extraction}\label{sec:abstat-computation}

Our summarization process, depicted in Figure~\ref{fig:workflow}, takes in input
an assertion set $\assertions{}$ and a terminology $\axioms{}$ and produces a
summary $\Sigma^{\assertions{},\axioms{}}$. First, the typing assertion set
$\assertions{\namedtype}$ is isolated from the relational assertion set
$\assertions{\namedrelation}$, while the subtype graph $\typegraph$ is extracted
from $\axioms{}$. Then, $\assertions{\namedtype}$ is processed and the set of
minimal types for each entity is computed. Finally,
$\assertions{\namedrelation}$ is processed in order to compute the minimal type
patterns that will form the minimal pattern base
$\widehat{\Pi}^{\assertions{},\typegraph}$. During each phase we keep track of
the occurrence of types, relations and patterns, which will be included as
statistics in the summary.

\concept{Summary Extraction} 
The subtype graph $\typegraph$ is extracted by traversing all the subrelation
and subtype relations in $\axioms$. The subtype graph will be further enriched
with types from external ontologies asserted in $\assertions{\namedtype}$ while
we compute minimal types of entities (i.e., \emph{external} types).

\begin{figure}[t] 
	\centering
	\includegraphics[width=\textwidth]{abstat-images/workflow.pdf}
	\caption{The summarization workflow.}
	\label{fig:workflow}
\end{figure}

Given an entity $x$, we compute the set $M_x$ of minimal types with respect to
$\typegraph$. We first select all the typing assertions $\namedtype(x) \in
\assertions{\namedtype}$ and form the set $\assertions{\namedtype}_x$ of typing
assertions about $x$. We then iteratively process $\assertions{\namedtype}_x$.
At each iteration we select a type $\namedtype$ and remove from $M_x$ all the
supertypes of $\namedtype$ according to $\typegraph$.
Then, if $M_x$ does not contain any $\namedtype'$ such that $\namedtype'
\subtype \namedtype $, we add $\namedtype$ to $M_x$. Notice that one preliminary
step of the algorithm is to include $\namedtype$ in $\typegraph$ if it was not
included during the subtype graph extraction phase.
If a type $\namedtype$ is not defined in the input terminology, is automatically
considered as a minimal type for the entity $x$. This approach allows us to
handle the types of entities that are not included in the original terminology.

For each relational assertion $\namedrelation(x,y) \in
\assertions{\namedrelation}$, we get the minimal types sets $M_x$ and $M_y$. For
all $\namedtype, D \in M_x, M_y$ we add a pattern
$\pattern{\namedtype}{\namedrelation}{D}$ to the minimal type pattern base. If
$y$ is a literal value we consider its explicit type if present,
$\named{\escaped{rdfs:Literal}}$ otherwise.

\concept{Summary Storage and Presentation} 
Every summary is stored, indexed and made accessible through two user
interfaces, i.e., ABSTATBrowse\footnote{\url{http://abstat.disco.unimib.it}} and
ABSTATSearch\footnote{\url{http://abstat.disco.unimib.it/search}}, and a SPARQL
endpoint\footnote{\url{http://abstat.disco.unimib.it/sparql}}.
In particular, ABSTATSearch\footnote{\url{http://abstat.disco.unimib.it/search}}
implements a full-text search functionality over a set of summaries. Types,
relations and patterns are represented by means of their local names (e.g.,
\quoted{Person}, \quoted{birthPlace} or \quoted{Person birthPlace Country}),
conveniently tokenized, stemmed and indexed, and retrieved using Lucene Score as
ranking model.
