\section{Summarization Model}\label{sec:abstat-dataset-profile}

Recall from Section~\ref{sec:knowledge-graphs} that a \kg is constituted by a
\emph{terminology} (or signature) $\vocabulary$, a set of \emph{terminological
axioms} $\axioms$ and a set of \emph{assertions} $\assertions{}$
\begin{formula*}
\knowledgraph = < \vocabulary, \axioms, \assertions{} >.
\end{formula*}
The terminology $\vocabulary$ of the \kg contains the set $\namedtypes$
of \textit{types}, the set $\namedrelations$ of named relations and the set of
instances $\namedinstances$ composed by entities and literals. Consistently
with the used in the rest of the dissertation, we use symbols like $\namedtype$,
$\namedtype'$, ..., and $D$, $D'$, ..., to denote types, symbols
$\namedrelation$, $Q$ to denote relations, and symbols $\namedinstance$, $b$ to
denote instances.

Assertions in $\assertions{}$ are of two kinds: \textit{type assertions} of form
$\namedtype(a)$, and \textit{relational assertions} of form $\namedrelation(a,b)$,
where $a$ is an entity and $b$ is either an entity or a literal. We denote the
sets of type and relational assertions by $\assertions{\namedtype}$ and
$\assertions{\namedrelation}$ respectively. Assertions can be extracted directly
from RDF data, even in absence of an input signature. \textit{Type assertions}
occur in a \kg as RDF triples $<x,\named{\escaped{rdf:type}},\namedtype>$ when
$x$ and $\namedtype$ are URIs, or can be derived from triples
$<x,\namedrelation,y\text{\^{}\^{}} \namedtype>$ where $y$ is a literal (in this
case $y$ is a typed literal), with $\namedtype$ being its datatype. Consistently
with definitions from Chapter~\ref{cap:dataspaces}, we say that $x$ is an
instance of a type $\namedtype$, denoted by $\namedtype(x)$, either $x$ is an
entity or $x$ is a typed literal. Every resource identifier that has no type is
considered to be of type $\named{\escaped{owl:Thing}}$ and every literal that
has no type is considered to be of type $\named{\escaped{rdfs:Literal}}$.
Observe that a literal occurring in a triple can have at most one type and at
most one type assertion can be extracted for each triple. Conversely, an
instance can be the subject of several type assertions. A \textit{relation
assertion} $\namedrelation(x,y)$ is any triple $<x,\namedrelation,y>$ such that
$\namedrelation\neq Q$, where $Q$ is either $\named{\escaped{rdf:type}}$ or one
of the relations used to model a terminology (e.g.
$\named{\escaped{rdfs:subClassOf}}$).

\concept{Subtype Graph}
A \textit{subtype graph} is a graph
\begin{formula*}
\typegraph = (\namedtypes,\subtype)
\end{formula*}
where $\namedtypes$ is the set of types (either concept or datatype) and
$\subtype$ is a relation over $\namedtypes$. We always include two type names
in $\namedtypes$, namely $\named{\escaped{owl:Thing}}$ and
$\named{\escaped{rdfs:Literal}}$, such that every concept is subtype of
$\named{\escaped{owl:Thing}}$ and every datatype is subtype of
$\named{\escaped{rdfs:Literal}}$. One type can be subtype of none, one or more
than one type.

\concept{Abstract Knowledge Pattern}
Abstract Knowledge Patterns (AKPs) are abstract representations of Knowledge
Patterns, i.e., constraints over a piece of domain knowledge defined by axioms
of a logical language, in the vein of Ontology Design
Patterns~\cite{staab2010handbook}. For sake of clarity, we will use the term
\textit{pattern} to refer to an AKP in the rest of the Chapter. A pattern is a
triple
\begin{formula*}
\pattern{\namedtype}{\namedrelation}{D} 
\end{formula*}
such that $\namedtype$ and $D$ are types and $\namedrelation$ is a relation.
Intuitively, an AKP states that there are instances of type $\namedtype$ that are linked
to instances of a type $D$ by a relation $\namedrelation$. In ABSTAT we
represent a set of AKP occurring in the \kg, which profiles the usage of the
terminology. However, instead of representing every AKP occurring in the \kg,
ABSTAT summaries include only a base of minimal type patterns, i.e., a subset
of the patterns such that every other pattern can be derived using the subtype
graph.

\concept{Pattern Occurrence} 
A pattern $\pattern{\namedtype}{\namedrelation}{D}$ \textit{occurs} in a set of
assertions $\assertions{}$ iff there exist some instances $x$ and $y$ such that
\begin{formula*}
\{\namedtype(x),~\namedrelation(y),~\namedrelation(x,y)\}\subseteq
\assertions{}.
\end{formula*} 
Patterns are also denoted by the symbol $\pi$. For \kgs that include
the transitive closure of type inference (e.g., DBpedia), the set of all
patterns occurring in an assertion set may be very large and include several
redundant patterns. To reduce the number of patterns we use the observation
that many patterns can be derived from other patterns if we use the
subtype graph that represents types and their subtypes.

\concept{Minimal Type Pattern} 
A pattern $\pattern{\namedtype}{\namedrelation}{D}$ is a \textit{minimal type
pattern} for a relational assertion $\namedrelation(a,b)\in\assertions{}$ and a
subtype graph $\typegraph$ iff $\pattern{\namedtype}{\namedrelation}{D}$
occurs in $\assertions{}$ and there does not exist a type $\namedtype'$ such
that $\namedtype'(a)\in\assertions{}$ and $\namedtype' \subtype
\namedtype$ or a type $D'$ such that $D'(b)\in\assertions{}$ and
$D'\prec^{\typegraph}D$.
 
\concept{Minimal Type Pattern Base} 
A \textit{minimal type pattern base} for a set of assertions
$\assertions{}$ under a subtype graph $\typegraph$ is a set of patterns
$\widehat{\Pi}^{\assertions{},\typegraph}$ such that $\pi \in
\widehat{\Pi}^{\assertions{},\typegraph}$ iff $\pi$ is a minimal type pattern
for some relational assertion in $\assertions{}$.

\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{abstat-images/example-2-complete.pdf}
	\caption{A small \kg and corresponding patterns.}
	\label{fig:example}
\end{figure}

Observe that different minimal type patterns $\pattern{\namedtype}{\namedrelation}{D}$ can be defined
for an assertion $\namedrelation(a,b)$ if $a$ and/or $b$ have more than one minimal type.
However, the minimal type pattern base excludes many patterns that can be
inferred following the subtype relations and that are not minimal type for any
assertion. In the graph represented in Figure~\ref{fig:example}, considering the
assertion set
\begin{formula*}
\assertions{}= \{\namedrelation(a,b),~\namedtype(a),~A(a),~F(b),~D(b),~A(b)\}
\end{formula*} 
there are six patterns occurring in $\mathcal{A}$:
\begin{formula*}
\pattern{\namedtype}{\namedrelation}{D}, \pattern{\namedtype}{\namedrelation}{F},
\pattern{\namedtype}{\namedrelation}{A}, \pattern{A}{\namedrelation}{D}, \pattern{A}{\namedrelation}{F},
\pattern{A}{\namedrelation}{A}. 
\end{formula*}
The minimal type pattern base for the \kg includes the
patterns: 
\begin{formula*}
\pattern{E}{Q}{D}, \pattern{E}{R}{T}, \pattern{\namedtype}{Q}{D},
\pattern{\namedtype}{R}{T}, \pattern{\namedtype}{\namedrelation}{D}
\end{formula*}
since $E$ and $\namedtype$ are minimal types of the instance $c$, while excluding
patterns like $\pattern{B}{Q}{D}$ or even $\pattern{A}{Q}{A}$ since not $B$ nor
$A$ are minimal types of any instance.

\concept{Data Summary}
A \textit{summary} of a \kg $\knowledgraph = < \vocabulary, \axioms, \assertions{} >$ is
a triple
\begin{formula*}
\Sigma^{\assertions{},\axioms}~=~<\typegraph,~\widehat{\Pi}^{\assertions{},\typegraph},~Stat>
\end{formula*}
such that: $\typegraph$ is \textit{Subtype Graph},
$\widehat{\Pi}^{\assertions{},\typegraph}$ is a \textit{Minimal Type Pattern Base} for
$\assertions{}$ under $\typegraph$, and $Stat$ is a set of \textit{statistics} about the
elements of $\typegraph$ and $\Pi$. Statistics describe the occurrences of types,
relations and patterns. They show how many instances have $\namedtype$ as minimal type,
how many relational assertions use a relation $\namedrelation$ and how many instances that
have $\namedtype$ as minimal type are linked to instances that have $D$ as minimal type
by a relation $\namedrelation$.
