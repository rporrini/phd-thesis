\section{Dataspace Components}

\begin{figure}[t]
	\centering
	\includegraphics[width=0.9\textwidth]{images/dataspace.pdf}
	\caption{The schematization of a dataspace.}
	\label{fig:general-dataspace}
\end{figure}

We now provide a more formal definition of a dataspace. A dataspace $\dataspace$
is composed by a Knowledge Graph $\knowledgraph$, a set of data sources
$\datasources$ and a set of mappings $\mappings$ between $\knowledgraph$ and
data sources in $\datasources$:
\begin{formula*}
	\dataspace = <\knowledgraph,~\datasources,~\mappings>.
\end{formula*}
Figure~\ref{fig:general-dataspace} provides a schematization of a
dataspace $\dataspace$. As a remark, in this section we do not aim at
providing a comprehensive formal definition of a dataspace. Instead, we focus
on the concepts that are more relevant to this dissertation: the \kg, the
data sources and the mappings between them. For simplicity and without
loss of generality, we use First Order Logic (FOL) statements to model
all the components of the dataspace described in the following sections. By
using FOL, we discuss this model without being bounded to any specific
representation. In practice, however, different data models
can be adopted to represent the \kg, the sources and the mappings, such as
RDFS, OWL, or the Relational Model, by using well-defined subsets of FOL (e.g.,
Description Logics~\cite{Baader2003} as for OWL2).

\subsection{Knowledge Graph}\label{sec:knowledge-graphs}

A Knowledge Graph (\kg) is constituted by a \emph{signature} $\vocabulary$, a
set of \emph{terminological axioms} $\axioms$ and a set of \emph{assertions}
$\assertions{}$
\begin{formula*}
\knowledgraph = < \vocabulary, \axioms, \assertions{} >.
\end{formula*}

\concept{Instances, Types and Relations}

The signature $\vocabulary$ of a \kg defines the vocabulary of a DI application
domain and is composed by a set $\namedinstances$ of \emph{individuals} (or
constant symbols), a set $\namedtypes$ of \emph{unary predicate} symbols and a
set $\namedrelations$ of a \emph{binary predicate} symbols
\begin{formula*}
\vocabulary = < \namedinstances, \namedtypes, \namedrelations >.
\end{formula*}
Adhering to the standard FOL formalism, given a signature $\vocabulary$ an
\emph{interpretation} $\mathfrak{I}$ is a pair $\langle \mathcal{D}, \mathcal{I}
\rangle$ where $\mathcal{D}$ is any nonempty set of objects, called the
\emph{domain} of the interpretation, and $\mathcal{I}$ is a mapping, called the
\emph{interpretation mapping}, from the non-logical symbols (constants,
predicates, and function symbols) to functions and relations over
$\mathcal{D}$~\cite{Brachman2004}. Signature elements are
interpreted under the standard FOL semantics as:
\begin{itemize}
  \item \emph{Instances} - $\namedinstances$. Instances are of two
  different types: entities (e.g., the instance that describes the city of
  Chicago), and literals (e.g., \literal{2} or \literal{chicago}); we denote
  instances with constants symbols like $\namedinstance$, $b$, \ldots.
  \item \emph{Types} - $\namedtypes$. Provide a categorization of instances, as
  for example a type that represents mobile phones; types are denoted with
  unary predicate symbols like $\namedtype$, $D$, \ldots.
  \item \emph{Relations} - $\namedrelations$. Conceptualize reciprocal
  relationships between instances, as for example the relation that holds
  between a mobile phone and its operating system; relations are denoted with
  binary predicate symbols like $\namedrelation$, $Q$, \ldots.
\end{itemize}
Instances, types and relations are associated to a set of
\emph{lexicalizations}. A lexicalization is a sequence of natural language
tokens that briefly characterizes an instance $\namedinstance$, relation
$\namedrelation$ or type $\namedtype$, respectively. We define a special
function $\thelexicalization(x)$ which tracks the correspondence between a
signature element $x$ (instance, type or relation) and its lexicalizations.

\concept{Assertions}

The set $\assertions{}$ contains assertions in the form of FOL statements about
instances. An assertion is a ground atomic formula built with the signature
$\vocabulary$. We distinguish between two kinds of assertions:
\emph{typing} and \emph{relational} assertions. Typing assertions are in the
form
\begin{formula*}
	\namedtype(\namedinstance),
\end{formula*}
stating that $\namedinstance$ is an instance of the type $\namedtype$.
Similarly, a \emph{relational} assertion in the form
\begin{formula*}
	\namedrelation(\namedinstance, b)
\end{formula*}
states that there is a relation named $\namedrelation$ which holds between
a \emph{subject} instance $\namedinstance$ and an \emph{object} instance $b$.
Through all this dissertation, we will denote with $\assertions{\namedtype}$
the set of all typing assertions, while we will denote with
$\assertions{\namedrelation}$ the set of all relational assertions.

Types and relations provide a representation of instances at different
granularities. Types provide a more coarse grained representation: an instance
is characterized as an instance of one or more types. For example, in the toy
\kg depicted in Figure~\ref{fig:kg} the instance $\named{Iphone6}$ is
characterized as an instance of the type $\named{SmartPhone}$ by means of the
assertion
\begin{formula*}
\named{SmartPhone}(\named{Iphone6}).
\end{formula*}
Conversely, relations provide a fine-grained representation: an instance is
characterized by means of its reciprocal relations with other instances. For
example, the instance $\named{Iphone6}$ may be characterized by its brand
(i.e., $\named{Apple}$), its operating system (i.e., $\named{Ios}$) etc, by
means of assertions such as
\begin{formula*}
\named{os}(\named{Iphone6}, \named{Ios}).
\end{formula*}

\begin{figure}[t] 
	\centering
	\includegraphics[width=0.7\textwidth]{images/knowledge-graph.pdf}
	\caption{A simple \kg.}
	\label{fig:kg}
\end{figure}

\concept{Terminological Axioms}

The set $\axioms$ of terminological axioms provides a specification of types and
relations in the form of FOL statements about them. In practice, such
terminological axioms can be specified using particular languages that
constitute a fixed subsets of FOL with well defined formal properties and
different expressivity, such as
RDFS\footnote{\url{https://www.w3.org/TR/rdf-schema/}} or
OWL2\footnote{\url{https://www.w3.org/TR/owl-overview/}}. Such specifications,
along with types and relations, constitute the \emph{schema} of the
\kg\footnote{For \kgs modeled using RDFS or OWL the schema is usually called
\emph{vocabulary} or \emph{ontology}.}.

One of the most basic, yet common, type of statements are the ones that allow to
specify \emph{subtype} relations between types. Statements such as
\begin{formula*}
\forall x~\big(D(x) \rightarrow \namedtype(x)\big)
\end{formula*}
specify a subtype relation holding between the type $D$ and the type
$\namedtype$ by stating that, whenever $x$ is an instance of $D$, then $x$ is
an instance of type $\namedtype$. From terminological axioms about
types it is possible to extract a \emph{subtype graph}
\begin{formula*}
\typegraph = (\namedtypes, \subtype)
\end{formula*}
where $\subtype$ is a particular relation over $\namedtypes$ introduced to
represent the subtype relation between two types. For instance in
Figure~\ref{fig:kg}, the subtype hierarchy (and hence, the subtype graph
$\typegraph$) of types in $\namedtypes$ can be specified by axioms
\begin{formula*}
\forall x~\big(\named{SmartPhone}(x) &\rightarrow \named{Product}(x)\big)\\
\forall x~\big(\named{Product}(x) &\rightarrow \named{Thing}(x)\big)\\
\forall x~\big(\named{Company}(x) &\rightarrow \named{Thing}(x)\big)\\
\forall x~\big(\named{OperatingSystem}(x) &\rightarrow \named{Thing}(x)\big).\\
\end{formula*}

Besides types, terminological axioms also provide a specification of \kg
relations in terms of their \emph{domain} (i.e., subjects of the relation) and
\emph{range} (i.e., objects of the relation), also called domain and range
\emph{restrictions}. In principle it is possible to specify different kinds of
domain and range restrictions (i.e., by stating what instances belong to the
domain/range). For example, using RDFS it is possible to specify the domain of a
relation $P$ by means of the axiom
\begin{formula*}
\forall x \forall y~\big(\namedrelation(x,y) \rightarrow \namedtype(x)\big),
\end{formula*}
stating that whenever an instance $x$ is subject of a relation $P$, then $x$
is an instance of the type $\namedtype$. Similarly, axioms in the form of
statements like
\begin{formula*}
\forall x \forall y~\big(\namedrelation(x,y) \rightarrow D(y)\big)
\end{formula*}
state that whenever an instance $y$ is object of a relation $P$, then $y$ is
an instance of the type $D$.

Observe that coarse-grained terminological axioms similar to the ones just
described are one of the well established practices to specify a \kg relation,
that is by making use of the RDFS language. Other more expressive languages,
such as OWL2, allow to specify more fine-grained domain or range restrictions.
For example, the axiom
\begin{formula*}
\forall x \forall y~\big( \namedtype(x) \rightarrow \big( \namedrelation(x,y) \rightarrow
D(y) \big)\big)
\end{formula*}
states that whenever $y$ is an object of a relation $\namedrelation$ whose
subject $x$ is an instance of the type $\namedtype$, then $y$ is an instance of
the type $D$. Another example of a more fine-grained specification of a
relation is the axiom
\begin{formula*}
\forall x \forall y~\big( \namedrelation(x,y) \rightarrow \namedtype_1(x) \vee \ldots
\vee \namedtype_n(x) \big)
\end{formula*}
which defines the domain of $\namedrelation$ as the set that includes all
$x$ that are instances of type $\namedtype_1$ or $\namedtype_2$ \ldots
or $\namedtype_n$.

A more ``basic'' language with limited expressive power like RDFS may be easier
to understand and master by domain experts. Moreover, despite the limited
expressive power, RDFS still supports basic inference that allows to derive new
knowledge (in the form of new assertions to be added to the \kg) from existing
assertions and domain and range restrictions. For example, from the following
specification of a relation $\named{os}$
\begin{formula*}
\forall x \forall y ~\big( \named{os}(x,y) \rightarrow \named{SmartPhone}(x)\big)
\end{formula*}
and the relational assertion
\begin{formula*}
\named{os}(\named{iphone6}, \named{ios})
\end{formula*}
it follows that the typing assertion $\named{SmartPhone(iphone6)}$ holds.

The usage of ``basic'' languages in the specification of the types and relations
has some limitations, imposed by their limited expressive power. Suppose, for
example, that domain experts in charge of maintaining the \kg want to reuse the
relation $\named{os}$ introduced to represent the operating system of smart
phones, to characterize also notebooks. Limited by the expressive power of RDFS,
they specify the following terminological axiom
\begin{formula*}
\forall x \forall y ~\big( \named{os}(x,y) \rightarrow \named{Notebook}(x)\big).
\end{formula*}
From this axiom, together with the axiom and assertions provided in the above
paragraph, it can be inferred that $\named{Notebook(iphone6)}$. A common
practice in order to enable the reuse of relations in similar situations is to
\emph{relax} their domain specificity, by defining a new generalist type, say
for example $\named{Device}$, which is a super-type of $\named{SmartPhone}$ and
$\named{Notebook}$ and to update terminological axioms accordingly. The effect
of this design choice is to make the relation less domain specific. In some
cases, where enabling the reuse of relations is the main goal, this design
choice ultimately leads to the definition of generalist relations by completely
relaxing domain and range restrictions (also known as
\emph{underspecification}~\cite{abedjanCIKM2012}). The other
common practice is to use a more expressive language, such as OWL2. However,
this more expressive power comes at the cost of a more difficult
understanding and thus error prone maintenance of the terminological axioms by
domain experts (e.g, risk of enabling undesired
inferences)~\cite{HoganHPDP10,abedjanCIKM2012}.

\subsection{Data Sources as Source Graphs}\label{sec:data-sources} 

Data come from data sources in a variety of different data representation
formats. Examples of these formats are depicted in Figure~\ref{fig:sources}.
Data sources may exchange their data by means of
\emph{structured} formats as exemplified by Figure~\ref{fig:source-structured}
(e.g., a database dump or an RDF graph), \emph{semi-structured} formats as
exemplified by Figure~\ref{fig:source-semi-structured} (e.g., CSV, XML, JSON) or
\emph{unstructured} formats exemplified by Figure~\ref{fig:source-unstructured}
(e.g., text). Regardless of how much structured is the format for data exchange
supported by a data source, a general approach is to represent the source data
as a \emph{source
graph}~\cite{spanosSWJ2012,limayeVLDB2010,sarawagiInformationExtraction,LinkdeData}.
When not explicitly represented~\cite{spanosSWJ2012,LinkdeData} this graph
structure is extracted from data using ad-hoc heuristics for semi-structured
formats (e.g., quasi-relational structure assumption for tabular
data~\cite{limayeVLDB2010}) or NLP techniques for unstructured
formats~\cite{sarawagiInformationExtraction}.

Hence, a source $\datasource$ is defined in a similar way as the \kg, that is
constituted by a \emph{signature} $\source{\vocabulary}$, a set of
\emph{terminological axioms} $\source{\axioms}$ and a set of \emph{assertions}
$\source{\assertions{}}$
\begin{formula*}
\datasource = < \source{\vocabulary}, \source{\axioms}, \source{\assertions{}}
>.
\end{formula*}
Similarly to the \kg, a data source signature $\source{\vocabulary}$ is
constituted by a set of \emph{instances} $\sourceinstances$, \emph{types}
$\sourcetypes$, and \emph{relations} $\sourcerelations$. Source types and
relations are defined as \emph{unary} and \emph{binary} FOL predicates,
respectively. In particular, we write $\sourcetype(\sourceinstance)$ and
$\sourcerelation(\sourceinstance_1, \sourceinstance_2)$ to denote the
classification of a source instance $\sourceinstance$ under the type
$\sourcetype$ (i.e., a typing assertion) and the relation $\sourcerelation$
holding between $\sourceinstance_1$ and $\sourceinstance_2$ (i.e., a relational
assertion), respectively.

\begin{figure}[t]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{images/source-structured.pdf}
  \caption{Structured.}
  \label{fig:source-structured}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{images/source-semi-structured.pdf}
  \caption{Semi structured.}
  \label{fig:source-semi-structured}
\end{subfigure}
\begin{subfigure}{.6\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{images/source-unstructured.pdf}
  \caption{Unstructured.}
  \label{fig:source-unstructured}
\end{subfigure}
\caption{Examples of sources that exchange data by means of
different formats.}
\label{fig:sources}
\end{figure}

Depending on how structured is the format, some information may or may not be
explicitly exposed by the data source. Structured data exchange formats such for
example RDF allow to explicitly represent and exchange the source signature
$\source{\vocabulary}$, the assertions in $\source{\assertions{}}$, and
terminological axioms $\source{\axioms}$ specified using RDFS or OWL2. In
particular, RDF provides a graph-based representation of data which is
explicitly conceived to support data exchange and integration over the
Web~\cite{LinkdeData}. In contrast, semi-structured data formats such as CSV,
XML or JSON do not allow to explicitly represent and exchange terminological
axioms, but only assertions. XML and JSON represent data with graph-like
structures, while CSV represents data as tables. In the latter case, even if not
explicitly represented, a graph-like structure can be extracted by leveraging
the quasi-relational structure of a table~\cite{limayeVLDB2010}\footnote{See
Section~\ref{sec:interpretation-semi-structured} for a more in-depth
discussion.}, as exemplified in Figure~\ref{fig:source-semi-structured}.

Source types and relations provided by a semi-structured data source are not
formally specified, resulting in a lack of explicit machine readable semantics
attached to them, in contrast with structured data sources. The lack of explicit
semantics is more observable in unstructured formats, such as plain or short
texts (e.g., microblog posts). In unstructured formats the signature, the
terminological axioms and assertions are not explicitly represented. Remarkably,
the management of data exchanged in such formats is more challenging and
requires the automatic extraction of source instances, types and relations from
the data, by applying Named Entity Recognition~\cite{nadeau2007survey} and
Relation Extraction~\cite{sarawagiInformationExtraction} approaches.

In summary, a dataspace may potentially integrate data exchanged in a variety of
different formats. In this setting, data \emph{heterogeneity} and
\emph{ambiguity} are two of the main challenges that arise. Source data is
heterogeneous because comes from independent sources, each one with its possibly
own representation. Data is ambiguous because in many cases lacks an explicit
semantics attached. Unstructured data is the most ambiguous, because it lacks
not only semantics, but also structure. In contrast, structured data represented
and exchanged in RDF or more generally through Semantic Web models, languages
and standards is potentially less ambiguous, because its semantics can be
explicitly exchanged. Moreover, a common and established practice is to reuse
the semantics provided by shared ontologies and vocabularies (e.g.,
Foaf\footnote{\url{http://www.foaf-project.org/}}, Dublin
Core\footnote{\url{http://dublincore.org/}},
Schema.org\footnote{\url{http://schema.org/}}), thus potentially reducing
heterogeneity of data provided by different sources~\cite{LinkdeData}. 

\subsection{Mappings}\label{sec:mappings}

A dataspace stores a set of \emph{mappings} $\mappings$ between \kg elements
(i.e., instances, types and relations) and elements from data sources. We define
three types of mappings, and characterize them in terms of the type of elements
between which they hold: \emph{type-to-type} (t-to-t),
\emph{relation-to-relation} (r-to-r) and \emph{instance-to-instance} (i-to-i).
Figure~\ref{fig:mappings} provides and example of these mappings. Observe that
these three types of mappings allow to specify the integration of the source
data into the KG at different granularity levels. Coarse grained integration is
performed by establishing t-to-t coarse grained mappings. The integration is
refined by establishing r-to-r mappings, so as to provide a richer
representation of source instances. Finally, i-to-i mappings provide fine
grained integration. As in traditional data integration
settings~\cite{Lenzerini2002}, mappings are specified using Horn clauses. This
formalism is consistent with the FOL logic-based formalism introduced to
represent the \kg and the sources, as Horn clauses can be represented by means
of FOL formulas.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.8\textwidth]{images/mappings.pdf}
	\caption{Mappings at different granularities.}
	\label{fig:mappings}
\end{figure}

A t-to-t mapping is encoded as a Horn clause in the form:
\begin{formula*}
\namedtype(x) \leftarrow \sourcetype(x)
\end{formula*}
where $\namedtype$ is a \kg type, $\sourcetype$ is a source type and
$x$ is a source instance. The semantics of a t-to-t mapping is
such that all source instances $\sourceinstance$ that are classified as
instances of the source type $\sourcetype$, are also classified as instances of
the \kg type $\namedtype$. For example, the specification of the t-to-t mapping depicted in
Figure~\ref{fig:mappings} between the source type $\named{Product}$ and the \kg
type $\named{SmartPhone}$ can be given by means of the Horn Clause
\begin{formula*}
\named{SmartPhone}(x) \leftarrow \named{Product}(x).
\end{formula*}

Similar to t-to-t mappings, r-to-r mappings are encoded as Horn clauses in the
form:
\begin{formula*}
\namedrelation(x, y) \leftarrow \sourcerelation(x, y)
\end{formula*}
where, $\namedrelation$ is a \kg relation, $\sourcerelation$ is a source
relation and $x$ and $y$ are two source instances. The semantics of a
r-to-r mapping is such that if a source relation $\sourcerelation$ holds
between two source instances $x$ and $y$, then the \kg relation $\namedrelation$
holds between $x$ and $y$. For example, the specification of the r-to-r
mapping depicted in Figure~\ref{fig:mappings} between the source relation
$\named{display}$ and the \kg relation $\named{displaySize}$ can be given by
means of the Horn Clause
\begin{formula*}
\named{displaySize}(x, y) \leftarrow \named{display}(x,y).
\end{formula*}

Instances are mapped by i-to-i mappings, specified Horn clauses with empty head
and using special relations that hold between a source and a \kg instance:
\begin{formula*}
\named{sameAs}(x,y).
\end{formula*}
The (rather trivial) semantics of an i-to-i mapping is such that the source
instance $x$ is equivalent to the \kg instance $y$. For example, the
specification of the i-to-i mapping depicted in Figure~\ref{fig:mappings}
between $\named{SamsungGalaxyA7}$ and the \kg
instance $\named{GalaxyA7}$ can be given by means of the Horn Clause
\begin{formula*}
\named{sameAs}(\named{GalaxyA7},\named{SamsungGalaxyA7}).
\end{formula*}
As a final remark, observe that in principle, the definition given in this
section allows to specify also more complex mappings, such for instance the
t-to-t mapping
\begin{formula*}
	\named{AndroidDevice}(x) \leftarrow
	\named{Product}(x),~\named{operatingSystem}(x,\named{Android})
\end{formula*}
without compromising the formal framework provided by this Chapter. However,
allowing the specification of arbitrarily complex and expressive mappings come
at the cost of sacrificing their ease of maintenance, and is a rare practice in
real world dataspaces.

\section{Facets: End-user Oriented Representation}\label{sec:facets}

\begin{figure}[t]
	\centering
	\includegraphics[width=0.9\textwidth]{images/characterization.pdf}
	\caption{A example of facets built on top of a \kg.}
	\label{fig:facets}
\end{figure}

Within a dataspace, a \kg provides a semantically rich representation of
instances through types and relations. This representation is ``general
purpose'', and supports the spectrum of data management operations performed to
maintain the dataspace. Besides such back-end oriented perspective, however, the
other important goal of a \kg is to enable a DI application to provide end-users
with enhanced access to the data of the dataspace, like one depicted in
Figure~\ref{fig:facets}. One established way to pursue this goal is through the
definition of a particular type of relations, that we name
\emph{facets}~\cite{weiJWE2013,dadzie2011approaches}. A facet is a relation that
represents a \emph{salient} characteristic of instances of a \kg type (i.e.,
domain) and can be informally defined as:

\begin{center}
\fboxsep5mm 
\fbox{\vbox{\hsize=0.8\textwidth\noindent 
    A (1) \emph{mutually exclusive}, and (2) \emph{collectively
    exhaustive} aspect, property, or characteristic of a (3) class or
    \emph{specific subject}~\cite{taylor2004wynar}.
    }}
\end{center}

Facets are relations but, in contrast with the general purpose ones introduced
in Section~\ref{sec:knowledge-graphs}, are meant for data presentation to
end-users. For example, Figure~\ref{fig:facets} depicts a dataspace in which
wine bottles are characterized by facets that include the country of
origin and an year of production (i.e., vintage). On top of this representation
a DI application can provide a faceted search interface where users can use
facets to filter the results of a full text
query~\cite{pietriga2006fresnel,FacetedPedia}. Applying the high level
definition provided in the beginning of this section, a facet is defined as a
relation that:
\begin{enumerate}
  \item has many-1 cardinality (referred to
  ``mutually exclusive'', in the above definition)\footnote{In real world, for
  practical reasons, the constraint on mutual exclusivity may be somehow
  relaxed, as for instance a facet representing actors starring in a movie. In
  this case, although not of many-1 cardinality, the relation represented by
  this facet is still a salient aspect for movies.};
  \item exhaustively covers the set of possible values of the relationship that
  represents (referred to ``collectively exhaustive'');
  \item is domain specific (referred to ``specific subject'');
\end{enumerate}

Observe that a facet is, by all means, a relation. However, a relation is not
necessarily a facet. Consider, for instance, the \kg depicted in
Figure~\ref{fig:facets}, which defines three different relations:
$\named{description}$, $\named{origin}$ and $\named{vintage}$. Among them, the
relation $\named{vintage}$ is a facet for representing wines, because it is
domain specific, in contrast with the relation $\named{description}$ (every
product offer may have a description attached). In fact, the range of
$\named{description}$ potentially contains heterogeneous values: such facet
would be composed by all the descriptions of all \kg instances, and can hardly
be considered a salient characteristic of wine bottles. Through the rest of this
dissertation, with the term \emph{facets} we will refer to this particular type
of \kg relations.

From a formal point of view, a facet holds between a fixed \emph{domain} of
instances of a specific \kg type $\namedtype$ and a \emph{range} of facet values
$\mathcal{V} = \facetvalues$:
\begin{formula*}
\mfacet = < \namedtype,~\facetrange > .
\end{formula*}
Since $\mfacet$ is a relation, its specification is given by a set
of terminological axioms that intuitively model the properties that a relation
must satisfy in order to be considered as a facet:
\begin{formula*}
&\forall x~\big(\namedtype(x) \rightarrow \exists y~\mfacet(x,y)\big)\\
&\forall x~\big(\namedtype(x) \rightarrow \forall z~\big( \mfacet(x,y)~\wedge~\mfacet(x,z) \rightarrow y=z\big)\big)\\
&\exists x~\big(\namedtype(x)~\wedge~\mfacet(x,\mvalue_1)\big)\\
&\ldots\\
&\exists x~\big(\namedtype(x)~\wedge~\mfacet(x,\mvalue_n)\big)
\end{formula*}
where $\mfacet(x,\mvalue)$ is a \emph{faceted assertion}, meaning
that $\mvalue$ is asserted to be the value of the facet $\mfacet$ that
characterizes the instance $x$. Observe that the first two axioms specify the
many-1 cardinality of the facet, while domain specificity with respect to a
specific \kg type $\namedtype$ is enforced by the inclusion of typing assertions
$\namedtype(x)$ in terminological axioms.



Depending on the level of abstraction of the \kg type to which they are
referred, facets can be of two types: generalist or domain specific. For
example, the facet $\named{seller}$ is generalist, as it may be referred to
product offers in general, while the facet $\named{vintage}$ is specific for the
domain of wines. Domain specific facets are crucial in order to provide enhanced
data access to end-users~\cite{pietriga2006fresnel}. By relying on domain
specific facets, a DI application is able to present more interesting and
meaningful information to end-users, which would be more difficult to provide by
relying on generalist facets only. This difference between domain specific and
generalist representations is exemplified by the two faceted search interfaces
depicted in Figure~\ref{fig:characterizations}. The one in the left side is
based on representation given by generalist
facets\footnote{\url{http://bit.ly/less-characterized-wines}, accessed on
February 2016}, while the one at the right is based on domain specific
facets\footnote{\url{http://bit.ly/more-characterized-wines}, accessed on
February 2016}. Wine bottles on the left side are characterized by generalist
facets such as $\named{seller}$ and $\named{price}$, which are applicable to all
the \kg instances (i.e., products in this example). Conversely, in the faceted
search interface on the left side, instances are characterize by domain specific
facets for wines (modeled as a specific \kg type), such as
$\named{grapeVarietal}$ or $\named{countryOfOrigin}$. It is straightforward to
notice that provides a more enhanced user experience to end-users.

\begin{figure}[t] 
	\centering
	\includegraphics[width=1\textwidth]{images/characterizations.pdf}
	\caption{Examples of generalist and domain specific representation of
	\kg instances.}
	\label{fig:characterizations}
\end{figure}

\section{Lifecycle of a Dataspace}\label{sec:ci-cycle}

The predominant data management methodology adopted for dataspaces, and in
particular of the management of \kg and the mappings between its elements and
source data, follows a \emph{pay-as-you-go} approach based on data co-existence
and supervised by domain experts with the use of (semi) automatic
tools~\cite{Halevy-queue05}. In this section we discuss the lifecycle of a
dataspace that supports DI applications, with particular focus on the process of
\emph{bootstrapping} and \emph{maintaining} the \kg and the mappings over time.

\subsection{Example}

To better illustrate the lifecycle of a dataspace, we rely on an example from
the eCommerce domain and in particular a Comparison Shopping Engine (CSE)
similar to the one described in Section~\ref{sec:motivating-example}. In the
case of a CSE, data sources consist of several eMarketplaces exchanging data by
means of structured or semi-structured formats, such for instance tabular CSV
files. The goal of a CSE is to provide users with uniform and advanced access to
offers sold by individual eMarketplaces and integrated into the dataspace.

Figure~\ref{fig:kg-bootstrap} depicts the dataspace of our hypothetical CSE
resulting from the bootstrapping phase. Observe that the \kg specifies only one
type (i.e., $\named{Product}$) of instances. This representation is generalist,
but sufficient to support the establishment of coarse-grained t-to-t mappings.
From an end-user viewpoint, offers are represented by a set of facets such as
$\named{price}$ and $\named{soldBy}$. Observe that, as the \kg specifies
$\named{Offers}$ as the only one type, such facets are somehow domain specific,
given the current status of the \kg (i.e., they are specific for offers).
However, they capture only few salient aspects of offers, being the level of
abstraction of the type $\named{Offer}$ high.  At this stage, source data is
weakly integrated into the dataspace, but the CSE is still able to deliver
search and browse features on top of it. However, such features (i.e., the
faceted search interface depicted in Figure~\ref{fig:kg-bootstrap}) are based on
the representation provided by generalist facets and thus cannot go much far
beyond full text and/or basic faceted search.

\begin{figure}[t]
	\centering
	\includegraphics[width=.9\textwidth]{images/kg-bootstrap.pdf}
	\caption{Bootstrapping a \kg.}
	\label{fig:kg-bootstrap}
\end{figure}

Figure~\ref{fig:kg-consolidation} depicts the dataspace of our hypothetical CSE
after the beginning of the maintenance phase. Now the \kg provides a more rich
and domain specific representation of instances. The \kg has been enriched with
new types, relations and facets, and thus supports the establishment of
fine-grained mappings. The \kg specifies two types of products,
$\named{SmartPhone}$ and $\named{Wine}$, with domain specific facets such as
$\named{displaySize}$ for smartphones and $\named{grape}$ for wines. The
presence of this more fine-grained representation supports the establishment of
fine-grained mappings, thus tightening the integration between sources up. The
CSE now starts to deliver enhanced user-experience on top of the domain specific
representation provided by the \kg.

\begin{figure}[t]
	\centering
	\includegraphics[width=.9\textwidth]{images/kg-consolidation.pdf}
	\caption{Maintaining a \kg.}
	\label{fig:kg-consolidation}
\end{figure}

\subsection{Model, Map and Materialize}

Three different but related data management tasks are continuously performed
during the whole lifespan of a dataspace, from bootstrapping to maintenance: the
\emph{modeling} of the schema of the \kg, the definition of \emph{mappings}
between the sources and the \kg, and \emph{materialization} of integrated data
into the dataspace. Modeling accounts to enrichment of the schema of the \kg
with types, relations and facets. The goal of the first two activities (i.e.,
modeling and mapping) is to enable the materialization phase, where mappings
from the sources to \kg types and relations defined in the modeling phase are
automatically leveraged in order to transform source data and import it into the
dataspace. 

Observe the materialization of source data is not strictly necessary, at least
in principle. Mappings are a set of ``transformation'' rules (e.g, from a source
type to a \kg type) that can be leveraged to enable the formulation of queries
over the integrated data without requiring to materialize it, in the vein of
virtual data integration methodologies. However, materialization is necessary
for the case of dataspaces of DI applications that require further bulk analysis
or processing over the integrated data (e.g, indexing into a full text search
engine). Conversely, DI applications that have specific requirements concerning
the freshness of data (e.g., a CSE for flights) may choose to materialize data
in response to end-users queries and apply mappings to transform data ``on the
fly''.

In this methodology, the integration between data sources is initially weak
(e.g, few coarse grained t-to-t and/or r-to-r mappings). The representation
provided by the \kg is incrementally enriched (i.e., modeling) over time with
new types, relations, and facets for data presentation, which in turn supports
the refinement of the integration of source data via the establishment of new
and more fine-grained mappings. As the integration is tightened, and the
representation is richer, the \kg is incrementally enriched with domain specific
facets, so as to support more advanced data access features, from and end-user
perspective. This pay-as-you-go approach based on the refinement of the
integration over time differentiates this methodology for managing a dataspace
from traditional data integration methodologies that require fully fledged
integration before any service can be delivered to
end-users~\cite{Lenzerini2002}.

Modeling, mapping, and materialization are performed during the whole lifecycle
of the dataspace, with one remark concerning modeling. In fact, while it is
true that modeling is performed during the entire lifecycle of a dataspace, it
has different goals whenever performed in the bootstrapping or maintenance
phases, as schematized in Figure~\ref{fig:kg-lifecycle}. The main goal of the
bootstrapping phase is to provide a representation of data so as to immediately
start the integration of data sources. In this phase, the representation
provided by the \kg is generalist, and aims at \emph{covering} the higher amount
of data as possible. For example, in a CSE, data may be represented with a
generalist type such as $\named{Offer}$, and characterized by a $\named{price}$
(a generalist facet). Conversely, the main focus during the maintenance phase is
to provide a richer and \emph{domain specific} representation of data, so as to
tighten the integration up via the establishment of new mappings, but also to
enable advanced data access features through the definition of domain specific
facets.

\begin{figure}[t]
	\centering
	\includegraphics[width=.9\textwidth]{images/extraction-interpretation.pdf}
	\caption{Evolution of a dataspace over time.}
	\label{fig:kg-lifecycle}
\end{figure}

\subsection{Role of Domain Experts in Data Management Tasks}

The contribution of domain experts is fundamental in the management of the
dataspace, as they are in charge of incrementally drive, supervise, and validate
all the data management tasks presented in the last section. Through an
effective supervision, domain experts incrementally enrich the \kg with a richer and
domain specific representation of instances, and establish mappings between
elements of such representation and the sources. Providing automatic support for
domain experts in these tasks is crucial for a proper and effective management
of the dataspace. 

Domain experts may be relatively autonomous in modeling the \kg during the
bootstrapping phase because it requires the definition of a generalist
representation. However, enriching the schema so as to provide a more rich and
domain specific representation of instances once in the maintenance phase,
requires a deep understanding of the salient characteristics of instances for a
large number of diverse domains, and thus is difficult and time consuming at a
large scale. Intelligent tools are needed also to support domain experts in the
establishment of mappings, to ultimately ensure an high quality of the data in
the dataspace. As a final remark, domain experts must be equipped not only with
tools that actually enrich the schema of the \kg and establish mappings, but
also with the necessary information to proper supervise such tools and validate
their results, in order to ensure the adequate quality of data.

\section{Summary}

In this chapter, we described the anatomy of a dataspace, its components and how
these components, and in particular domain specific facets, provide a rich
representation that can be leveraged by DI applications to deliver advanced data
access features to end-users. We described the lifecycle of a dataspace,
focusing on the bootstrapping and maintenance of the dataspace and its
corresponding \kg over time.

Domain experts in charge of supervising the whole data management process of a
dataspace face hard challenges. Enriching a \kg with a domain specific
representation of instances is difficult, but nevertheless crucial. Such process
requires, among other things, the extraction of end-user oriented relations that
model the \emph{salient} characteristics of instances of a \kg type, which are
named facets, and their consequent domain specific interpretation. However, this
is particularly challenging and time consuming for domain experts to be
performed at a large scale, because it requires extensive knowledge of disparate
domains. Moreover, domain experts in charge of maintaining the dataspace and the
\kg must be equipped not only with tools to actually perform a diverse set of
data management tasks, but also with the necessary information to proper
supervise such tools and validate their results, in order to ensure the adequate
quality of data.
