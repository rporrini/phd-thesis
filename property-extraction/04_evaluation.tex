\section{Evaluation}\label{sec:facet-extraction-evaluation}

The core idea of our proposed approach to domain specific facet extraction is
that we group facet values according to a structural criterion (i.e., TLD).
Hence, we focus on evaluating the facet value clustering phase. Our goal is to
show that TLD effectively captures the STME principle and supports domain
experts in facets definition. To the best of our knowledge there are no distance
metrics for taxonomies that explicitly aim at capturing the STME principle.
However, structural similarity metrics that consider path distance between types
of a taxonomy are good candidates to compare our work to. Intuitively, the more
two source types co-occur in the same source taxonomy path (i.e., they are
similar to some degree according to structural similarity metrics) from the root
to a leaf, the less they are mutually exclusive and the more they should be
clustered into different facet ranges (i.e., the clustering algorithm should
consider them distant from each other).

In the experiments described in this section, TLD is compared with two known
structural type similarity metrics, namely Leacock and
Chodorow~\cite{leacock1998combining} (LC) and Wu and Palmer~\cite{WuAndPalmer}
(WP) metrics. Both LC and WP achieve high effectiveness results in determining
the similarity of concepts within the WordNet taxonomy~\cite{Schwartz}. LC
measures the similarity between two taxonomy types by considering the shortest
path between them and scaling it by the depth of the taxonomy. Similarly, WP
measures the similarity between two types by considering the distance from their
nearest common ancestor and the distance of the nearest common ancestor from the
taxonomy root. We adapted LC and WP to the case of multiple taxonomies. More
specifically, given two source types $\source\namedtype$ and $\source{D}$
we evaluate their LC and WP similarities for each source taxonomy where
$\source\namedtype$ and $\source{D}$ co-occur and we take the mean similarity as
the final distance value.

\subsection{Gold Standard}
We created a gold standard from the real world TrovaPrezzi Italian PCE
dataspace. We chose ten TrovaPrezzi global categories and ran the Values
Extraction phase over them. We presented the set of top $k$ frequent values to
TrovaPrezzi domain experts, who found that relevant facet values generally
appear among the top 100 ranked values. Thus we choose $k = 100$ as cardinality
of the set of extracted facet values. Facet values were manually grouped
together by a domain expert from TrovaPrezzi mapping team and facets were then
validated by other domain experts in order to ensure their correctness. As we
expected, some of the values were discarded by domain experts as they could be
added to any existing facet range.

Table~\ref{tab:gold-standard} shows some statistics about the Gold Standard.
Gold Standards' \kg types cover different domains and a relevant portion of the
overall \kg of the CSE, that is 688 source taxonomies and 22594 leaf mappings.
For each source taxonomy an average of about 33 mappings have been specified.
Moreover, for 322 source taxonomies mappings to more than one \kg type have been
specified. Notice that all the data upon which we created the gold standard are
lexicalized in Italian. Our approach to domain specific facet extraction is
language independent, thus results that we present in following sections are
comparable to others obtained considering different languages. For sake of
clarity, we provide examples translated to English.

\begin{smalltable}
	\centering
	\caption{Gold Standard statistics.}
	\begin{tabular}{ l c c c c }
	\toprule
	\midrule
	& \textbf{\#Source Taxonomies} & \textbf{\#Mappings} & \textbf{Mean} &
	\textbf{Std.Dev.} \\ \midrule
	Dogs and Cats Food & 80 & 5592 & 69.90 & 266.95 \\
	Grappe, Liquors, Aperitives & 115 & 1254 & 10.90 & 24.49 \\
	Wines & 184 & 8967 & 48.73 & 324.63 \\ 
	Beers & 58 & 156 & 2.69 & 3.84 \\ 
	DVD Movies & 164 & 2042 & 12.45 & 45.48 \\
	Rings & 138 & 936 & 6.78 & 13.61 \\ 
	Blu-Ray Movies & 55 & 395 & 7.18 & 16.67 \\
	Musical Instruments & 128 & 1306 & 10.20 & 27.49 \\   
	Ski and Snowboards & 55 & 790 & 14.36 & 23.45 \\
	Necklaces & 148 & 1156 & 7.81 & 17.88 \\ \midrule
	\textbf{Overall} & \textbf{688} & \textbf{22594} & \textbf{32.84} & \textbf{195.79} \\ 
	\midrule
	\bottomrule
	\end{tabular}
	\label{tab:gold-standard}
\end{smalltable}

\subsection{Evaluation Metrics}
We evaluate our facet extraction approach from two different perspectives: facet
value effectiveness and value clustering effectiveness. This kind of evaluation
campaign has been previously used to evaluate several facet extraction
approaches~\cite{kongSIGIR2013,douCIKM2011}. We introduce the notation we will
use in the rest of the section. Given a \kg type $\namedtype$, we denote with
$\nobjects{\namedtype}{}$ the set of discovered facet values (i.e., values that
have not been classified as noise by the algorithm). We denote with
$\nobjects{\namedtype}{*}$ the set of gold standard facet values (i.e., values not
classified as noise by domain experts). Lastly, we denote with
$\facetranges{\namedtype}$ the set of manually discovered facet ranges (i.e., the
gold standard for the \kg type $\namedtype$), which is compared to the set
$\facetranges{\namedtype}_*$ of automatically discovered facet ranges.

\concept{Value Effectiveness}
In our proposed approach noisy values are discarded. In order to evaluate the
ability of our technique to filter noisy values out we compare sets
$\nobjects{\namedtype}{}$ and $\nobjects{\namedtype}{*}$, using Precision ($P$),
Recall ($R$) and F-Measure ($F_1$). All these metrics do not take clustering
effectiveness into account.

\concept{Value Clustering Effectiveness}
We evaluate clustering effectiveness using several standard clustering quality
metrics, that are Purity ($P^*$), Normalized Mutual Information ($NMI^*$),
Entropy ($E^*$), and F-Measure for clustering ($F^*$).
One remark about the usage of these evaluation metrics is that the set of facet
values clustered by our approach is different from the set of facet values
grouped by humans (i.e., $\facetranges{\namedtype} \neq \facetranges{\namedtype}_*$).
We may fail in including meaningful values into some clusters, or we may
mistakenly include noisy values into some facets. Clustering quality metrics
cannot handle these cases. Thus, we modify facet ranges in
$\facetranges{\namedtype}$ by (1) removing all noisy values and by (2) adding to
$\facetranges{\namedtype}$ as single value facet ranges all gold standard values
that have been automatically classified as noise. These adjustment ensures that
$\facetranges{\namedtype} = \facetranges{\namedtype}_*$ and thus clustering quality
metrics can be used properly. With this adjustment, facet value effectiveness is
not considered.

\concept{Overall Quality} 
In order to evaluate the overall effectiveness of our approach, we aggregate
facet value precision $P$, facet value recall $R$ and clustering F-measure $F^*$
into an overall quality measure. The $PRF^*$ measure combines $P$, $R$ and $F^*$
by means of an armonic mean:
\begin{formula*}
	PRF^* = \frac{3 * P * R * F^*}{R*P + P*F + P*R}.
\end{formula*}

\subsection{Experimental Results}

We conducted several experiments, comparing clustering performance of TLD, LC
and WP metrics. We recall from Section~\ref{sec:clustering} that the DBSCAN
algorithm used for clustering is configured with a maximum distance threshold
$\epsilon$. Optimal values of $\epsilon$ depend on the used distance metric, and
influence clustering performance. The tuning of $\epsilon$ can be driven by two
orthogonal factors: overall quality (i.e., $PRF^*$) and the number of discovered
clusters. High values of $\epsilon$ (i.e., quality oriented configuration) can
lead to better overall quality, but fewer discovered clusters (i.e., the
clustering algorithm will tend to group values into one single cluster). Lower
values of $\epsilon$ (i.e., cluster number oriented configuration) can lead to
lower quality, but more discovered clusters. We found that quality oriented and
cluster number oriented configurations generally coincide except for LC. In the
following section we refer to quality oriented configuration of LC as LC$_{q}$
while we indicate with LC$_{n}$ the corresponding cluster number oriented
configuration. Since optimal configurations for WP and TLD coincide we omit
pedices for them.

Table~\ref{tab:evaluation} presents results of our experiments. TLD is more
effective in finding relevant facet values and discarding noisy ones, as
indicated by an higher $F_1$. The ability of effectively discarding noisy values
substantially reduces domain experts' effort in validating discovered facets.
LC$_{q}$ and WP obtain almost perfect value recall, but substantially lower
precision. Thus, they do not effectively support domain experts. Moreover, TLD
achieves best performance according to quite all clustering effectiveness
metrics, with the exceptions of purity and entropy for LC$_{n}$.
Clusters discovered by LC$_{n}$ contain more homogeneous values, in the sense
that they have been manually classified as belonging to the same gold standard
group. However, LC$_{n}$ achieves better purity and entropy at the cost of
discarding most of the values as noise, thus sacrificing overall quality.

\begin{smalltable}
	\caption{Effectiveness of TLD, LC and WP metrics.}
	\label{tab:evaluation}
	\begin{tabularx}{\textwidth}{@{}l *8{>{\centering\arraybackslash}X}@{}}
	\toprule
	\midrule
	& \multicolumn{3}{c}{\textbf{Value Effectiveness}} &
	\multicolumn{4}{c}{\textbf{Clustering Effectiveness}} & \textbf{Quality} \\
 	& $P$ & $R$ & $F_1$ & $F^*$ & $NMI^*$ & Purity & $E^*$ & $PRF^*$\\\midrule
	LC$_{n}$ & 0.359 & 0.447 & 0.370 & 0.403 & 0.603 & \textbf{0.308} & \textbf{0.243} & 0.359 \\
	LC$_{q}$ & 0.394 & 0.953 & 0.537 & 0.666 & 0.709 & 0.220 & 0.685 & 0.531 \\
	WP & 0.377 & \textbf{0.984} & 0.525 & 0.682 & 0.714 & 0.210 & 0.744 & 0.520 \\
	TLD & \textbf{0.416} & 0.901 & \textbf{0.541} & \textbf{0.719} & \textbf{0.746} & 0.286 & 0.416 & \textbf{0.558} \\
	\midrule
	\bottomrule
	\end{tabularx}
\end{smalltable}

The difference between TLD and state-of-the-art metrics is even more evident if
we consider the number of detected clusters for each gold standard type
$\namedtype$ (Table~\ref{tab:clusters}). WP and LC$_{q}$ fail in properly
partitioning the overall set $\nobjects{\namedtype}{k}$ of facet values, thus
failing in detecting groups (i.e., they detect only one or two clusters). They
are too inclusive and thus they group facet values at a granularity level that
is too high to be suitable for effectively supporting domain experts in
bootstrapping a faceted classification system within the dataspace. From the
other side, LC$_{n}$ discards too much values to be effective.

\begin{smalltable} 
	\caption{Number of groups discovered by TLD, LC, and WP for each source type,
	compared to the gold standard.}
	\label{tab:clusters}
	\begin{tabularx}{\textwidth}{@{}l *5{>{\centering\arraybackslash}X}@{}}
	\toprule
	\midrule
	& $|\facetranges{\namedtype}_*|$ & LC$_{q}$ & LC$_{n}$ & WP & TLD \\ \midrule
	Dogs and Cats Food  & 3 & 1 & 5 & 1 & 7 \\
	Grappe, Liquors, Aperitives  & 1 & 1 & 5 & 1 & 6 \\
	Wines  & 3 & 1 & 1 & 1 & 6 \\
	Beers  & 2 & 6 & 4 & 3 & 14 \\
	DVD Movies  & 2 & 2 & 3 & 1 & 3 \\
	Rings  & 4 & 1 & 6 & 2 & 7\\
	Blu-Ray Movies  & 2 & 2 & 3 & 2 & 5\\
	Musical Instruments  & 6 & 1 & 3 & 1 & 5\\
	Ski and Snowboards  & 1 & 1 & 3 & 1 & 7\\
	Necklaces  & 8 & 2 & 6 & 3 & 11\\
	\midrule
	\bottomrule
	\end{tabularx}
\end{smalltable}

In addition to standard evaluation metrics, we provide a more intuitive insight
of results of the facet extraction process, using TLD for facet value clustering
compare to state-of-the-art metrics. Table~\ref{tab:groups} depicts an example
of facets ranges discovered for the \kg type $\named{Wines}$ by TLD, WP,
LC$_{q}$, and LC$_{n}$ compared to manually defined ones. Validating and
refining ranges discovered by TLD requires much less domain experts' effort than
LC$_{q}$, LC$_{n}$ and WP.

\begin{smalltable}
	\caption{Discovered ranges of domain specific facet ranges for TLD, WP and LC
	compared to manually discovered ones. Numbers after facet ranges indicate
	their cardinality.}
	\label{tab:groups}
	\begin{tabularx}{\textwidth}{l X}
	\toprule
	\midrule
	LC$_{q}$ & $\facetrange_1 = \{$Wine, Red Wine, White Wine, \ldots, Piedmont,
	Lombardy, \ldots, Sicily, Donnafugata, Cusumano, \ldots, Alessandro di
	Camporeale, \ldots, France$\}$ (98)\\ \midrule LC$_{n}$ & $\facetrange_1 = 
	\{$Wine, Red Wine, White Wine, \ldots, France, \ldots, Chianti$\}$ (36)\\
	\midrule WP & $\facetrange_1 = \{$Wine, Red Wine, White Wine, \ldots ,
	Piedmont, Lombardy, \ldots , Sicily, Donnafugata, Cusumano, \ldots, France$\}$
	(100)\\ \midrule \multirow{6}{*}{TLD} & $\facetrange_1 = \{$ Piedmont,
	Tuscany, Sicily, , \ldots, France $\}$ (14) \\
	&  $\facetrange_2 = \{$ Red, White, Ros\'e $\}$ (3) \\
	&  $\facetrange_3 = \{$ Red Wine, White Wine, Ros\'e Wine $\}$ (3) \\
	&  $\facetrange_4 = \{$ Moscato, Chardonnay, \ldots, Merlot $\}$ (13) \\
	&  $\facetrange_5 = \{$ Tuscany Wine, Sicily Wine$\}$ (2) \\ 
	&  $\facetrange_6 = \{$ Donnafugata, Cusumano, \ldots, Principi di Butera
	$\}$ (27)\\ \midrule \multirow{3}{*}{Gold Standard} & $\facetrange_1 = \{$ Piedmont,
	Lombardy, \ldots, Sicily $\}$ (21)\\
	 & $\facetrange_2 = \{$ Red Wine, White Wine, \ldots, Ros\'e Wine
	 $\}$ (14)\\
	 & $\facetrange_3 = \{$ Donnafugata, Cusumano, \ldots, Alessandro di
	 Camporeale$\}$ (12) \\ 
	 \midrule
	 \bottomrule
	\end{tabularx}
\end{smalltable}

Table~\ref{tab:groups} highlights a difficulty of TLD in grouping together
different lexicalizations of same values (e.g., $\named{Red\;Wine}$ and
$\named{Red}$). One naive approach to overcome this difficulty is to normalize
source type names by removing terms belonging to the \kg types for which the
facets are extracted (e.g., the term $\named{Wine}$ when extracting facets to
characterize instances of the \kg type $\named{Wines}$). However, this naive
solution cannot be generalized to every \kg type. For example, if we remove from
the source type $\named{Dog\;Food}$ all the terms belonging to the gold standard
\kg type $\named{Dogs\;and\;Cats\;Food}$ we end up with an empty, inconsistent
facet value. Moreover, also the more conservative approach of removing \kg type
terms only if they \emph{all} occur in the source type cannot be generalized.
For example, if we consider the gold standard type
$\named{Musical\;Instruments}$, using the more conservative approach we will not
normalize source types $\named{Wind\;Instruments}$ and $\named{Winds}$.

We implemented and evaluated both the previously described naive solutions, and
found that they both decrease the effectiveness of our approach. We believe that
effectively solving the problem of different lexicalizations requires Natural
Language Processing language specific techniques. NLP techniques can be used to
discriminate between \kg types terms that refer to nouns, verbs, etc. and thus
can be safely removed from source types without creating inconsistencies or
change types lexicalizations' semantics. Introducing this kind of NLP language
specific techniques comes at the cost of sacrificing the language independence
of our approach. However, this represents an interesting extension of our
approach.
