\section{Problem Definition}\label{sec:facet-extraction-problem}

Our goal is to extract a set of facets so as to provide a domain specific
representation of instances of a specific \kg type. As input of the problem, we
assume that there exists a \kg subtype graph and a set of mappings between types
from a set of source taxonomies and \kg types. We assume that the mappings have
many-to-one cardinality, i.e., many source types in each source taxonomy are
mapped one \kg type. In the rest of this section we recall the formal
definitions introduced in Chapter~\ref{cap:dataspaces} and define the Domain
Specific Facet Extraction problem.

\concept{\kg Subtype Graph and Types}
The \kg subtype graph $\typegraph = (\namedtypes, \subtype)$ consists of a set
of $\namedtypes$ of \kg types and a subtype relation $\subtype$. We recall from
Section~\ref{sec:knowledge-graphs} that such subtype graph is constructed from
terminological axioms about \kg types.

\concept{Source Taxonomy and Types}
A source taxonomy $\source{\typetaxonomy} = (\source{\namedtypes},
\source{\subtype})$ consists of a set $\source{\namedtypes}$ of source types and a subtype relation
$\source{\subtype}$ that imposes a partial order over $\source{\namedtypes}$. We
also assume that a source type $\source{\namedtype}$ is associated to one or
more lexicalizations. Consistently with Section~\ref{sec:facets}, we assume
that the correspondence between a source type $\source{\namedtype}$ and its
lexicalizations is provided by the values taken by the function
$\thelexicalization(\source{\namedtype})$.

\concept{T-to-t Mapping}
A t-to-t mapping $\namedtype(x) \leftarrow \source{\namedtype}(x)$ is a
correspondence between a \emph{leaf} type $\source{\namedtype}$ of some source
taxonomy $\source{\namedtypes}$ and a \kg type $\namedtype$. The semantics of a
t-to-t mapping between $\source{\namedtype}$ and $\namedtype$ is that instances
that are classified under $\sourcetype$ at the source are classified as
instances of $\namedtype$ once they are integrated into the dataspace.

\concept{Facet}
Recall from Section~\ref{sec:facets} that a \emph{facet} can be defined as ``a
clearly defined, mutually exclusive, and collectively exhaustive aspect,
property, or characteristic of a class or specific
subject''~\cite{taylor2004wynar}. A facet $\mfacet = < \namedtype, \facetrange
>$ is a relation that holds between a \emph{domain} of instances of
the \kg type $\namedtype$ and a \emph{range} of facet values $\facetrange =
\facetvalues$. For example, a facet such as $\mfacet = <\named{Wines},
\{\named{Italy}, \named{France}, \ldots, \named{Chile}\}>$ may conceptualize the
relation that holds between instances of the \kg type $\named{Wines}$ and the
respective country of origin.

\concept{Facet Extraction}
Given a \kg type $\namedtype$ and a set of t-to-t mappings between source
types $\source\namedtype_1, \ldots, \source\namedtype_i$ and $\namedtype$,
extract a set of facets $\dsfacets{\namedtype}$ that provide a domain specific
representation of instances of $\namedtype$. More formally, given $\namedtype$
and mappings between source types $\source\namedtype_1, \ldots,
\source\namedtype_i$ and $\namedtype$, we aim at extracting a set of facets
\begin{formula*}
\dsfacets{\namedtype} = \{\mfacet_1 = <\namedtype, \facetrange_1>, \ldots,
\mfacet_n = <\namedtype, \facetrange_n> \}
\end{formula*}
where the domain of all the facets is fixed and includes all instances of the
\kg type $\namedtype$. Observe that, as the facet domain is fixed, solving the
Domain Specific Facet Extraction Problem basically accounts to the extraction of
facet ranges $\facetranges{\namedtype} = \{\facetrange_1, \ldots,
\facetrange_n\}$ for domain specific facets, given a \kg type $\namedtype$.
