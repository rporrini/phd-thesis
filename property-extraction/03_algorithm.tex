\section{Approach}\label{sec:facet-extraction-algorithm}

The approach to facet extraction proposed in this dissertation is sketched in
Figure~\ref{fig:process} and is aimed to support domain experts who are in
charge of maintaining domain specific \kgs and corresponding mappings. Domain
experts trigger the extraction process for a specified \kg type. An automatic
facet extraction algorithm suggests a set of domain specific facets to domain
experts, who inspect, validate and refine them, deciding which facets will be
consolidated into the \kg.

\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{property-extraction-images/approach-detailed-grey-final.pdf}
	\caption{Overview of the proposed approach to domain specific facet
	extraction.}
	\label{fig:process}
\end{figure}

The automatic facet extraction algorithm at the core of the proposed approach is
inspired by the following principle: specialized taxonomies used in data sources
contain information that can be analyzed to extract a set of significant domain
specific facets that characterize instances of a specific \kg type.
The facet extraction algorithm extracts the set of facets
$\dsfacets{\namedtype}$ for a \kg type $\namedtype$ using a two-phase process:
\begin{enumerate}
  \item \textbf{Value Extraction:} a set of normalized facet values is produced
  by case lowerization, special characters removal and stemming of all the
  source types mapped to $\namedtype$.
  \item \textbf{Value Clustering:} facet values are clustered together into
  facet ranges according to source taxonomies structural analysis. Since we
  look for facets of mutual exclusive values, we admit facets containing at least two
  values. Thus, values that cannot be added to any facet (i.e., clusters of one
  element) are discarded.
\end{enumerate}

\subsection{Value Extraction}\label{sec:facet-extraction-value}
During this phase we identify the set of facet values that are frequently used
for the representation of source instances. In order to identify such values
for for a \kg type $\namedtype$ we rely on existing mappings to $\namedtype$.
For each source taxonomy $\source\typetaxonomy$ we form the set
$\unobjects{\namedtype}{\source\typetaxonomy}$ of values occurring as
lexicalization of source types mapped to $\namedtype$ and all their ancestors in the respective
source taxonomy. The level of detail of source taxonomies can be different in
each source taxonomy, thus ancestors' lexicalizations are included in
$\unobjects{\namedtype}{\source\typetaxonomy}$ to consider every possible significant
value. The set $\unobjects{\namedtype}{\source\typetaxonomy}$ for a \kg type
$\namedtype$ and a source taxonomy $\source\typetaxonomy$ is defined as
\begin{formula*}
\unobjects{\namedtype}{\source\typetaxonomy} = \{
\thelexicalization(\source\namedtype)~|~\namedtype(x)
\leftarrow
\source\namedtype(x)~\vee~\namedtype(x) \leftarrow
\source\namedtype'(x),~\text{with}~\source\namedtype'~\source\subtype~\source\namedtype\}.
\end{formula*}

The set $\nobjects{\namedtype}{\source\typetaxonomy}$ of normalized values is
obtained by applying case lowerization, special characters removal and stemming to
$\unobjects{\namedtype}{\source\typetaxonomy}$. As far as stemming is concerned, we use
Hunspell Stemmer\footnote{\url{http://hunspell.sourceforge.net/}} to normalize
values' terms with respect to their singular form. Hunspell stemmer is based on
language dependent stemming rules that are available for most of languages.
Normalized values are then unioned together to form the set
$\nobjects{\namedtype}{}$ of facet values for a for a \kg type $\namedtype$. In this
phase, duplicated values are removed. The set $\nobjects{\namedtype}{}$ of unique
values for a  \kg type $\namedtype$ over all the $n$ source taxonomies is defined
as
\begin{formula*}
\nobjects{\namedtype}{} = \bigcup\limits^n_{i = 1}
\nobjects{\namedtype}{\source\typetaxonomy_i}.
\end{formula*}

After normalization and unioning, a simple ranking function is applied to
$\nobjects{\namedtype}{}$. Unique values are ranked according to their frequency
over the all sets $\nobjects{\namedtype}{\source\typetaxonomy_i}$. Intuitively, the more a
value occurs as source type mapped to the \kg type $\namedtype$, the higher rank it
will get. Based on this ranking we reduce $\nobjects{\namedtype}{}$ to the set
$\nobjects{\namedtype}{k}$ of the top $k$ frequent values. The rationale
behind this choice is to keep only those values that are more commonly used
across many independent and heterogeneous sources and thus are likely to be
more relevant for the fine-grained representation of dataspace instances. The
set $\nobjects{\namedtype}{k}$ of the top frequent values produced by this
phase represents the input for the next phase. In addition, we keep track of
the (possibly) many source types to which each value $\mvalue \in
\nobjects{\namedtype}{k}$ correspond. In this way, the annotation of the
dataspace instances with facet values extracted by the algorithm is
straightforward.

\paragraph{Example.} Given the two taxonomies $A$ and $B$ in
Figure~\ref{fig:process}, for the \kg type $\named{Wines}$
\begin{formula*}
\unobjects{\named{Wines}}{A} = \{\named{Beverages}, \named{Wines}, \named{Tuscany},
\named{Chianti}, \named{Sicily}, \named{Nero\;d'Avola},
\named{Vermentino}\} 
\end{formula*} and
\begin{formula*}
\unobjects{\named{Wines}}{B} = \{ \named{Root}, \named{Food\;And\;Drinks}, \named{Wines},
\named{White\;Wines}, \named{Verdicchio}, \named{Red\;Wines},\\ \named{Cabernet},
\named{Lombardy}, \named{Sicily}, \named{Chianti}\}.
\end{formula*}
After normalization, values from
$\unobjects{\named{Wines}}{A}$ and $\unobjects{\named{Wines}}{B}$ form the set of
unique facet values 
\begin{formula*}
\nobjects{\named{Wines}}{} = \{\named{Root}, \named{Beverage},
\named{Food\;And\;Drink}, \named{Wine}, \named{Lombardy}, \named{Cabernet},
\named{Tuscany},\\ \named{Chianti}, \named{Sicily}, \named{Vermentino},
\named{Nero\;d'Avola}, \named{White\;Wine},
\named{Verdicchio}, \named{Red\;Wine}\}.
\end{formula*}

\subsection{Value Clustering}\label{sec:clustering}

Values in $\nobjects{\namedtype}{k}$ are clustered to form the set of facet
ranges $\facetranges{\namedtype}$. The goal is to cluster together all values that are
more likely to be coordinate values identifying objects of the same relation. As
an example, suppose that the set $\nobjects{\named{Wines}}{k} =
\{\named{Cabernet}, \named{Chianti}, \named{Lombardy}, \named{Sicily}\}$ is
produced in the Value Extraction phase. An ideal clustering should be
$\mfterms_1 = \{\named{Cabernet}, \named{Chianti}\}$ and $\mfterms_2 =
\{\named{Lombardy}, \named{Sicily}\}$ because each facet range refers to a
same relation (i.e., the wine's grape variety and the origin Italian region).

So as to discover the set $\facetranges{\namedtype}$ of facets ranges over
$\nobjects{\namedtype}{k}$, the DBSCAN \emph{density-based} clustering
algorithm~\cite{Ester96adensity-based} is used. DBSCAN clusters together values
within a maximum distance threshold $\epsilon$ and satisfying a cluster density
criterion and discards as noise values that are distant from any resulting
cluster. The DBSCAN algorithm requires in input the minimum cardinality of
expected clusters and the maximum distance threshold $\epsilon$. In the proposed
approach, the minimum cardinality is set to 2, while the best value for
$\epsilon$ is found empirically (see
Section~\ref{sec:facet-extraction-evaluation}). Finally, DBSCAN does not require
a number of expected clusters as input. DBSCAN is used for several reasons. The
proposed approach must deal with heterogeneous taxonomies, thus it cannot make
any assumption about the shape of clusters (i.e., facet ranges) and it must
employ clustering techniques that incorporate the notion of noise. Otherwise,
clustering algorithms requiring the expected number of clusters as input are not
suitable (e.g., KMeans~\cite{KMEANS}) since the number of facets to detect is
not known in advance.

In order to use the DBSCAN clustering algorithm it is crucial to provide an
effective distance metric between the values. We propose a distance metric that
considers near those values that refer to a same characteristic of instances,
according to a taxonomic structural criterion. We now formally define the
proposed distance metric, starting from the principle that it aims at capturing:
source type mutual exclusivity.

\concept{Source Type Mutual Exclusivity Principle}

Recall from Section~\ref{sec:facets} that a facet is ``a
clearly defined, mutually exclusive, and collectively exhaustive aspect,
property, or characteristic of a class or specific
subject''~\cite{taylor2004wynar}. The Source Type Mutual Exclusivity principle
(STME) states that the more two values refer to mutually exclusive types, the
more they should be grouped together into the same facet range. Given two source
types $\source\namedtype_1$ and $\source\namedtype_2$, their occurrence as siblings may
indicate that $\source\namedtype_1$ and $\source\namedtype_2$ are mutually exclusive (e.g.,
$\named{Cabernet}$ and $\named{Chianti}$ in Figure~\ref{fig:taxonomies}). STME
is a \emph{structural} principle: it takes source taxonomies structure into account
by considering reciprocal relationships among types.

\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{property-extraction-images/taxonomies-grey-final.pdf}
	\caption{An example of mutually exclusive source types.}
	\label{fig:taxonomies}
\end{figure}

\concept{Taxonomy Layer Distance}
We propose a distance metric that captures the STME principle by considering
sibling relationships between source types, or more generally, the co-occurrence
of types on a same taxonomy layer. Given a taxonomy $\source\typetaxonomy$, a taxonomy
\emph{layer} $\layer{\source\typetaxonomy}$ of $\source\typetaxonomy$ is the set of all
types that are at the same distance from the taxonomy root. For example, the set
$\{\named{Lombardy}, \named{Tuscany}, \named{Sicily}\}$ is a layer for taxonomy
$A$ in Figure~\ref{fig:taxonomies}. At large scale, types occurring on same taxonomy
layers are likely to be mutually exclusive since they usually represent
partitions of the set of source instances categorized under the considered
taxonomy. Considering co-occurrences on the same taxonomy layer represents a
principled way to capture the STME principle:
the more two values $\mvalue_1$ and $\mvalue_2$ co-occur at the same layer
across all source taxonomies, the more they should be clustered together and thus the less they
are distant from each other.

We compute the Taxonomy Layer Distance (TLD) between two values $\mvalue_1$ and
$\mvalue_2$ by counting their co-occurrences on the same taxonomy layer and
scaling it by their nominal occurrences across all source taxonomies. Computing TLD is
equivalent to computing the Jaccard Distance between the two sets of taxonomy
layers where two values $\mvalue_1$ and $\mvalue_2$ occur, respectively. Given
a value $v$ and a source taxonomy $\source\typetaxonomy$ we define the set
$\Layer{v}{\source\typetaxonomy}$ of layers containing $v$ in
$\source\typetaxonomy$ as $\Layer{\mvalue}{\source\typetaxonomy} =
\{\layer{\source\typetaxonomy}~|~\mvalue \in \layer{\source\typetaxonomy}\}$ (a
type can occur in more than one layer). The overall set $\Layer{\mvalue}{}$ of
layers containing $\mvalue$ is computed by unioning all layers across all $n$
source taxonomies as
\begin{formula*}
\Layer{\mvalue}{} = \bigcup_{i = 1}^n \Layer{v}{\source\typetaxonomy_i}.
\end{formula*}
The Jaccard Distance between $\Layer{\mvalue_1}{}$ and
$\Layer{\mvalue_2}{}$ is then computed as:
\begin{formula*}
	\text{TLD}(\mvalue_1, \mvalue_2) = 1 - \frac{|\Layer{\mvalue_1}{} \cap
	\Layer{\mvalue_2}{}|}{|\Layer{\mvalue_1}{} \cup \Layer{\mvalue_2}{}|}.
\end{formula*}

\paragraph{Example.}
Given the two source taxonomies from Figure~\ref{fig:taxonomies} and
values $\named{Cabernet}$ and $\named{Chianti}$, we first compute layers
containing $\named{Cabernet}$ and $\named{Chianti}$
\begin{formula*}
\layer{A}^1 &= \{\named{Cabernet}, \named{Chianti}, \named{Cantina Firriato},
\named{Cantina Almeria}\}\\
\layer{A}^2 &= \{\named{Cabernet}, \named{Nero\;d'Avola}\}\\
\layer{B}^1 &= \{\named{Vermentino}, \named{Cabernet}, \named{Verdicchio},
\named{Chianti}\}.
\end{formula*}
The set of layers containing $\named{Cabernet}$ and $\named{Chianti}$ are
\begin{formula*}
\Layer{\named{Cabernet}}{A} &= \{\layer{A}^1,~\layer{A}^2\}\\
\Layer{\named{Cabernet}}{B} &= \{\layer{B}^1\}\\
\Layer{\named{Cabernet}}{} &= \Layer{\named{Cabernet}}{A} \cup
\Layer{\named{Cabernet}}{B} = \{\layer{A}^1,~\layer{A}^2,~\layer{B}^1\}\\
\Layer{\named{Chianti}}{A} &= \{\layer{A}^1\}\\
\Layer{\named{Chianti}}{B} &= \{\layer{B}^1\}\\
\Layer{\named{Chianti}}{} &= \Layer{\named{Chianti}}{A} \cup
\Layer{\named{Chianti}}{B} = \{\layer{A}^1,~\layer{B}^1\}.
\end{formula*}
Hence, the distance between $\named{Chianti}$ and $\named{Cabernet}$ is computed
as
\begin{formula*}
\text{TLD}(\named{Cabernet}, \named{Chianti}) = 1 -
\cfrac{|\Layer{\named{Cabernet}}{} \cap
\Layer{\named{Chianti}}{}|}{|\Layer{\named{Cabernet}}{} \cup \Layer{\named{Chianti}}{}|}
= 1 - \cfrac{2}{3} = \cfrac{1}{3}.
\end{formula*}

\section{Faceted Assertions Generation}\label{sec:facet-extraction-assertions}

The output of the facet extraction algorithm described in the last section is
the set of extracted facets $\dsfacets{\namedtype} = \{\mfacet_1 = <\namedtype,
\facetrange_1>, \ldots, \mfacet_n = <\namedtype, \facetrange_n> \}$ that, after
the validation of domain experts, enrich the schema of the \kg. However, to
enable the proper publication of such facets in the front-end of the dataspace,
additional information must be stored. In particular, to enable query answering
after the materialization of source data into the dataspace (see,
Section~\ref{sec:ci-cycle}), a set of mappings from the sources to the newly
extracted facets has to be generated.

Recall from Section~\ref{sec:facet-extraction-value} that values in the facet
ranges are extracted from the lexicalizations of source types and normalized by
applying lowerization, special characters removal and stemming. During such
phase, we keep track of the (possibly) many source types to which each
normalized facet value correspond. To this end, we define a convenient function
$origin$ that takes a normalized facet value in input and returns the set of
source types from which the value have been extracted. This function
conceptually ``reverses'' the value normalization by accessing the
correspondences between normalized facet values and source types stored during
the value extraction phase.

Given an extracted facet $\mfacet = <\namedtype, \facetrange>$ and the
correspondences between normalized facet values and source types from which they
have been extracted, mappings are generated in the following form:
\begin{formula*}
\mfacet(x, \mvalue) \leftarrow \sourcetype(x),~\sourcetype \in
origin(\mvalue),~\mvalue \in \facetrange.
\end{formula*}
The semantics of such mappings is that a value $\mvalue$ is asserted to be the
value of the facet $\mfacet$ for the instance $x$, if $x$ is an instance of the
source type $\sourcetype$, $\mvalue$ is included in the extracted facet range
and was extracted from $\sourcetype$. Such mapping acts as a transformation
rule used to populate the \kg with a set of faceted assertions during the
materialization of source data into the dataspace. Such faceted assertions will
then be leveraged in the front-end of the dataspace to support, for example, a
faceted search interface (see Section~\ref{sec:facets} for more details).
