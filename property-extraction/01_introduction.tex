\section{Overview}\label{sec:intro}

\emph{Type-based} and \emph{facet-based} browsing are two examples of data
access features that many DI intensive applications, such as the Comparison
Shopping Engine described in Section~\ref{sec:motivating-example}, aim to
deliver to their users. These features require the creation and maintenance of a
domain specific representation respectively based on \kg types, and
\emph{facets}~\cite{taylor2004wynar,weiJWE2013}. As introduced in
Sections~\ref{sec:knowledge-graphs} and~\ref{sec:facets}, \kg types provide a
coarse-grained representation of all the instances in the dataspace, which
helps users to rapidly recall the ``family'' of instances they are interested
in. Conversely, facets provide a fine-grained representation of \kg instances,
which helps users to rapidly recall instances with specific characteristics
(e.g., \quoted{Grape: Barolo}, \quoted{Type: Red Wine}).

Facet creation and maintenance is an extremely time and effort consuming task in
the context of DI applications and specifically in the context of CSEs. This
task is left to manual work of domain experts and requires a deep understanding
of the salient characteristics of \kg instances (e.g., wines are characterized
by grape, type, provenance, and so on) for a large number of diverse product
types or domains. As a result, many CSEs provide only few generalist facets
(e.g., $\named{price}$ and $\named{merchant}$) and others provide a richer set
of domain specific facets but only for a limited amount of popular product
types.

This chapter introduces an automatic, domain specific facet extraction approach,
which supports domain experts in the creation of significant domain specific
facets. Facets are specialized relations aimed at model the salient
characteristics of entities from specific domains (e.g., news, actors, or wine
bottles), and thus the technical problem tackled in this chapter accounts to the
extraction of salient domain specific relations. Our approach leverages the
information already present in the dataspace, namely (i) taxonomies used to
classify instances from structured the data sources and (ii) t-to-t mappings
established between source and \kg types, to suggest meaningful facets specific
for a given \kg type. In fact, unlike the \kg types, which have to cover
instances from very diverse domains, source taxonomies are often domain
specific. Domain experts map domain specific types from source taxonomies (e.g.,
$\named{Barolo}$) to generalist types in the \kg (e.g., $\named{Wines}$). The
intuition behind the proposed approach is to reuse the fine-grained source types
that occur in several source taxonomies mapped to \kg types (e.g.,
$\named{Barolo}$, $\named{Cabernet}$), to extract a set of relevant facets for a
given \kg type. In addition, since our approach extracts facets from source
types, the generation of the corresponding faceted assertions for extracted
facets over dataspace instances is straightforward, supporting facet-based
browsing.

The proposed approach incorporates an automatic facet extraction algorithm that
consists of two steps: \textit{extraction} of potential facet values (e.g.,
$\named{Cabernet}$) and \textit{clustering} of facet values into sets of
mutually exclusive facet ranges (e.g., $\named{Bordeaux}$, $\named{Cabernet}$,
$\named{Chianti}$). The algorithm is based on structural analysis of source
taxonomies and on \textit{Taxonomy Layer Distance}, a novel metric introduced to
evaluate the distance between source types in different heterogeneous
taxonomies. Experiments conducted to evaluate the approach show that our
algorithm is able to extract meaningful facets that can then be refined by
domain experts. The remaining of the chapter discusses in details this
contribution, starting from Section~\ref{sec:facet-extraction-problem}, in which
we provide a definition of the problem of extracting domain specific facets. In
Section~\ref{sec:facet-extraction-algorithm} we describe our facet extraction
approach, and we discuss how to populate the \kg with faceted assertions for the
extracted facets in Section~\ref{sec:facet-extraction-assertions}. We evaluate
our approach in Section~\ref{sec:facet-extraction-evaluation}.
Comparison with related work (Section~\ref{sec:facet-extraction-related}) and a
final summary of the contribution here described
(Section~\ref{sec:facet-extraction-conclusion}) end the chapter.
