unset key
set key default
set key box

plot "dbpedia-numbers-ndcg-with-context-comparison.data" using 1:2 pt 6 title "freq", \
"" using 1:3 pt 8 title "freq+cov", \
"" using 1:4 pt 4 title "DRC"
