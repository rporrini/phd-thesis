set ylabel "MRR" offset 2,0
set yrange [0:1]
set xtics ("yago-explicit" 1, "yago-ambiguous" 2)

plot "yago-mrr-comparison.data" using ($1-0.21):2:(0.2) ls 1 fs pattern 1 title "frequency", \
"" using ($1):3:(0.2) ls 2 fs pattern 2 title "frequency+coverage", \
"" using ($1+0.21):4:(0.2) ls 3 fs solid 0.8 title "drc"

