set xrange [1:5]
set xtics 1

plot "yago-abstract-ndcg.data" using 1:2 pt 6 title "majority", \
"yago-abstract-ndcg.data" using 1:3 pt 8 title "maximum likelihood", \
"yago-abstract-ndcg.data" using 1:4 pt 4 title "drc"
