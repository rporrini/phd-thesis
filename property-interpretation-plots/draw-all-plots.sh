#!/bin/sh

set -e

relative_path=`dirname $0`
current_dir=`cd $relative_path;pwd`
cd $current_dir

for f in `ls *ndcg*.p`; 
do
	./draw-ndcg-plots ${f%%.*};
done

for f in `ls *recall*.p`; 
do
	./draw-ndcg-plots ${f%%.*};
done

for f in `ls *.p | grep -v ndcg | grep -v recall`; 
do
	./draw-box-plots ${f%%.*};
done

./draw-key
./draw-key-comparison
./draw-key-ndcg
./draw-key-ndcg-comparison
